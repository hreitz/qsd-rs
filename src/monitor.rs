use crate::error::{BlockError, BlockResult};
#[cfg(feature = "vhost-user-blk")]
use crate::helpers::InfallibleFuture;
use crate::helpers::{
    nbd, BlockFutureResult, BoxedFuture, MainThreadOnlyMarker, SendOnDrop, SingleThreadSingleton,
};
use crate::node::{
    self, BackgroundOpResult, DirtyBitmapIterator, FadingNode, Node, NodeConfig, NodeId,
    PollableNode, PollableNodeStopMode, ReopenQueue,
};
use crate::server::{self, Server, ServerConfig, ServerDriverData, ServerNodeData};
use crate::thread::Thread;
use clap::Parser;
use serde::{Deserialize, Serialize};
use std::cell::{Cell, RefCell};
use std::collections::{HashMap, HashSet, LinkedList};
use std::future::Future;
use std::pin::Pin;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};
use std::thread::ThreadId;
use tokio::sync::mpsc;

mod chardev;
use chardev::{Chardev, ChardevConfig};

mod id_generator;
use id_generator::IdGenerator;

mod prompt;
use prompt::{Prompt, PromptConfig};

pub mod qmp;

pub struct Monitor {
    nodes: RefCell<HashMap<String, Option<Arc<Node>>>>,
    pollable_nodes: RefCell<HashMap<NodeId, PollableNode>>,
    paused_nodes: RefCell<HashSet<NodeId>>,
    servers: RefCell<HashMap<String, Server>>,

    fading_nodes: RefCell<Vec<FadingNode>>,
    /// Necessary to provide qemu's job commands.  Maps job IDs to node names while the node is
    /// running, and to node::JobInfo objects when concluded.
    jobs: RefCell<HashMap<String, JobReference>>,

    /// If `None`, the chardev has been claimed e.g. by a prompt (but the ID is still considered in
    /// use, and so the entry in the hash map remains present)
    chardevs: RefCell<HashMap<String, Option<Chardev>>>,
    prompts: RefCell<HashMap<String, Prompt>>,

    /// Prompts must be able to add new prompts.  This happens during `Monitor::poll_prompts()`,
    /// where the `prompts` map is borrowed mutably.  New prompts must therefore be first inserted
    /// into this list, before they can be drained into the map.
    new_prompts: RefCell<Vec<Prompt>>,

    /// Receiving end of the global QMP event queue
    event_r: RefCell<mpsc::UnboundedReceiver<qmp::Event>>,

    /// Non-prompt listeners for events
    event_listeners: RefCell<Vec<mpsc::UnboundedSender<qmp::Event>>>,

    /// True if `quit()` is invoked.  Do not allow further QMP then.
    quitting: Cell<bool>,

    /// True if `quit()` is done, and all that is left is to close all prompts.
    close_all_prompts: Cell<bool>,

    /// If `None`, the thread is in the process of being joined, which has not been completed yet.
    /// The ID cannot be used until the thread is fully joined.
    threads: RefCell<HashMap<ThreadId, Option<MonitorThread>>>,
    thread_ids: RefCell<HashMap<String, ThreadId>>,

    id_generator: RefCell<IdGenerator>,

    /// Whether a new future to be polled by the monitor has been added (i.e. a pollable node, a
    /// server, or a prompt).  The poll handler needs to loop until this is false.
    added_pollee: Cell<bool>,
}

/// Implements `Future` and can thus be polled.  The monitor cannot implement `Future`, because
/// `Future::poll()` requires providing mutable access to `self`, which we cannot do for the
/// monitor.  The `MonitorPoller` in contrast can be owned by a single user, thus be mutable, and
/// will delegate `<MonitorPoller as Future>::poll()` to the various `Monitor::poll_*()` methods.
pub struct MonitorPoller {
    monitor: Rc<Monitor>,
    /// If the monitor has nothing to do anymore (no pollable nodes, no servers, no prompts) and
    /// intends to quit, but has not actually `quit()` yet, do it by storing a future here and poll
    /// it (and the monitor) until it is done.
    quit: Option<BlockFutureResult<'static, ()>>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct ObjectConfig {
    qom_type: String,
    id: String,
}

#[derive(clap::Parser)]
struct CommandLineOpts {
    /// Create a node in the block graph.
    ///
    /// Takes a JSON object with parameters.
    /// Mandatory parameters are `node-name`, which gives the node an ID, and `driver`, which
    /// selects the kind of node to create.  Further options largely depend on the selected driver.
    ///
    /// Examples:
    ///
    /// --blockdev '{"node-name": "node0", "driver": "file", "filename": "vm.qcow2"}'
    ///
    /// --blockdev '{"node-name": "node1", "driver": "qcow2", "file": "node0"}'
    ///
    /// --blockdev '{"node-name": "node2", "driver": "vhost-user-blk-export", "exported": "node1", "addr": {"type": "unix", "path": "vhost-user-blk.sock"}}'
    #[clap(long)]
    blockdev: Vec<String>,

    /// Create an export node in the block graph.
    ///
    /// Takes a JSON object with parameters.
    /// This option only exists for compatibility with QSD, and is effectively an alias for
    /// `--blockdev`, with the following differences: The `type` value is suffixed with "-export"
    /// and used as `driver`; the `node-name` value is used `exported`; and the `id` value is
    /// prefixed with ":export:" and used as `node-name`.
    ///
    /// Example:
    ///
    /// --export '{"id": "exp0", "type": "vhost-user-blk", "node-name": "node1", "addr": {"type": "unix", "path": "vhost-user-blk.sock"}}'
    #[clap(long)]
    export: Vec<String>,

    /// Create a server, which is necessary for export nodes that allow attaching multiple exports
    /// to a single server (i.e. NBD).
    ///
    /// Takes a JSON object with parameters.
    /// Mandatory parameters are `id`, which gives the server an ID, and `type`, which selects the
    /// kind of server to create.  Further parameters depend on the selected server type.
    ///
    /// Example:
    ///
    /// --server '{"id": "srv0", "type": "nbd", "addr": {"type": "inet", "host": "::", "port": "10809"}}'
    #[clap(long)]
    server: Vec<String>,

    /// Create an NBD server.
    ///
    /// Takes a JSON object with parameters.
    /// This option only exists for compatibility with QSD, and is effectively an alias for
    /// `--server`, with `type` being set to "nbd", and `id` being set to "<unnamed-nbd-server>".
    ///
    /// Example:
    ///
    /// --nbd-server '{"addr": {"type": "inet", "host": "::", "port": "10809"}}'
    #[clap(long)]
    nbd_server: Vec<String>,

    /// Create a character device for use in --monitor.
    ///
    /// Takes a JSON object with parameters.
    /// Mandatory parameters are `id`, which gives the chardev an ID, `backend.type`, which selects
    /// the kind of chardev to create, and `backend.data`, which contains parameters that depend on
    /// the selected type.
    ///
    /// Examples:
    ///
    /// --chardev '{"id": "chr0", "backend": {"type": "stdio", "data": {}}}'
    ///
    /// --chardev '{"id": "chr1", "backend": {"type": "socket", "data": {"server": true, "addr": {"type": "unix", "path": "qmp.sock"}}}}'
    #[clap(long)]
    chardev: Vec<String>,

    /// Create an interface to QMP.
    ///
    /// Takes a JSON object with parameters.
    /// Mandatory parameters are `id`, which gives this interface an ID, and `chardev`, which
    /// selects a previously created character device over which the QMP client will connect to
    /// RSD.
    ///
    /// Example:
    ///
    /// --monitor '{"id": "mon0", "chardev": "chr0"}'
    #[clap(long)]
    monitor: Vec<String>,

    /// Create a miscellaneous object.
    ///
    /// Takes a JSON object with parameters.
    /// Mandatory parameters are `id`, which gives the object an ID, and `qom-type`, which selects
    /// the kind of object to create.  Currently, the only supported object type is "iothread".
    ///
    /// Example:
    ///
    /// --object '{"qom-type": "iothread", "id": "thread0"}'
    #[clap(long)]
    object: Vec<String>,
}

pub struct MonitorThread {
    thread: Rc<Thread>,

    /// Whether this thread is to be joined when the last handle to it is dropped
    ephemeral: bool,
}

pub struct ThreadHandle {
    /// If `None`, this is a pseudo-handle to the main thread
    thread: Option<Rc<Thread>>,

    /// Whether this thread is to be joined when the last handle to it is dropped
    ephemeral: bool,

    /// Thread handles can only be used from the main thread
    _main_thread: MainThreadOnlyMarker,
}

/// Reference to a job
enum JobReference {
    /// Job is running on the given node
    NodeName(String),

    /// Job is no longer running, keep the last info until it is dismissed
    Concluded(node::JobInfo),
}

/// The single global monitor.
static MONITOR: SingleThreadSingleton<Monitor> = SingleThreadSingleton::new();

/// Allows pushing events into the global event queue.
static EVENTS: Mutex<Option<mpsc::UnboundedSender<qmp::Event>>> = Mutex::new(None);

/// Retrieves a strong reference to the global monitor.  Must only be called from the main thread.
pub fn monitor() -> Rc<Monitor> {
    MONITOR
        .get(|| {
            let (event_s, event_r) = mpsc::unbounded_channel();
            let previous = EVENTS.lock().unwrap().replace(event_s);
            assert!(previous.is_none());
            Monitor::new(event_r)
        })
        .expect("Attempted to access the monitor from a thread that is not the main thread")
}

/// Push an event into the global event queue.  May be called from any thread.
pub fn broadcast_event(qmp_event: qmp::Event) {
    EVENTS
        .lock()
        .unwrap()
        .as_mut()
        .expect("Attempted to send an event before the monitor has been set up")
        .send(qmp_event)
        .unwrap();
}

fn export_opts_to_blockdev_opts(opts: serde_json::Value) -> BlockResult<NodeConfig> {
    let mut obj = match opts {
        serde_json::Value::Object(obj) => obj,
        _ => return Err(format!("{:?} is not a JSON object", opts).into()),
    };

    let driver = obj.remove("type").ok_or("`type` key is missing")?;
    let id = obj.remove("id").ok_or("`id` key is missing")?;
    let exported = obj
        .remove("node-name")
        .ok_or("`node-name` key is missing")?;

    let id = match id {
        serde_json::Value::String(s) => s,
        _ => return Err(format!("`id` must be a string, but {:?} is not", id).into()),
    };

    // Give exports added via the legacy interface a different namespace
    let id = format!(":export:{}", id);

    let driver = match driver {
        serde_json::Value::String(s) => s,
        _ => return Err(format!("`type` must be a string, but {:?} is not", driver).into()),
    };

    // Node drivers are named this way to differentiate them from the potential client counterparts
    let driver = format!("{}-export", driver);

    let existing = obj.insert(String::from("driver"), serde_json::Value::String(driver));
    assert!(existing.is_none());
    let existing = obj.insert(String::from("exported"), exported);
    assert!(existing.is_none());
    let existing = obj.insert(String::from("node-name"), serde_json::Value::String(id));
    assert!(existing.is_none());

    let opts: NodeConfig = serde_json::from_value(serde_json::Value::Object(obj))?;
    Ok(opts)
}

impl Monitor {
    fn new(event_r: mpsc::UnboundedReceiver<qmp::Event>) -> Self {
        Monitor {
            nodes: Default::default(),
            pollable_nodes: Default::default(),
            paused_nodes: Default::default(),
            fading_nodes: Default::default(),
            servers: Default::default(),
            jobs: Default::default(),
            chardevs: Default::default(),
            prompts: Default::default(),
            new_prompts: Default::default(),
            event_r: RefCell::new(event_r),
            event_listeners: Default::default(),
            quitting: Cell::new(false),
            close_all_prompts: Cell::new(false),
            threads: Default::default(),
            thread_ids: Default::default(),
            id_generator: Default::default(),
            added_pollee: Cell::new(false),
        }
    }

    pub async fn parse_args(self: &Rc<Self>) -> BlockResult<()> {
        let opt = CommandLineOpts::parse();

        for object in opt.object {
            let opts =
                serde_json::from_str(&object).map_err(|e| BlockError::from(e).prepend(&object))?;

            self.object_add(opts)
                .await
                .map_err(|e| e.prepend(&object))?;
        }

        for server in opt.server {
            let opts =
                serde_json::from_str(&server).map_err(|e| BlockError::from(e).prepend(&server))?;

            self.server_start(opts)
                .await
                .map_err(|e| e.prepend(&server))?;
        }

        for nbd_server in opt.nbd_server {
            let opts = serde_json::from_str(&nbd_server)
                .map_err(|e| BlockError::from(e).prepend(&nbd_server))?;

            self.nbd_server_start(opts)
                .await
                .map_err(|e| e.prepend(&nbd_server))?;
        }

        for blockdev in opt.blockdev {
            let opts = serde_json::from_str(&blockdev)
                .map_err(|e| BlockError::from(e).prepend(&blockdev))?;

            self.blockdev_add(opts)
                .await
                .map_err(|e| e.prepend(&blockdev))?;
        }

        for export in opt.export {
            let opts =
                serde_json::from_str(&export).map_err(|e| BlockError::from(e).prepend(&export))?;

            let blockdev_opts =
                export_opts_to_blockdev_opts(opts).map_err(|e| e.prepend(&export))?;

            self.blockdev_add(blockdev_opts)
                .await
                .map_err(|e| e.prepend(&export))?;
        }

        for chardev in opt.chardev {
            let opts = serde_json::from_str(&chardev)
                .map_err(|e| BlockError::from(e).prepend(&chardev))?;

            self.chardev_add(opts)
                .await
                .map_err(|e| e.prepend(&chardev))?;
        }

        for prompt in opt.monitor {
            let opts =
                serde_json::from_str(&prompt).map_err(|e| BlockError::from(e).prepend(&prompt))?;

            self.monitor_add(opts)
                .await
                .map_err(|e| e.prepend(&prompt))?;

            self.add_new_prompts();
        }

        Ok(())
    }

    async fn single_blockdev_add(&self, node_opts: NodeConfig) -> BlockResult<()> {
        let (node, pollable) = node::new(node_opts).await?;
        let node_id = node.id();

        let mut nodes = self.nodes.borrow_mut();
        if nodes.contains_key(&node.name) {
            return Err(format!("Node with name \"{}\" already exists", node.name).into());
        }
        if let Some(job_info) = node.block_job_info() {
            let mut jobs = self.jobs.borrow_mut();
            let existing = jobs.insert(job_info.id, JobReference::NodeName(node.name.clone()));
            assert!(existing.is_none());
        }
        let existing = nodes.insert(node.name.clone(), Some(node));
        assert!(existing.is_none());

        if let Some(pollable) = pollable {
            let mut pollable_nodes = self.pollable_nodes.borrow_mut();
            let existing = pollable_nodes.insert(node_id, pollable);
            assert!(existing.is_none());

            self.added_pollee.replace(true);
        }

        Ok(())
    }

    pub async fn blockdev_add(self: &Rc<Self>, node_opts: NodeConfig) -> BlockResult<()> {
        let mut opts_list = Vec::<NodeConfig>::new();
        node_opts.split_tree(&mut opts_list)?;

        let mut undo_list = Vec::<String>::new();

        for node_opts in opts_list {
            let name = node_opts.node_name.clone();
            if let Err(e) = self.single_blockdev_add(node_opts).await {
                let mut events = self.add_event_listener();
                // Delete all previously added nodes, in reverse order (top to bottom)
                for node in undo_list.drain(..).rev() {
                    // Try stopping the node first (ignore errors that occur if this is not a
                    // pollable node).  Note that we cannot have `single_blockdev_add()` add just
                    // the node and skip adding the pollable node until we have added all nodes:
                    // Pollable nodes rely on the background operation actually running.  If it is
                    // never run, reopen operations (which happen when adding a parent) may stall
                    // because they try to communicate the reopen changes to the background
                    // operations.
                    if self
                        .blockdev_stop(node.clone(), PollableNodeStopMode::Hard)
                        .await
                        .is_ok()
                    {
                        while let Some(event) = events.recv().await {
                            if let qmp::Events::BackgroundOperationCompleted {
                                node_name,
                                error: _,
                            } = event.event
                            {
                                if node_name == node {
                                    break;
                                }
                            }
                        }

                        // Maybe the node went away on its own; check that before running
                        // `blockdev_del()`.
                        if self.lookup_node(&node).is_err() {
                            continue;
                        }
                    }

                    if let Err(err) = self.blockdev_del(node.clone()).await {
                        broadcast_event(qmp::Event::new(qmp::Events::NodeFadeError {
                            node_name: node,
                            error: err.into_description(),
                        }));
                    }
                }
                return Err(e);
            }
            undo_list.push(name);
        }

        Ok(())
    }

    pub async fn blockdev_stop(
        &self,
        node_name: String,
        mode: PollableNodeStopMode,
    ) -> BlockResult<()> {
        let fut = {
            self.nodes
                .borrow()
                .get(&node_name)
                .ok_or_else(|| format!("Node with name \"{}\" not found", node_name))?
                .as_ref()
                .ok_or_else(|| format!("Node with name \"{}\" is fading", node_name))?
                .stop(mode)
        };

        fut.await
    }

    pub async fn blockdev_pause(&self, node_name: String) -> BlockResult<()> {
        let node = {
            let node = Arc::clone(
                self.nodes
                    .borrow()
                    .get(&node_name)
                    .ok_or_else(|| format!("Node with name \"{}\" not found", node_name))?
                    .as_ref()
                    .ok_or_else(|| format!("Node with name \"{}\" is fading", node_name))?,
            );

            if !self.pollable_nodes.borrow().contains_key(&node.id()) {
                return Err(format!("No background operation on node \"{}\"", node_name).into());
            }

            // `.insert()` returns *false* if the value *was already present*
            if !self.paused_nodes.borrow_mut().insert(node.id()) {
                return Err(format!("Node \"{}\" is already paused", node_name).into());
            }

            node
        };

        node.quiesce_background().await;
        Ok(())
    }

    pub fn blockdev_resume(&self, node_name: String) -> BlockResult<()> {
        let node = {
            let node = Arc::clone(
                self.nodes
                    .borrow()
                    .get(&node_name)
                    .ok_or_else(|| format!("Node with name \"{}\" not found", node_name))?
                    .as_ref()
                    .ok_or_else(|| format!("Node with name \"{}\" is fading", node_name))?,
            );

            if !self.paused_nodes.borrow_mut().remove(&node.id()) {
                return Err(format!("Node \"{}\" is not paused", node_name).into());
            }

            node
        };

        node.unquiesce_background();
        Ok(())
    }

    pub async fn blockdev_del(self: &Rc<Self>, node_name: String) -> BlockResult<()> {
        let node = {
            let mut nodes = self.nodes.borrow_mut();
            let node_ref = nodes
                .get_mut(&node_name)
                .ok_or_else(|| format!("Node with name \"{}\" not found", node_name))?;

            let node = node_ref
                .take()
                .ok_or_else(|| format!("Node with name \"{}\" is fading", node_name))?;
            match Arc::try_unwrap(node) {
                Ok(node) => {
                    nodes.remove(&node_name);
                    node
                }

                Err(node) => {
                    node_ref.replace(node);
                    return Err(format!("Node \"{}\" is in use", node_name).into());
                }
            }
        };

        {
            if self.paused_nodes.borrow_mut().remove(&node.id()) {
                node.unquiesce_background();
            }
        }

        if let Ok(node_inner) = SendOnDrop::try_unwrap(node) {
            // Let the node e.g. write cached data before dropping it, while we can still do so in
            // an async context.  (`Drop::drop()` does not run in an async context, and creating
            // one there means creating a nested context, which tokio does not like.)
            // If `SendOnDrop::try_unwrap()` failed, someone (i.e. `FadingNode`) already installed
            // an on-drop receiver on the node, and should be taking care of this once we drop it.
            node_inner.drain_caches().await;
        }

        Ok(())
    }

    pub async fn blockdev_reopen(
        self: &Rc<Self>,
        mut node_opts: Vec<NodeConfig>,
    ) -> BlockResult<()> {
        let mut opts_list = Vec::<NodeConfig>::new();
        for opts in node_opts.drain(..) {
            opts.split_tree(&mut opts_list)?;
        }

        let mut queue = ReopenQueue::new();
        for opts in opts_list {
            let node = self.lookup_node(&opts.node_name)?;
            queue.push(node, opts)?;
        }

        queue.reopen().await
    }

    pub async fn blockdev_move_child(
        self: &Rc<Self>,
        node_name: String,
        child_name: String,
        new_child_node_name: String,
    ) -> BlockResult<()> {
        let node = self.lookup_node(&node_name)?;

        let config = node.get_opts();
        let json = serde_json::to_value(config)?;
        let mut json_obj = match json {
            serde_json::Value::Object(obj) => obj,
            _ => {
                return Err(format!(
                    "Stored options {:?} for node {} are not an object",
                    json, node_name
                )
                .into())
            }
        };

        json_obj.insert(
            child_name,
            serde_json::Value::String(new_child_node_name.clone()),
        );
        let config: NodeConfig = serde_json::from_value(serde_json::Value::Object(json_obj))?;

        let mut queue = ReopenQueue::new();
        queue.push(node, config)?;
        queue.reopen().await?;
        Ok(())
    }

    pub async fn query_jobs<I: From<node::JobInfo>>(self: &Rc<Self>) -> BlockResult<Vec<I>> {
        Ok(self
            .jobs
            .borrow()
            .values()
            .filter_map(|job_ref| match job_ref {
                JobReference::NodeName(node_name) => self
                    .lookup_node(node_name)
                    .ok()
                    .and_then(|node| node.block_job_info()),

                JobReference::Concluded(info) => Some(info.clone()),
            })
            .map(|info| info.into())
            .collect())
    }

    pub async fn nbd_server_start(
        self: &Rc<Self>,
        nbd_opts: server::nbd::ServerConfig,
    ) -> BlockResult<()> {
        self.server_start(nbd_opts.into()).await
    }

    pub async fn server_start(self: &Rc<Self>, server_opts: ServerConfig) -> BlockResult<()> {
        let server = Server::new(server_opts).await?;

        let mut servers = self.servers.borrow_mut();
        if servers.contains_key(&server.id) {
            return Err(BlockError::from_desc(format!(
                "Server ID \"{}\" is already in use",
                server.id
            )));
        }
        let existing = servers.insert(server.id.clone(), server);
        assert!(existing.is_none());

        self.added_pollee.replace(true);

        Ok(())
    }

    pub fn server_stop(self: &Rc<Self>, id: String) -> BlockResult<()> {
        let mut servers = self.servers.borrow_mut();
        let server = servers
            .get_mut(&id)
            .ok_or_else(|| format!("Unknown server ID \"{}\"", id))?;
        server.stop();
        Ok(())
    }

    pub async fn chardev_add(self: &Rc<Self>, chardev_opts: ChardevConfig) -> BlockResult<()> {
        let chardev = Chardev::new(chardev_opts).await?;

        let mut chardevs = self.chardevs.borrow_mut();
        if chardevs.contains_key(&chardev.id) {
            return Err(format!("Chardev ID \"{}\" is already in use", chardev.id).into());
        }
        let existing = chardevs.insert(chardev.id.clone(), Some(chardev));
        assert!(existing.is_none());

        Ok(())
    }

    pub async fn monitor_add(self: &Rc<Self>, prompt_opts: PromptConfig) -> BlockResult<()> {
        let chardev_id = &prompt_opts.chardev;
        let chardev = self
            .chardevs
            .borrow_mut()
            .get_mut(chardev_id)
            .ok_or_else(|| format!("Chardev \"{}\" not found", chardev_id))?
            .take()
            .ok_or_else(|| format!("Chardev \"{}\" is already in use", chardev_id))?;

        let prompt = Prompt::new(prompt_opts, chardev).await?;
        self.new_prompts.borrow_mut().push(prompt);

        // Because we are not allowed to borrow the `prompts` map here, we cannot tell whether the
        // ID is already taken.  Only `self.add_new_prompts()` will know that.

        Ok(())
    }

    pub async fn object_add(self: &Rc<Self>, opts: ObjectConfig) -> BlockResult<()> {
        if opts.qom_type == "iothread" {
            self.add_named_thread(opts.id)
        } else {
            Err(format!("Unknown object type \"{}\"", opts.qom_type).into())
        }
    }

    pub async fn object_del(self: &Rc<Self>, id: String) -> BlockResult<()> {
        // Once we support more object types, we need to keep a map of IDs to object types to
        // delete the right thing here.  For now, we only have threads, so no need for a map.
        self.join_thread(&id).await
    }

    pub async fn block_dirty_bitmap_add(
        self: &Rc<Self>,
        node: String,
        name: String,
        granularity: u64,
        disabled: bool,
    ) -> BlockResult<()> {
        let node = self.lookup_node(&node)?;
        node.add_dirty_bitmap(&name, granularity, !disabled).await?;
        Ok(())
    }

    pub async fn block_dirty_bitmap_remove(
        self: &Rc<Self>,
        node: String,
        name: String,
    ) -> BlockResult<()> {
        let node = self.lookup_node(&node)?;
        node.remove_dirty_bitmap(&name).await?;
        Ok(())
    }

    pub fn block_dirty_bitmap_enable(
        self: &Rc<Self>,
        node: String,
        name: String,
        enabled: bool,
    ) -> BlockResult<()> {
        let node = self.lookup_node(&node)?;
        let bitmap = node.get_dirty_bitmap(&name)?;
        bitmap.lock().unwrap().set_enabled(enabled);
        Ok(())
    }

    pub fn block_dirty_bitmap_clear(
        self: &Rc<Self>,
        node: String,
        name: String,
    ) -> BlockResult<()> {
        let node = self.lookup_node(&node)?;
        let bitmap = node.get_dirty_bitmap(&name)?;
        bitmap.lock().unwrap().full_clear();
        Ok(())
    }

    pub fn block_dirty_bitmap_merge(
        self: &Rc<Self>,
        target_node: String,
        target_name: String,
        bitmaps: Vec<(String, String)>,
    ) -> BlockResult<()> {
        let target = self
            .lookup_node(&target_node)?
            .get_dirty_bitmap(&target_name)?;
        let mut target = target.lock().unwrap();

        let sources = bitmaps
            .into_iter()
            // Skip self-merge
            .filter(|bitmap| bitmap.0 != target_node || bitmap.1 != target_name)
            .map(|bitmap| -> BlockResult<_> {
                self.lookup_node(&bitmap.0)?
                    .get_dirty_bitmap(&bitmap.1)
            })
            .collect::<BlockResult<Vec<_>>>()?;

        for source in sources {
            target.merge(&source.lock().unwrap());
        }

        Ok(())
    }

    /// Drains the list of new prompts and attempts to add them all to `self.prompts`.  Return
    /// `true` if new prompts were added.
    fn add_new_prompts(&self) -> bool {
        let mut prompts = self.prompts.borrow_mut();
        let mut new_prompts = self.new_prompts.borrow_mut();
        let mut added_prompts = false;

        for prompt in new_prompts.drain(..) {
            if prompts.contains_key(&prompt.id) {
                let error = format!("Prompt ID \"{}\" is already in use", prompt.id);
                let ev = qmp::Events::PromptCreationError {
                    id: prompt.id,
                    error,
                };
                broadcast_event(qmp::Event::new(ev));
                continue;
            }
            let existing = prompts.insert(prompt.id.clone(), prompt);
            assert!(existing.is_none());
            added_prompts = true;
        }

        if added_prompts {
            self.added_pollee.replace(true);
        }

        added_prompts
    }

    pub fn lookup_node(&self, name: &str) -> BlockResult<Arc<Node>> {
        self.nodes
            .borrow()
            .get(name)
            .cloned()
            .ok_or_else(|| format!("Node with name \"{}\" not found", name))?
            .ok_or_else(|| format!("Node with name \"{}\" is fading", name).into())
    }

    pub fn add_node_to_server(
        &self,
        id: &str,
        node_name: &str,
        node_data: ServerNodeData,
    ) -> BlockResult<()> {
        self.servers
            .borrow()
            .get(id)
            .ok_or_else(|| format!("Server \"{}\" not found", id))?
            .add_node(node_name, node_data)
    }

    pub fn remove_node_from_server(&self, server_id: &str, node_name: &str) -> BlockResult<()> {
        self.servers
            .borrow()
            .get(server_id)
            .ok_or_else(|| format!("Server \"{}\" not found", server_id))?
            .remove_node(node_name)
    }

    /// Have the given node fade out, emitting the given legacy event when done.  Potential errors
    /// are emitted as `NodeFadeError` events.  The node must have been taken from `self.nodes`
    /// (using `.take()`) and on error, it will be re-inserted into that list.
    fn fade_node(&self, node: Arc<Node>, legacy_fade_event: Option<qmp::Events>) {
        let node_name = node.name.clone();

        let successor = match node.get_successor() {
            Some(successor) => successor,
            None => {
                broadcast_event(qmp::Event::new(qmp::Events::NodeFadeError {
                    node_name: node_name.clone(),
                    error: format!("Node \"{}\" cannot name a successor", node_name),
                }));

                self.nodes
                    .borrow_mut()
                    .get_mut(&node_name)
                    .unwrap()
                    .replace(node);

                return;
            }
        };

        // Fading nodes must not be paused, so unpause it now
        {
            if self.paused_nodes.borrow_mut().remove(&node.id()) {
                node.unquiesce_background();
            }
        }

        match FadingNode::new(node, successor, legacy_fade_event) {
            Ok(fading) => {
                self.fading_nodes.borrow_mut().push(fading);
                self.added_pollee.replace(true);
            }

            Err((err, node)) => {
                self.nodes
                    .borrow_mut()
                    .get_mut(&node_name)
                    .unwrap()
                    .replace(node);

                broadcast_event(qmp::Event::new(qmp::Events::NodeFadeError {
                    node_name,
                    error: err.into_description(),
                }));
            }
        }
    }

    fn lookup_job(&self, id: &str) -> BlockResult<String> {
        let jobs = self.jobs.borrow();
        let job_ref = jobs
            .get(id)
            .ok_or_else(|| format!("Job \"{}\" not found", id))?;

        match job_ref {
            JobReference::NodeName(node_name) => Ok(node_name.clone()),
            JobReference::Concluded(_) => {
                Err(format!("Job \"{}\" is already concluded", id).into())
            }
        }
    }

    /// Cancels the job, with a twist: If `force == false` and the job is in `Ready` state,
    /// complete it instead, not switching over to the target.
    async fn block_job_cancel(&self, id: &str, force: bool) -> BlockResult<()> {
        let node_name = self.lookup_job(id)?;

        if force {
            return self
                .blockdev_stop(node_name, PollableNodeStopMode::Hard)
                .await;
        }

        let info = {
            self.nodes
                .borrow()
                .get(&node_name)
                .ok_or_else(|| {
                    format!(
                        "Node \"{}\" belonging to job \"{}\" not found",
                        node_name, id
                    )
                })?
                .as_ref()
                .ok_or_else(|| format!("Job \"{}\" is already finalized", id))?
                .block_job_info()
                .ok_or_else(|| format!("Job \"{}\" is already dismissed", id))?
        };

        if info.status == qmp::JobStatus::Ready || info.status == qmp::JobStatus::Standby {
            self.blockdev_stop(
                node_name,
                PollableNodeStopMode::CopyComplete { switch_over: false },
            )
            .await
        } else {
            self.blockdev_stop(node_name, PollableNodeStopMode::Hard)
                .await
        }
    }

    async fn job_cancel(&self, id: &str) -> BlockResult<()> {
        let node = self.lookup_job(id)?;
        self.blockdev_stop(node, PollableNodeStopMode::Hard).await
    }

    async fn job_complete(&self, id: &str) -> BlockResult<()> {
        let node = self.lookup_job(id)?;
        self.blockdev_stop(
            node,
            PollableNodeStopMode::CopyComplete { switch_over: true },
        )
        .await
    }

    /// Used by block jobs to transfer ownership of the job info to the monitor once they are
    /// concluded.
    pub fn job_concluded(&self, info: node::JobInfo) {
        let id = info.id.clone();

        let mut jobs = self.jobs.borrow_mut();
        match jobs.remove(&id) {
            Some(JobReference::NodeName(_)) => {
                jobs.insert(id.clone(), JobReference::Concluded(info))
            }
            Some(JobReference::Concluded(_)) => panic!("Job {} concluded twice", id),
            None => panic!("Job {} concluded, but is not in the list of jobs", id),
        };
    }

    pub fn job_dismiss(&self, id: &str) -> BlockResult<()> {
        let mut jobs = self.jobs.borrow_mut();
        match jobs.get(id) {
            Some(JobReference::NodeName(_)) => {
                Err(format!("Job \"{}\" has not concluded yet", id).into())
            }
            Some(JobReference::Concluded(_)) => {
                jobs.remove(id);

                broadcast_event(qmp::Event::new(qmp::Events::JobStatusChange {
                    id: String::from(id),
                    status: qmp::JobStatus::Null,
                }));

                Ok(())
            }
            None => Err(format!("Job \"{}\" not found", id).into()),
        }
    }

    fn job_finalize(&self, id: &str) -> BlockResult<()> {
        let node_name = self.lookup_job(id)?;
        let node = {
            self.nodes
                .borrow_mut()
                .get_mut(&node_name)
                .ok_or_else(|| {
                    format!(
                        "Node \"{}\" belonging to job \"{}\" not found",
                        node_name, id
                    )
                })?
                .take()
                .ok_or_else(|| format!("Job \"{}\" is already finalized", id))?
        };

        self.fade_node(node, None);
        Ok(())
    }

    async fn job_pause(&self, id: &str) -> BlockResult<()> {
        let node = self.lookup_job(id)?;
        self.blockdev_pause(node).await
    }

    fn job_resume(&self, id: &str) -> BlockResult<()> {
        let node = self.lookup_job(id)?;
        self.blockdev_resume(node)
    }

    pub async fn quit(self: &Rc<Self>) -> BlockResult<()> {
        let already_quitting = self.quitting.replace(true);
        if already_quitting {
            // We cannot guarantee that the other 'quit' will succeed, so better be safe than sorry
            // and return an error here.
            return Err("Already quitting".into());
        }
        let res = self.do_quit().await;

        match res {
            Ok(()) => Ok(()),
            Err(err) => {
                self.quitting.replace(false);
                Err(err)
            }
        }
    }

    async fn do_quit(self: &Rc<Self>) -> BlockResult<()> {
        let mut events = self.add_event_listener();

        let paused_nodes: Vec<Arc<Node>> = {
            let mut paused_nodes = self.paused_nodes.borrow_mut();
            let nodes = self.nodes.borrow();

            let list: Vec<Arc<Node>> = nodes
                .values()
                .filter_map(|node| {
                    node.as_ref().and_then(|node| {
                        paused_nodes.contains(&node.id()).then(|| Arc::clone(node))
                    })
                })
                .collect();

            paused_nodes.clear();

            list
        };
        for node in paused_nodes {
            node.unquiesce_background();
        }

        let mut pollable_nodes: HashSet<String> = {
            let nodes = self.nodes.borrow();
            let pollable_nodes = self.pollable_nodes.borrow();
            nodes
                .iter()
                .filter_map(|(name, node)| {
                    node.as_ref().and_then(|node| {
                        pollable_nodes
                            .contains_key(&node.id())
                            .then(|| name.to_string())
                    })
                })
                .collect()
        };
        for name in pollable_nodes.iter() {
            self.blockdev_stop(name.clone(), PollableNodeStopMode::Hard)
                .await?;
        }
        while !pollable_nodes.is_empty() {
            let event = events.recv().await.unwrap().event;
            if let qmp::Events::BackgroundOperationCompleted {
                node_name,
                error: _,
            } = event
            {
                let present = pollable_nodes.remove(&node_name);
                assert!(present);
            }
        }
        assert!(self.pollable_nodes.borrow().is_empty());

        while !self.fading_nodes.borrow().is_empty() {
            let _event = events.recv().await.unwrap().event;
            // Wait for NodeFaded or NodeFadeError events, but we do not really care, they
            // automatically drain `self.faded_nodes`
        }

        let mut servers: HashSet<String> = {
            let servers = self.servers.borrow();
            servers.keys().map(|id| id.to_string()).collect()
        };
        for srv in servers.iter() {
            self.server_stop(srv.clone())?;
        }
        while !servers.is_empty() {
            let event = events.recv().await.unwrap().event;
            if let qmp::Events::ServerDeleted { id, error: _ } = event {
                let present = servers.remove(&id);
                assert!(present);
            }
        }
        assert!(self.servers.borrow().is_empty());

        // Delete all nodes
        {
            // TODO: Do this explicitly topologically (not just implicitly)
            // Unwrapping the node references is safe, because we have waited for nodes to fade
            // above; when there are no fading nodes, all entries in `nodes` must be `Some(_)`.
            while !self.nodes.borrow().is_empty() {
                let to_delete: Vec<String> = self
                    .nodes
                    .borrow()
                    .iter()
                    .filter(|(_, node)| Arc::strong_count(node.as_ref().unwrap()) == 1)
                    .map(|(name, _)| name.to_string())
                    .collect();

                assert!(!to_delete.is_empty());
                for name in to_delete {
                    self.blockdev_del(name).await.unwrap();
                }
            }
        }

        // Join all threads
        let threads: Vec<String> = {
            let thread_ids = self.thread_ids.borrow();
            thread_ids.keys().map(|id| id.to_string()).collect()
        };
        for thr in threads {
            self.join_thread(&thr).await?;
        }

        // Delete all chardevs that are not in use (the ones in use will be dropped immediately
        // when the prompt is closed, thanks to `quitting` being `true`)
        self.chardevs.borrow_mut().clear();

        // Signal to the `poll()` handler to close all prompts.  We cannot do that here, because
        // this `quit` here was most likely invoked through a prompt, i.e. we are actually
        // currently running beneath `poll()` with the prompts borrowed.
        self.close_all_prompts.replace(true);

        Ok(())
    }

    fn x_dbg_dump_bitmap(
        self: &Rc<Self>,
        node: String,
        name: String,
        clear: bool,
    ) -> BlockResult<Vec<(u64, u64)>> {
        let node = self.lookup_node(&node)?;
        let bitmap = node.get_dirty_bitmap(&name)?;
        let iter = DirtyBitmapIterator::new(bitmap, u64::MAX, clear);

        Ok(iter.map(|area| (area.offset, area.length)).collect())
    }

    /// Execute the given QMP command, returning the respective reply.  Capabilities must have
    /// already been negotiated.
    pub async fn execute(self: &Rc<Self>, qmp_cmd: qmp::Command) -> qmp::Reply {
        if self.quitting.get() {
            return qmp::error("Already quitting");
        }

        match qmp_cmd {
            qmp::Command::BlockdevAdd(opts) => self.blockdev_add(opts).await.into(),

            qmp::Command::BlockdevBackup {
                job_id,
                device,
                target,
                sync,
                granularity,
                buf_size,
                filter_node_name,
                auto_finalize,
                auto_dismiss,
            } => {
                let filter_node_name = filter_node_name.unwrap_or_else(|| {
                    let mut idgen = self.id_generator.borrow_mut();
                    idgen.generate("#node")
                });
                node::copy::blockdev_backup(
                    job_id,
                    device,
                    target,
                    sync,
                    granularity,
                    buf_size,
                    filter_node_name,
                    auto_finalize,
                    auto_dismiss,
                )
                .await
                .into()
            }

            qmp::Command::BlockdevDel { node_name } => self.blockdev_del(node_name).await.into(),

            qmp::Command::BlockdevMirror {
                job_id,
                device,
                target,
                sync,
                granularity,
                buf_size,
                filter_node_name,
                copy_mode,
                auto_finalize,
                auto_dismiss,
            } => {
                let filter_node_name = filter_node_name.unwrap_or_else(|| {
                    let mut idgen = self.id_generator.borrow_mut();
                    idgen.generate("#node")
                });
                node::copy::blockdev_mirror(
                    job_id,
                    device,
                    target,
                    sync,
                    granularity,
                    buf_size,
                    filter_node_name,
                    copy_mode,
                    auto_finalize,
                    auto_dismiss,
                )
                .await
                .into()
            }

            qmp::Command::BlockdevPause { node_name } => {
                self.blockdev_pause(node_name).await.into()
            }

            qmp::Command::BlockdevReopen { options } => self.blockdev_reopen(options).await.into(),

            qmp::Command::BlockdevResume { node_name } => self.blockdev_resume(node_name).into(),

            qmp::Command::BlockdevStop { node_name, mode } => {
                self.blockdev_stop(node_name, mode).await.into()
            }

            qmp::Command::BlockDirtyBitmapAdd {
                node,
                name,
                granularity,
                disabled,
            } => self
                .block_dirty_bitmap_add(
                    node,
                    name,
                    granularity.unwrap_or(65536),
                    disabled.unwrap_or(false),
                )
                .await
                .into(),

            qmp::Command::BlockDirtyBitmapClear(bitmap) => self
                .block_dirty_bitmap_clear(bitmap.node, bitmap.name)
                .into(),

            qmp::Command::BlockDirtyBitmapDisable(bitmap) => self
                .block_dirty_bitmap_enable(bitmap.node, bitmap.name, false)
                .into(),

            qmp::Command::BlockDirtyBitmapEnable(bitmap) => self
                .block_dirty_bitmap_enable(bitmap.node, bitmap.name, true)
                .into(),

            qmp::Command::BlockDirtyBitmapMerge {
                node,
                target,
                bitmaps,
            } => {
                let bitmaps = bitmaps
                    .into_iter()
                    .map(|bitmap| match bitmap {
                        qmp::BlockDirtyBitmapOrStr::Local(name) => (node.clone(), name),
                        qmp::BlockDirtyBitmapOrStr::External(bitmap) => (bitmap.node, bitmap.name),
                    })
                    .collect();
                self.block_dirty_bitmap_merge(node, target, bitmaps).into()
            }

            qmp::Command::BlockDirtyBitmapRemove(bitmap) => self
                .block_dirty_bitmap_remove(bitmap.node, bitmap.name)
                .await
                .into(),

            qmp::Command::BlockExportAdd(opts) => {
                let blockdev_opts = match export_opts_to_blockdev_opts(opts) {
                    Ok(opts) => opts,
                    Err(err) => return qmp::error(err),
                };
                self.blockdev_add(blockdev_opts).await.into()
            }

            qmp::Command::BlockExportDel { id, mode } => self
                .blockdev_stop(format!(":export:{}", id), mode)
                .await
                .into(),

            qmp::Command::BlockExportMove { id, node_name } => self
                .blockdev_move_child(
                    format!(":export:{}", id),
                    String::from("exported"),
                    node_name,
                )
                .await
                .into(),

            qmp::Command::BlockJobCancel { device, force } => {
                self.block_job_cancel(&device, force).await.into()
            }

            qmp::Command::BlockJobComplete { device } => self.job_complete(&device).await.into(),

            qmp::Command::BlockJobDismiss { id } => self.job_dismiss(&id).into(),

            qmp::Command::BlockJobFinalize { id } => self.job_finalize(&id).into(),

            qmp::Command::BlockJobPause { device } => self.job_pause(&device).await.into(),

            qmp::Command::BlockJobResume { device } => self.job_resume(&device).into(),

            qmp::Command::ChardevAdd(opts) => self.chardev_add(opts).await.into(),

            qmp::Command::JobCancel { id } => self.job_cancel(&id).await.into(),

            qmp::Command::JobComplete { id } => self.job_complete(&id).await.into(),

            qmp::Command::JobDismiss { id } => self.job_dismiss(&id).into(),

            qmp::Command::JobFinalize { id } => self.job_finalize(&id).into(),

            qmp::Command::JobPause { id } => self.job_pause(&id).await.into(),

            qmp::Command::JobResume { id } => self.job_resume(&id).into(),

            qmp::Command::MonitorAdd(opts) => self.monitor_add(opts).await.into(),

            qmp::Command::NbdServerStart(opts) => self.nbd_server_start(opts).await.into(),

            qmp::Command::NbdServerStop(opts) => {
                // TODO: Check whether this is an NBD server
                let id = opts
                    .and_then(|o| o.id)
                    .unwrap_or_else(|| nbd::DEFAULT_SERVER_NAME.to_string());
                self.server_stop(id).into()
            }

            qmp::Command::ObjectAdd(opts) => self.object_add(opts).await.into(),

            qmp::Command::ObjectDel { id } => self.object_del(id).await.into(),

            qmp::Command::QmpCapabilities => {
                qmp::error("Capabilities already negotiated, cannot re-negotiate")
            }

            qmp::Command::QomSet {
                path,
                property,
                value,
            } => {
                if let Some(export) = path.strip_prefix("/exports/") {
                    if property == "node-name" {
                        self.blockdev_move_child(
                            format!(":export:{}", export),
                            String::from("exported"),
                            value,
                        )
                        .await
                        .into()
                    } else {
                        qmp::error(format!("Unknown export property \"{}\"", property))
                    }
                } else {
                    qmp::error(format!("Unknown path \"{}\"", path))
                }
            }

            qmp::Command::QueryBlockJobs => self.query_jobs::<qmp::BlockJobInfo>().await.into(),

            qmp::Command::QueryJobs => self.query_jobs::<qmp::JobInfo>().await.into(),

            qmp::Command::Quit => self.quit().await.into(),

            qmp::Command::ServerStart(opts) => self.server_start(opts).await.into(),

            qmp::Command::ServerStop { id } => self.server_stop(id).into(),

            qmp::Command::XDbgDumpBitmap { node, name, clear } => self
                .x_dbg_dump_bitmap(node, name, clear.unwrap_or(false))
                .into(),
        }
    }

    /// For use during the capability negotiation phase: Execute the given QMP command, returning
    /// the respective reply.  If the reply indicates success, the capability negotiation phase is
    /// done.
    pub async fn execute_preamble(self: &Rc<Self>, qmp_cmd: qmp::Command) -> qmp::Reply {
        if self.quitting.get() {
            return qmp::error("Already quitting");
        }

        match qmp_cmd {
            qmp::Command::QmpCapabilities => qmp::ok(()),

            _ => qmp::error("Expecting capabilities negotiation via qmp_capabilities"),
        }
    }

    /// Create a thread with the given name, without checking the name for validity.
    fn add_thread(&self, name: String, ephemeral: bool) -> BlockResult<Rc<Thread>> {
        let mut thread_ids = self.thread_ids.borrow_mut();
        if thread_ids.contains_key(&name) {
            return Err(format!("Thread ID \"{}\" is already in use", name).into());
        }

        let thread = Rc::new(Thread::new(name.clone())?);
        let mon_thread = MonitorThread {
            thread: Rc::clone(&thread),
            ephemeral,
        };

        let mut threads = self.threads.borrow_mut();
        let existing = threads.insert(thread.id(), Some(mon_thread));
        assert!(existing.is_none());

        let existing = thread_ids.insert(name, thread.id());
        assert!(existing.is_none());

        Ok(thread)
    }

    /// Create a thread with the given name.
    pub fn add_named_thread(&self, name: String) -> BlockResult<()> {
        let first_char = name.chars().next().ok_or("Empty thread ID given")?;
        if !first_char.is_alphabetic() {
            return Err(format!(
                "Thread ID \"{}\" does not begin with an alphabetic character",
                name
            )
            .into());
        }

        if name == "main" {
            return Err("Thread ID \"main\" is reserved for the actual main thread".into());
        }

        self.add_thread(name, false)?;
        Ok(())
    }

    /// Create a thread with an auto-generated ID.  While technically this thread is equivalent to
    /// any other named thread, the caller is expected to run only a single future in it.  Dropping
    /// the returned object will auto-join the thread.
    pub fn add_ephemeral_thread(&self) -> BlockResult<ThreadHandle> {
        let name = {
            let mut idgen = self.id_generator.borrow_mut();
            idgen.generate("#thread")
        };

        let thread = self.add_thread(name, true)?;
        Ok(ThreadHandle {
            thread: Some(thread),
            ephemeral: true,
            _main_thread: Default::default(),
        })
    }

    pub fn lookup_thread(&self, name: &str) -> BlockResult<ThreadHandle> {
        if name == "main" {
            return Ok(ThreadHandle {
                thread: None,
                ephemeral: false,
                _main_thread: Default::default(),
            });
        }

        let thread_id = {
            let thread_ids = self.thread_ids.borrow();
            *thread_ids
                .get(name)
                .ok_or_else(|| format!("Unknown thread ID \"{}\"", name))?
        };

        let threads = self.threads.borrow();
        let thread = threads
            .get(&thread_id)
            .unwrap()
            .as_ref()
            .ok_or_else(|| format!("Thread \"{}\" is being joined", name))?;

        Ok(ThreadHandle {
            thread: Some(Rc::clone(&thread.thread)),
            ephemeral: thread.ephemeral,
            _main_thread: Default::default(),
        })
    }

    pub fn lookup_thread_by_id(&self, id: ThreadId) -> BlockResult<ThreadHandle> {
        if id == std::thread::current().id() {
            // Main thread
            return Ok(ThreadHandle {
                thread: None,
                ephemeral: false,
                _main_thread: Default::default(),
            });
        }

        let threads = self.threads.borrow();
        let thread = threads
            .get(&id)
            .ok_or_else(|| format!("No thread with internal ID {:?}", id))?
            .as_ref()
            .ok_or_else(|| format!("Thread with internal ID {:?} is being joined", id))?;

        Ok(ThreadHandle {
            thread: Some(Rc::clone(&thread.thread)),
            ephemeral: thread.ephemeral,
            _main_thread: Default::default(),
        })
    }

    /// Helper function for anywhere where something is to be put into a thread specified by the
    /// user, which may be either a named thread (`Some(name)`) or an ephemeral thread (`None`).
    /// If `name` is given, look it up.  Otherwise, create and return an ephemeral thread.
    pub fn get_thread_from_opt(&self, name: &Option<String>) -> BlockResult<ThreadHandle> {
        match name.as_ref() {
            Some(name) => self.lookup_thread(name),
            None => self.add_ephemeral_thread(),
        }
    }

    /// Attempt to join the thread with the given name.  This will fail if the thread still has
    /// futures running in it.
    pub async fn join_thread(&self, name: &str) -> BlockResult<()> {
        if name == "main" {
            return Err("Cannot join the main thread".into());
        }

        let thread_id = {
            let thread_ids = self.thread_ids.borrow();
            *thread_ids
                .get(name)
                .ok_or_else(|| format!("Unknown thread ID \"{}\"", name))?
        };

        let mut mon_thread = {
            let mut threads = self.threads.borrow_mut();
            threads
                .get_mut(&thread_id)
                .unwrap()
                .take()
                .ok_or_else(|| format!("Thread \"{}\" is already being joined", name))?
        };

        // If there are still other references, we cannot join this thread
        let mut thread = match Rc::try_unwrap(mon_thread.thread) {
            Ok(thread) => thread,
            Err(thread) => {
                mon_thread.thread = thread;
                let mut threads = self.threads.borrow_mut();
                let existing = threads.insert(thread_id, Some(mon_thread));
                assert!(matches!(existing, Some(None)));
                return Err(format!("Cannot join thread \"{}\", still in use", name).into());
            }
        };

        match thread.try_join().await {
            Ok(()) => {
                let mut thread_ids = self.thread_ids.borrow_mut();
                let existing = thread_ids.remove(name);
                assert!(existing == Some(thread_id));
                let mut threads = self.threads.borrow_mut();
                let existing = threads.remove(&thread_id);
                assert!(matches!(existing, Some(None)));
                Ok(())
            }

            Err(err) => {
                mon_thread.thread = Rc::new(thread);
                let mut threads = self.threads.borrow_mut();
                let existing = threads.insert(thread_id, Some(mon_thread));
                assert!(matches!(existing, Some(None)));
                Err(err)
            }
        }
    }

    fn add_event_listener(&self) -> mpsc::UnboundedReceiver<qmp::Event> {
        let (sender, receiver) = mpsc::unbounded_channel();
        self.event_listeners.borrow_mut().push(sender);
        receiver
    }

    fn poll_nodes(&self, cx: &mut Context<'_>) -> bool {
        {
            let mut fading_nodes = self.fading_nodes.borrow_mut();
            let mut i = 0;
            while i < fading_nodes.len() {
                match fading_nodes[i].poll(cx) {
                    Poll::Ready(result) => {
                        let name = fading_nodes[i].node_name().clone();
                        fading_nodes.swap_remove(i);

                        let mut nodes = self.nodes.borrow_mut();
                        let event = match result {
                            Ok(()) => {
                                let existing = nodes.remove(&name);
                                assert!(matches!(existing, Some(None)));

                                qmp::Events::NodeFaded { node_name: name }
                            }

                            Err((err, node)) => {
                                let node_ref = nodes.get_mut(&name).unwrap();
                                assert!(node_ref.is_none());
                                node_ref.replace(node);

                                qmp::Events::NodeFadeError {
                                    node_name: name,
                                    error: err.into_description(),
                                }
                            }
                        };
                        broadcast_event(qmp::Event::new(event));
                    }
                    Poll::Pending => i += 1,
                }
            }
        }

        let mut pollable_nodes = self.pollable_nodes.borrow_mut();
        let mut ready_list = LinkedList::new();

        for (id, pollable) in pollable_nodes.iter_mut() {
            if let Poll::Ready(result) = pollable.poll(cx) {
                ready_list.push_back((pollable.node().name.clone(), *id, result));
            }
        }

        for (name, id, result) in ready_list {
            pollable_nodes.remove(&id);

            let BackgroundOpResult { result, auto_fade } = result;
            let error = result.err().map(|e| e.into_description());

            if auto_fade {
                let legacy_fade_event = name.strip_prefix(":export:").map(|export_name| {
                    qmp::Events::BlockExportDeleted {
                        id: export_name.to_string(),
                        error: error.clone(),
                    }
                });

                let node = {
                    self.nodes
                        .borrow_mut()
                        .get_mut(&name)
                        .map(|node| node.take())
                };

                match node {
                    Some(Some(node)) => self.fade_node(node, legacy_fade_event),

                    // If the node is already marked as fading, ignore this attempt
                    Some(None) => (),

                    // If there is no node with this name, consider it faded
                    None => {
                        broadcast_event(qmp::Event::new(qmp::Events::NodeFaded {
                            node_name: name.clone(),
                        }));

                        if let Some(event) = legacy_fade_event {
                            broadcast_event(qmp::Event::new(event));
                        }
                    }
                }
            } else if error.is_none() {
                let job_info = {
                    self.nodes
                        .borrow()
                        .get(&name)
                        .and_then(|node| node.as_ref())
                        .and_then(|node| node.block_job_info())
                };

                if let Some(job_info) = job_info {
                    broadcast_event(qmp::Event::new(qmp::Events::BlockJobPending {
                        job_type: job_info.job_type,
                        id: job_info.id,
                    }));
                }
            }

            let event = qmp::Events::BackgroundOperationCompleted {
                node_name: name,
                error,
            };
            broadcast_event(qmp::Event::new(event));
        }

        pollable_nodes.is_empty()
    }

    fn poll_servers(&self, cx: &mut Context<'_>) -> bool {
        let mut servers = self.servers.borrow_mut();
        let mut ready_list = LinkedList::new();

        for (name, server) in servers.iter_mut() {
            if let Poll::Ready(result) = server.poll(cx) {
                ready_list.push_back((name.clone(), result));
            }
        }

        for (name, result) in ready_list {
            servers.remove(&name);

            let error = result.err().map(|e| e.into_description());
            let ev = qmp::Events::ServerDeleted { id: name, error };
            broadcast_event(qmp::Event::new(ev));
        }

        servers.is_empty()
    }

    fn poll_prompts(&self, cx: &mut Context<'_>) -> bool {
        let mut prompts = self.prompts.borrow_mut();
        let mut ready_list = LinkedList::new();

        for (name, prompt) in prompts.iter_mut() {
            if let Poll::Ready((chardev, result)) = prompt.poll(cx) {
                ready_list.push_back((name.clone(), result));

                let existing = if chardev.is_closed() || self.quitting.get() {
                    let existing = self.chardevs.borrow_mut().remove(&chardev.id);

                    let ev = qmp::Events::ChardevDeleted { id: chardev.id };
                    broadcast_event(qmp::Event::new(ev));

                    existing
                } else {
                    self.chardevs
                        .borrow_mut()
                        .insert(chardev.id.clone(), Some(chardev))
                };
                if !self.quitting.get() {
                    assert!(matches!(existing, Some(None)));
                }
            }
        }

        for (name, result) in ready_list {
            prompts.remove(&name);

            let error = result.err().map(|e| e.into_description());
            let ev = qmp::Events::PromptDeleted { id: name, error };
            broadcast_event(qmp::Event::new(ev));
        }

        if self.close_all_prompts.take() {
            for (_, prompt) in prompts.iter_mut() {
                prompt.close();
            }
        }

        let empty = prompts.is_empty();
        drop(prompts); // Drop borrow before invoking `add_new_prompts()`

        let new_prompts = self.add_new_prompts();

        empty && !new_prompts
    }

    /// Drain the global event queue, and submit the events via all connected prompts
    fn submit_events(&self, cx: &mut Context<'_>) {
        let mut event_r = self.event_r.borrow_mut();
        while let Poll::Ready(Some(event)) = event_r.poll_recv(cx) {
            let mut prompts = self.prompts.borrow_mut();

            if prompts.is_empty() {
                // If no prompt is connected, log the event on stdout
                println!("{}", event);
            } else {
                for (_name, prompt) in prompts.iter_mut() {
                    prompt.submit_event(&event);
                }
            }

            let mut listeners = self.event_listeners.borrow_mut();
            let mut i = 0;
            while i < listeners.len() {
                if listeners[i].send(event.clone()).is_err() {
                    // Error means the receiving end was dropped, so remove the listener
                    listeners.swap_remove(i);
                } else {
                    i += 1;
                }
            }
        }
    }
}

impl MonitorPoller {
    pub fn new(mon: Rc<Monitor>) -> Self {
        MonitorPoller {
            monitor: mon,
            quit: None,
        }
    }
}

impl Future for MonitorPoller {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if let Some(quit_fut) = self.quit.as_mut() {
            match Future::poll(quit_fut.as_mut(), cx) {
                Poll::Ready(Ok(())) => {
                    self.quit.take();
                }
                Poll::Ready(Err(e)) => eprintln!("Failed to properly quit: {}", e),
                Poll::Pending => (),
            }
        }

        let (mut pollable_nodes_empty, mut servers_empty, mut prompts_empty);

        loop {
            pollable_nodes_empty = self.monitor.poll_nodes(cx);
            servers_empty = self.monitor.poll_servers(cx);
            prompts_empty = self.monitor.poll_prompts(cx);

            // Loop while futures to be polled are added
            if !self.monitor.added_pollee.take() {
                break;
            }
        }

        self.monitor.submit_events(cx);

        if pollable_nodes_empty && servers_empty && prompts_empty && self.quit.is_none() {
            if self.monitor.quitting.get() {
                // Has actually properly quit
                Poll::Ready(())
            } else {
                // We have not quit yet, do it now
                let cloned_monitor = Rc::clone(&self.monitor);
                self.quit = Some(Box::pin(async move { cloned_monitor.quit().await }));
                Future::poll(self, cx)
            }
        } else {
            Poll::Pending
        }
    }
}

impl ThreadHandle {
    /// Send `create_future` to `thread`, run it and the future it returns to completion there
    /// (without blocking the main thread).
    #[cfg(feature = "vhost-user-blk")] // Used only by vhost-user-blk
    pub fn spawn<G: FnOnce() -> InfallibleFuture<'static> + Send + 'static>(
        &self,
        create_future: G,
    ) {
        self.thread
            .as_ref()
            .expect("Cannot asynchronously spawn something in the main thread")
            .spawn(create_future);
    }

    /// Send `create_future` to `thread`, run it and the future it returns there, and block until
    /// the future has completed, returning its result.
    pub async fn run<'a, T: Send + 'a, G: FnOnce() -> BoxedFuture<'a, T> + Send + 'a>(
        &self,
        create_future: G,
    ) -> T {
        let thread = match self.thread.as_ref() {
            Some(thread) => thread,
            None => {
                // Main thread
                return create_future().await;
            }
        };

        thread.run(create_future).await
    }

    /// Does the same thing as `Self::run()`, but takes ownership of `self`.  This allows the
    /// following:
    /// ```rust
    /// fn run_something() -> BlockFutureResult<()> {
    ///     let thread = monitor::monitor().add_ephemeral_thread()?;
    ///     Box::pin(thread.owned_run(|| Box::pin(async move { Ok(()) })))
    /// }
    /// ```
    /// Ownership of `thread` is transferred to `.owned_run()`, so the future it generates can be
    /// returned by `run_something()`.
    /// Using `.run()` instead of `.owned_run()` would have `run_something()` return ownership of
    /// `thread`, so it would be dropped at the end of the function, which will cause a compiler
    /// error (lifetime of the future returned by `.run()` is limited by `thread`'s lifetime, which
    /// ends when the function ends, so the future cannot be returned).
    pub async fn owned_run<'a, T: Send + 'a, G: FnOnce() -> BoxedFuture<'a, T> + Send + 'a>(
        self,
        create_future: G,
    ) -> T {
        let thread = match self.thread.as_ref() {
            Some(thread) => thread,
            None => {
                // Main thread
                return create_future().await;
            }
        };

        thread.run(create_future).await
    }
}

impl Drop for ThreadHandle {
    fn drop(&mut self) {
        if !self.ephemeral {
            return;
        }

        // Must be present because `ephemeral` can only be set for handles that point to real
        // threads, not pseudo-handles to the main thread
        let thread = self.thread.take().unwrap();

        // One reference is `thread`, the other is in `Monitor::threads`.  Any other reference must
        // be from external parties, so let them drop the thread then.
        let refcount = Rc::strong_count(&thread);
        assert!(refcount >= 2);
        if refcount == 2 {
            // Drop monitor reference
            {
                let mon = monitor();
                let mut thread_ids = mon.thread_ids.borrow_mut();
                let id = thread_ids.remove(thread.name()).unwrap();
                assert!(id == thread.id());
                let mut threads = mon.threads.borrow_mut();
                threads.remove(&id).unwrap()
            };

            let thread = Rc::try_unwrap(thread).unwrap();
            thread.join_sync();
        }
    }
}
