#![feature(io_error_more)]
#![feature(linked_list_cursors)]
#![allow(clippy::assertions_on_constants)]
#![deny(unsafe_op_in_unsafe_fn)]

mod error;
mod helpers;
mod monitor;
mod node;
mod server;
mod thread;

use error::BlockResult;
use futures::FutureExt;
use monitor::MonitorPoller;
use tokio::sync::mpsc;

async fn run() -> error::BlockResult<()> {
    let mon = monitor::monitor();
    let (quit_s, mut quit_r) = mpsc::channel::<BlockResult<()>>(1);

    if let Err(err) = mon.parse_args().await {
        quit_s.try_send(Err(err)).unwrap();
    }

    let _: Result<(), _> = ctrlc::set_handler(move || {
        let _: Result<(), _> = quit_s.try_send(Err("Interrupted".into()));
    });

    let mon_fut = MonitorPoller::new(mon).fuse();
    let quit_fut = quit_r.recv().fuse();
    futures::pin_mut!(mon_fut);
    futures::pin_mut!(quit_fut);

    // Wait for either the monitor to exit or a signal appearing on the 'quit' channel.  If the
    // monitor exits, exit altogether.  Otherwise, go on and invoke the 'quit' command.
    let result = futures::select! {
        r = quit_fut => match r {
            Some(result) => result,
            None => {
                // The quit channel has been closed, probably because there was an error installing
                // the ctrl-c handler (which took ownership of the sender).  Poll the monitor to
                // completion.
                mon_fut.await;
                return Ok(());
            }
        },

        _ = mon_fut => return Ok(()),
    };

    // But if the monitor is still running, we need to have it quit now
    let mon = monitor::monitor();
    let quit_fut = mon.quit();

    // Wait for both futures to complete
    let _: (Result<(), _>, ()) = futures::join!(quit_fut, mon_fut);

    result
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    if let Err(e) = run().await {
        eprintln!("{}", e);
        std::process::exit(1);
    }

    // Explicitly exit to kill potentially lingering threads
    // (That is an issue with the stdio chardev: Tokio creates a background thread for reading from
    // stdin, which blocks, and is not killed when the handle is dropped.  It is not a real
    // problem, though, we can just exit here and kill it.)
    std::process::exit(0);
}
