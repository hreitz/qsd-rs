use crate::error::BlockResult;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};

mod socket;
mod stdio;

/// Represents a character device.  A character device in itself is not polled or used by the
/// monitor, but it is moved to prompts that will use them to send and receive QMP messages.
/// Character devices consist of a read half and a write half, each of which acts independently.
/// This is required by prompts so that events can be sent while waiting on data to be read
/// becoming available.  (If the device was not split, waiting on readability would require giving
/// up ownership on the whole character device, prohibiting writing something to it while waiting.)
pub struct Chardev {
    pub id: String,

    read_half: ChardevReadHalf,
    write_half: ChardevWriteHalf,
}

pub struct ChardevReadHalf {
    id: String,
    driver: Box<dyn ChardevDriverReadHalf>,
}

pub struct ChardevWriteHalf {
    id: String,
    driver: Box<dyn ChardevDriverWriteHalf>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct ChardevConfig {
    id: String,
    backend: ChardevDriverConfig,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type", content = "data", rename_all = "kebab-case")]
pub enum ChardevDriverConfig {
    Socket(socket::Config),
    Stdio(stdio::Config),
}

impl Chardev {
    /// Split a character device into its independent read and write halves.
    pub fn split(self) -> (ChardevReadHalf, ChardevWriteHalf) {
        (self.read_half, self.write_half)
    }

    /// Reconstruct a character device from its previously split halves.
    pub fn construct(rh: ChardevReadHalf, wh: ChardevWriteHalf) -> Self {
        assert!(rh.id == wh.id);

        Chardev {
            id: rh.id.clone(),
            read_half: rh,
            write_half: wh,
        }
    }

    /// Check whether the character device is closed (both read and write half).
    pub fn is_closed(&self) -> bool {
        self.read_half.is_closed() && self.write_half.is_closed()
    }
}

#[async_trait(?Send)]
pub trait ChardevDriverReadHalf {
    /// Close the read half
    async fn close(&mut self);
    /// Check whether the read half is closed
    fn is_closed(&self) -> bool;
    /// Read as much data as is available, but at least one byte (and block until it becomes
    /// available)
    async fn read_partial(&mut self, buf: &mut [u8]) -> BlockResult<usize>;
}

#[async_trait(?Send)]
pub trait ChardevDriverWriteHalf {
    /// Close the write half
    async fn close(&mut self);
    /// Check whether the write half is closed
    fn is_closed(&self) -> bool;
    /// Write the whole slice
    async fn write_all(&mut self, buf: &[u8]) -> BlockResult<()>;
}

impl Chardev {
    pub async fn new(opts: ChardevConfig) -> BlockResult<Self> {
        let (rh, wh): (
            Box<dyn ChardevDriverReadHalf>,
            Box<dyn ChardevDriverWriteHalf>,
        ) = match opts.backend {
            ChardevDriverConfig::Socket(o) => socket::new(o).await?,
            ChardevDriverConfig::Stdio(o) => stdio::new(o).await?,
        };

        Ok(Chardev {
            read_half: ChardevReadHalf {
                id: opts.id.clone(),
                driver: rh,
            },
            write_half: ChardevWriteHalf {
                id: opts.id.clone(),
                driver: wh,
            },

            id: opts.id,
        })
    }
}

impl ChardevReadHalf {
    /// Close the read half
    pub async fn close(&mut self) {
        self.driver.close().await;
    }

    /// Check whether the read half is closed
    pub fn is_closed(&self) -> bool {
        self.driver.is_closed()
    }

    /// Read as much data as is available, but at least one byte (and block until it becomes
    /// available)
    pub async fn read_partial(&mut self, buf: &mut [u8]) -> BlockResult<usize> {
        self.driver.read_partial(buf).await
    }
}

impl ChardevWriteHalf {
    /// Close the write half
    pub async fn close(&mut self) {
        self.driver.close().await;
    }

    /// Check whether the write half is closed
    pub fn is_closed(&self) -> bool {
        self.driver.is_closed()
    }

    /// Write the whole slice
    pub async fn write_all(&mut self, buf: &[u8]) -> BlockResult<()> {
        self.driver.write_all(buf).await
    }
}
