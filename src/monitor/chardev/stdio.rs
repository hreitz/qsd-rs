use crate::error::BlockResult;
use crate::monitor::chardev::{ChardevDriverReadHalf, ChardevDriverWriteHalf};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::io::{Read, Write};
use std::thread;
use tokio::sync::mpsc;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {}

/// The read half consists of a thread that can read input from stdin.  Doing so is done by sending
/// a read command through the `cmd` channel, and once done, the number of bytes read comes through
/// the `ack` channel.  If the number of bytes is 0, stdin can be considered closed.
pub struct ReadHalf {
    cmd_s: mpsc::UnboundedSender<ReadCommand>,
    closed: bool,
}

/// The write half consists of a thread that can write output to stdout.  Doing so is done by
/// sending a write command through the `cmd` channel, and once done, the number of bytes written
/// comes through the `ack` channel.
pub struct WriteHalf {
    cmd_s: mpsc::UnboundedSender<WriteCommand>,
    closed: bool,
}

#[derive(Debug)]
enum ReadCommand {
    /// Read as many bytes as possible (but at least one).
    Read {
        max_len: usize,
        ack: mpsc::Sender<BlockResult<Vec<u8>>>,
    },

    /// Quit the read thread.
    Quit,
}

#[derive(Debug)]
enum WriteCommand {
    /// Write all bytes.
    WriteAll {
        buf: Vec<u8>,
        ack: mpsc::Sender<BlockResult<()>>,
    },

    /// Quit the write thread.
    Quit,
}

pub async fn new(
    _opts: Config,
) -> BlockResult<(
    Box<dyn ChardevDriverReadHalf>,
    Box<dyn ChardevDriverWriteHalf>,
)> {
    let (read_cmd_s, mut read_cmd_r) = mpsc::unbounded_channel::<ReadCommand>();
    let (write_cmd_s, mut write_cmd_r) = mpsc::unbounded_channel::<WriteCommand>();

    thread::spawn(move || {
        let stdin = std::io::stdin();

        while let Some(cmd) = read_cmd_r.blocking_recv() {
            match cmd {
                ReadCommand::Read { max_len, ack } => {
                    let mut buf = vec![0u8; max_len];
                    let result = stdin.lock().read(&mut buf).map(|len| {
                        buf.truncate(len);
                        buf
                    });
                    let _: Result<(), _> = ack.blocking_send(result.map_err(From::from));
                }

                ReadCommand::Quit => break,
            }
        }
    });

    thread::spawn(move || {
        let stdout = std::io::stdout();

        while let Some(cmd) = write_cmd_r.blocking_recv() {
            match cmd {
                WriteCommand::WriteAll { buf, ack } => {
                    let result = stdout.lock().write_all(&buf);
                    let _: Result<(), _> = ack.blocking_send(result.map_err(From::from));
                }

                WriteCommand::Quit => break,
            }
        }
    });

    let rh = ReadHalf {
        cmd_s: read_cmd_s,
        closed: false,
    };

    let wh = WriteHalf {
        cmd_s: write_cmd_s,
        closed: false,
    };

    Ok((Box::new(rh), Box::new(wh)))
}

#[async_trait(?Send)]
impl ChardevDriverReadHalf for ReadHalf {
    async fn close(&mut self) {
        let _: Result<(), _> = self.cmd_s.send(ReadCommand::Quit);
        self.closed = true;
    }

    fn is_closed(&self) -> bool {
        self.closed
    }

    async fn read_partial(&mut self, buf: &mut [u8]) -> BlockResult<usize> {
        let (ack_s, mut ack_r) = mpsc::channel::<BlockResult<Vec<u8>>>(1);
        self.cmd_s.send(ReadCommand::Read {
            max_len: buf.len(),
            ack: ack_s,
        })?;

        let result_buf = ack_r.recv().await.ok_or("stdin channel closed")??;
        let len = result_buf.len();
        assert!(len <= buf.len());

        if len > 0 {
            let buf = &mut buf[..len];
            buf.copy_from_slice(&result_buf[..]);
        } else {
            self.closed = true;
        }
        Ok(len)
    }
}

#[async_trait(?Send)]
impl ChardevDriverWriteHalf for WriteHalf {
    async fn close(&mut self) {
        let _: Result<(), _> = self.cmd_s.send(WriteCommand::Quit);
        self.closed = true;
    }

    fn is_closed(&self) -> bool {
        self.closed
    }

    async fn write_all(&mut self, buf: &[u8]) -> BlockResult<()> {
        let (ack_s, mut ack_r) = mpsc::channel::<BlockResult<()>>(1);
        self.cmd_s.send(WriteCommand::WriteAll {
            buf: buf.to_vec(),
            ack: ack_s,
        })?;
        ack_r.recv().await.ok_or("stdout channel closed")?
    }
}
