use crate::error::BlockResult;
use crate::helpers::{BoxedFuture, InfallibleFuture};
use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};
use std::thread::{self, JoinHandle, ThreadId};
use tokio::sync::{mpsc, oneshot};

#[derive(Debug)]
pub struct Thread {
    name: String,
    id: ThreadId,
    handle: Option<JoinHandle<()>>,
    msgs: mpsc::UnboundedSender<ThreadMessage>,
}

struct InThread {
    futures: Vec<InfallibleFuture<'static>>,
    settling: bool,
    msgs: mpsc::UnboundedReceiver<ThreadMessage>,
}

pub type ThreadFutureParams<'a> = dyn FnOnce() -> InfallibleFuture<'static> + Send + 'a;

enum ThreadMessage {
    AddFuture(Box<ThreadFutureParams<'static>>),
    // Return through the channel whether the thread can be joined or not
    Join(oneshot::Sender<bool>),
    SettleJoin,
}

impl Thread {
    pub fn new(name: String) -> BlockResult<Self> {
        let (msgs_s, msgs_r) = mpsc::unbounded_channel::<ThreadMessage>();

        let thread = thread::Builder::new().name(name.clone()).spawn(|| {
            // Create this struct in the thread so that the future does not need to implement
            // `Send`
            let in_thr = InThread {
                futures: Default::default(),
                settling: false,
                msgs: msgs_r,
            };
            futures::pin_mut!(in_thr);

            tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(in_thr);
        })?;

        Ok(Thread {
            name,
            id: thread.thread().id(),
            handle: Some(thread),
            msgs: msgs_s,
        })
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    /// Get the `std::thread::ThreadId`
    pub fn id(&self) -> ThreadId {
        self.id
    }

    /// Send `create_future` to this thread, and run it and the future it returns in this thread.
    /// Does not block.
    #[cfg(feature = "vhost-user-blk")] // Used only by vhost-user-blk
    pub fn spawn<G: FnOnce() -> InfallibleFuture<'static> + Send + 'static>(
        &self,
        create_future: G,
    ) {
        self.msgs
            .send(ThreadMessage::AddFuture(Box::new(create_future)))
            .ok()
            .unwrap()
    }

    /// Send `create_future` to this thread, and run it and the future it returns in this thread.
    /// Wait (block) until the future has completed, then return the future's result.
    pub fn run<'a, T: Send + 'a, G: FnOnce() -> BoxedFuture<'a, T> + Send + 'a>(
        &self,
        create_future: G,
    ) -> impl Future<Output = T> + 'a {
        let (result_s, result_r) = oneshot::channel::<T>();

        let blocking_wrapper = Box::new(move || -> InfallibleFuture {
            let fut = create_future();

            Box::pin(async move {
                result_s.send(fut.await).ok().unwrap();
            })
        });

        // Safe because we actively block waiting for the result, and we will not keep
        // `create_future` or the future beyond the lifetime of this function.
        let blocking_wrapper = unsafe {
            std::mem::transmute::<
                Box<dyn FnOnce() -> InfallibleFuture<'a> + Send>,
                Box<dyn FnOnce() -> InfallibleFuture<'static> + Send + 'static>,
            >(blocking_wrapper)
        };

        self.msgs
            .send(ThreadMessage::AddFuture(blocking_wrapper))
            .ok()
            .unwrap();

        async move { result_r.await.unwrap() }
    }

    pub async fn try_join(&mut self) -> BlockResult<()> {
        if self.handle.is_none() {
            // Already joined
            return Ok(());
        }

        let (can_join_s, can_join_r) = oneshot::channel::<bool>();
        self.msgs
            .send(ThreadMessage::Join(can_join_s))
            .ok()
            .unwrap();
        if can_join_r.await.unwrap() {
            self.handle.take().unwrap().join().unwrap();
            Ok(())
        } else {
            Err(format!("Cannot join thread \"{}\", still in use", self.name).into())
        }
    }

    /// Waits for all ongoing futures to complete and then join the thread, completely synchronous.
    /// WARNING: Only use this when you know that all futures are about to complete!
    pub fn join_sync(mut self) {
        if self.handle.is_none() {
            return;
        }

        self.msgs.send(ThreadMessage::SettleJoin).ok().unwrap();
        self.handle.take().unwrap().join().unwrap();
    }
}

impl Drop for Thread {
    fn drop(&mut self) {
        assert!(self.handle.is_none());
    }
}

impl Future for InThread {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        while let Poll::Ready(msg) = self.msgs.poll_recv(cx) {
            let msg = match msg {
                Some(msg) => msg,
                // Channel closed?  Cancel everything.
                None => return Poll::Ready(()),
            };

            match msg {
                ThreadMessage::AddFuture(gen) => {
                    let fut = gen();
                    self.futures.push(fut);
                }

                ThreadMessage::Join(can_join_channel) => {
                    let can_join = self.futures.is_empty();
                    can_join_channel.send(can_join).unwrap();
                    if can_join {
                        return Poll::Ready(());
                    }
                }

                ThreadMessage::SettleJoin => {
                    self.settling = true;
                }
            }
        }

        let mut i = 0;
        while i < self.futures.len() {
            let fut = &mut self.futures[i];
            if Future::poll(fut.as_mut(), cx).is_ready() {
                self.futures.swap_remove(i);
            } else {
                i += 1;
            }
        }

        if self.settling && self.futures.is_empty() {
            Poll::Ready(())
        } else {
            Poll::Pending
        }
    }
}

impl Drop for InThread {
    fn drop(&mut self) {
        assert!(self.futures.is_empty());
    }
}
