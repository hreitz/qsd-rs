use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, IoVector, IoVectorMut};
use crate::node::{
    ChangeGraphStep, IoQueue, IoQueueDriverData, Node, NodeBasicInfo, NodeCacheConfig, NodeConfig,
    NodeConfigOrReference, NodeDriverData, NodeLimits, NodePerm, NodePermPair, NodeUser,
    NodeUserBuilder,
};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::sync::atomic::{AtomicU64, AtomicUsize};
use std::sync::{Arc, Mutex};

struct MutData {
    file: Arc<NodeUser>,
    read_only: bool,
    pre_reopen_file: Option<Arc<NodeUser>>,
    pre_reopen_read_only: Option<bool>,
}

pub struct Data {
    mut_data: Arc<Mutex<MutData>>,
}

pub struct Queue {
    file: IoQueue,
    mut_data: Arc<Mutex<MutData>>,
    read_only: bool,

    pre_reopen_file: Option<IoQueue>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub file: NodeConfigOrReference,
}

impl Config {
    pub fn split_tree(&mut self, vec: &mut Vec<NodeConfig>) -> BlockResult<()> {
        self.file.split_tree(vec)
    }
}

impl Data {
    pub async fn new(
        node_name: &str,
        opts: Config,
        read_only: bool,
        _cache: &NodeCacheConfig,
    ) -> BlockResult<Box<Self>> {
        let file_user = NodeUser::builder(node_name, "file");
        let file = opts.file.lookup()?.add_user(file_user).await?;

        let mut_data = MutData {
            file,
            read_only,
            pre_reopen_file: None,
            pre_reopen_read_only: None,
        };
        Ok(Box::new(Data {
            mut_data: Arc::new(Mutex::new(mut_data)),
        }))
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        let data = self.mut_data.lock().unwrap();
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(data.file.node().size()),
                request_alignment: AtomicUsize::new(data.file.node().request_align()),
                memory_alignment: AtomicUsize::new(data.file.node().mem_align()),
                enforced_memory_alignment: AtomicUsize::new(1),
            },
        })
    }

    fn new_queue(&self) -> BlockResult<Box<dyn IoQueueDriverData>> {
        let data = self.mut_data.lock().unwrap();
        Ok(Box::new(Queue {
            file: data.file.new_queue()?,
            mut_data: Arc::clone(&self.mut_data),
            read_only: data.read_only,
            pre_reopen_file: None,
        }))
    }

    fn get_children(&self) -> Vec<Arc<Node>> {
        let file = Arc::clone(self.mut_data.lock().unwrap().file.node());
        vec![file]
    }

    fn get_children_after_reopen(&self, opts: &NodeConfig) -> BlockResult<Vec<Arc<Node>>> {
        let opts: &Config = (&opts.driver).try_into()?;
        let file = opts.file.lookup()?;
        Ok(vec![file])
    }

    fn reopen_change_graph<'a>(
        &'a self,
        opts: &'a NodeConfig,
        perms: NodePermPair,
        read_only: bool,
        step: ChangeGraphStep,
    ) -> BlockFutureResult<'a, ()> {
        Box::pin(async move {
            let opts: &Config = (&opts.driver).try_into()?;
            let new_file = opts.file.lookup()?;
            let mut data = self.mut_data.lock().unwrap();

            match step {
                ChangeGraphStep::Release => {
                    data.file
                        .set_perms_in_reopen_change_graph(NodePermPair::default())?;
                }

                ChangeGraphStep::Acquire => {
                    let mut file_user = NodeUserBuilder::from(data.file.as_ref()).set_perms(perms);
                    if read_only {
                        file_user = file_user
                            .unrequire(NodePerm::Write)
                            .unrequire(NodePerm::WriteUnchanged);
                    }
                    let new_file = new_file.add_user_in_reopen_change_graph(file_user)?;
                    let old_file = std::mem::replace(&mut data.file, new_file);
                    data.pre_reopen_file.replace(old_file);
                }
            }

            Ok(())
        })
    }

    fn reopen_do(
        &self,
        _opts: NodeConfig,
        _perms: NodePermPair,
        read_only: bool,
    ) -> BlockFutureResult<()> {
        Box::pin(async move {
            let mut data = self.mut_data.lock().unwrap();
            data.pre_reopen_read_only = Some(data.read_only);
            data.read_only = read_only;
            Ok(())
        })
    }

    fn reopen_clean(&self) {
        let mut data = self.mut_data.lock().unwrap();
        data.pre_reopen_file.take();
        data.pre_reopen_read_only.take();
    }

    fn reopen_roll_back(&self) {
        let mut data = self.mut_data.lock().unwrap();
        if let Some(old_file) = data.pre_reopen_file.take() {
            data.file = old_file;
        }
        if let Some(read_only) = data.pre_reopen_read_only.take() {
            data.read_only = read_only;
        }
    }
}

impl IoQueueDriverData for Queue {
    fn readv<'a>(&'a self, bufv: IoVectorMut<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        self.file.readv(bufv, offset)
    }

    fn writev<'a>(&'a self, bufv: IoVector<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        if self.read_only {
            Box::pin(async { Err("Node is read-only".into()) })
        } else {
            self.file.writev(bufv, offset)
        }
    }

    fn flush(&self) -> BlockFutureResult<'_, ()> {
        self.file.flush()
    }

    fn reopen_do(&mut self) -> BlockResult<()> {
        let data = self.mut_data.lock().unwrap();
        let new_file = data.file.new_queue()?;
        let old_file = std::mem::replace(&mut self.file, new_file);
        self.pre_reopen_file.replace(old_file);

        self.read_only = data.read_only;

        Ok(())
    }

    fn reopen_clean(&mut self) {
        self.pre_reopen_file.take();
    }

    fn reopen_roll_back(&mut self) {
        let old_file = self.pre_reopen_file.take().unwrap();
        self.file = old_file;
        self.read_only = self.mut_data.lock().unwrap().read_only;
    }
}
