use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, IoVector, IoVectorMut};
use crate::node::{
    IoQueueDriverData, Node, NodeBasicInfo, NodeCacheConfig, NodeConfig, NodeDriverData,
    NodeLimits, NodePermPair,
};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::sync::atomic::{AtomicU64, AtomicUsize};
use std::sync::{Arc, Mutex};

struct MutData {
    opts: Config,
    read_only: bool,

    pre_reopen_opts: Option<Config>,
    pre_reopen_read_only: Option<bool>,
}

pub struct Data {
    mut_data: Arc<Mutex<MutData>>,
}

pub struct Queue {
    no_op: bool,
    read_only: bool,
    mut_data: Arc<Mutex<MutData>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub size: u64,

    /// Whether reads should be no-ops, i.e. not zero out the buffer (warning: setting this to true
    /// can lead to undefined results)
    #[serde(default)]
    pub no_op: bool,
}

impl Config {
    #[allow(clippy::ptr_arg)]
    pub fn split_tree(&mut self, _vec: &mut Vec<NodeConfig>) -> BlockResult<()> {
        Ok(())
    }
}

impl Data {
    pub async fn new(
        _name: &str,
        opts: Config,
        read_only: bool,
        _cache: &NodeCacheConfig,
    ) -> BlockResult<Box<Self>> {
        let mut_data = MutData {
            opts,
            read_only,
            pre_reopen_opts: None,
            pre_reopen_read_only: None,
        };
        Ok(Box::new(Data {
            mut_data: Arc::new(Mutex::new(mut_data)),
        }))
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        let data = self.mut_data.lock().unwrap();
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(data.opts.size),
                request_alignment: AtomicUsize::new(1),
                memory_alignment: AtomicUsize::new(1),
                enforced_memory_alignment: AtomicUsize::new(1),
            },
        })
    }

    fn new_queue(&self) -> BlockResult<Box<dyn IoQueueDriverData>> {
        let data = self.mut_data.lock().unwrap();
        Ok(Box::new(Queue {
            no_op: data.opts.no_op,
            read_only: data.read_only,
            mut_data: Arc::clone(&self.mut_data),
        }))
    }

    fn get_children(&self) -> Vec<Arc<Node>> {
        Vec::new() // no children
    }

    fn get_children_after_reopen(&self, _opts: &NodeConfig) -> BlockResult<Vec<Arc<Node>>> {
        Ok(Vec::new()) // no children
    }

    fn reopen_do(
        &self,
        opts: NodeConfig,
        _perms: NodePermPair,
        read_only: bool,
    ) -> BlockFutureResult<()> {
        Box::pin(async move {
            let opts = opts.driver.try_into()?;
            let mut data = self.mut_data.lock().unwrap();
            let old = std::mem::replace(&mut data.opts, opts);
            data.pre_reopen_opts.replace(old);
            data.pre_reopen_read_only = Some(data.read_only);
            data.read_only = read_only;
            Ok(())
        })
    }

    fn reopen_clean(&self) {
        let mut data = self.mut_data.lock().unwrap();
        data.pre_reopen_opts.take();
        data.pre_reopen_read_only.take();
    }

    fn reopen_roll_back(&self) {
        let mut data = self.mut_data.lock().unwrap();
        if let Some(old) = data.pre_reopen_opts.take() {
            data.opts = old;
        }
        if let Some(read_only) = data.pre_reopen_read_only.take() {
            data.read_only = read_only;
        }
    }
}

impl IoQueueDriverData for Queue {
    fn readv<'a>(&'a self, mut bufv: IoVectorMut<'a>, _offset: u64) -> BlockFutureResult<'a, ()> {
        if self.no_op {
            Box::pin(async { Ok(()) })
        } else {
            Box::pin(async move {
                bufv.fill(0);
                Ok(())
            })
        }
    }

    fn writev<'a>(&'a self, _bufv: IoVector<'a>, _offset: u64) -> BlockFutureResult<'a, ()> {
        if self.read_only {
            Box::pin(async { Err("Null node is read-only".into()) })
        } else {
            Box::pin(async { Ok(()) })
        }
    }

    fn flush(&self) -> BlockFutureResult<'_, ()> {
        Box::pin(async { Ok(()) })
    }

    fn reopen_do(&mut self) -> BlockResult<()> {
        let data = self.mut_data.lock().unwrap();
        self.no_op = data.opts.no_op;
        self.read_only = data.read_only;
        Ok(())
    }

    fn reopen_clean(&mut self) {}

    fn reopen_roll_back(&mut self) {
        let data = self.mut_data.lock().unwrap();
        self.no_op = data.opts.no_op;
        self.read_only = data.read_only;
    }
}
