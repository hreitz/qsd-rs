/**
 * Implementation of packed virt queues.
 */
use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, FlatSize};
use crate::node::vhost_user_blk_export::guest_memory::{GuestMemory, MemRegion};
use crate::node::vhost_user_blk_export::protocol::{
    RingEventFlag, VhostUserFeature, VirtqDescFlag, VirtqEventSuppress, VirtqPackedDesc,
};
use crate::node::vhost_user_blk_export::traits::{DescriptorIterator, VirtioDeviceThreadData};
use crate::node::vhost_user_blk_export::virtq::{InFlight, ReqBuffers, VirtqRingAddresses};
use std::collections::LinkedList;
use std::sync::atomic::Ordering;

/// Represents the technical state of a packed virt queue
pub struct VirtqPackedRings<'a> {
    /// Descriptor ring
    descriptor: &'a mut [VirtqPackedDesc],
    /// Device's event suppression field
    device_event_suppression: &'a mut VirtqEventSuppress,
    /// Driver's event suppression field
    driver_event_suppression: &'a VirtqEventSuppress,

    /// Handle on guest memory
    guest_mem: &'a GuestMemory,

    /// Queue size minus one, such that `index & queue_mask == index % queue_size`
    queue_mask: usize,

    /// Bit 15 is the actual device wrap counter, all other bits are always 0
    device_wrap_counter: u16,

    /// Bit 7: Device wrap counter; bit 15: inverse
    fetch_wrap_bits: u16,
    /// Bit 7 & 15: Device wrap counter
    store_wrap_bits: u16,

    /// Index in the descriptor ring from which to fetch the next available descriptor
    fetch_index: usize,
    /// Index in the descriptor ring to where to store the next used descriptor
    store_index: usize,
}

/// Iterator over a chain of available descriptors
struct PackedDescriptorIterator<'a> {
    /// Handle on guest memory
    guest_mem: &'a GuestMemory,
    /// Cached region for `guest_mem.map_guest_cached()`
    current_region: Option<&'a MemRegion>,
    /// Handle on the descriptor ring; when encountering an indirect descriptor, this will be
    /// replaced with the memory area to which the indirect descriptor points
    descriptor_ring: &'a [VirtqPackedDesc],

    /// The next index from which to fetch a descriptor (`descriptor_ring[current_index]`)
    current_index: Option<usize>,
    /// After following an indirect descriptor, this is the number of descriptors remaining in its
    /// memory area
    indirect: Option<usize>,
    /// Queue mask
    qm: usize,

    /// Collects buffers fetched from the original descriptor ring (with ID and total number of
    /// writable bytes)
    buffers: ReqBuffers,
}

impl<'a> Iterator for PackedDescriptorIterator<'a> {
    type Item = &'a mut [u8];

    fn next(&mut self) -> Option<&'a mut [u8]> {
        let index = self.current_index?;
        let desc = &self.descriptor_ring[index];

        let addr: u64 = desc.addr.into();
        let size = u32::from(desc.len) as usize;
        let flags: u16 = desc.flags.into();
        let id: u16 = desc.id.into();

        let write_size = if flags & 0x6 == 0x2 { size } else { 0 };

        if self.indirect.is_none() {
            match &mut self.buffers {
                ReqBuffers::Empty => {
                    self.buffers = ReqBuffers::Single((id, write_size));
                }

                ReqBuffers::Single((buf, len)) => {
                    let list = vec![(*buf, *len), (id, write_size)];
                    self.buffers = ReqBuffers::List(list);
                }

                ReqBuffers::List(list) => list.push((id, write_size)),
            }
        } else if write_size > 0 {
            match &mut self.buffers {
                ReqBuffers::Empty => unreachable!(),
                ReqBuffers::Single((_buf, len)) => *len += write_size,
                ReqBuffers::List(list) => list.last_mut().unwrap().1 += write_size,
            }
        }

        // Ignore indirect descriptors that do not hold anything
        if flags & 4 != 0 && size >= VirtqPackedDesc::SIZE {
            let count = size / VirtqPackedDesc::SIZE;

            // Follow indirect descriptor
            self.descriptor_ring = self
                .guest_mem
                .map_guest_cached(&mut self.current_region, addr, count)
                .unwrap();

            self.current_index = Some(0);
            self.indirect = Some(count - 1);

            return Iterator::next(self);
        } else if flags & 1 != 0 || self.indirect.unwrap_or(0) > 0 {
            self.current_index = Some((index + 1) & self.qm);
            if let Some(indirect_count) = self.indirect.as_mut() {
                *indirect_count -= 1;
            }
        } else {
            self.current_index = None;
        }

        // Skip 0-byte descriptors
        if size == 0 {
            return Iterator::next(self);
        }

        let addr = self
            .guest_mem
            .map_guest_cached(&mut self.current_region, addr, size)
            .unwrap();

        Some(addr)
    }
}

impl<'a> DescriptorIterator<'a> for PackedDescriptorIterator<'a> {
    fn is_empty(&self) -> bool {
        self.current_index.is_none()
    }

    fn get_buffers(self) -> ReqBuffers {
        self.buffers
    }
}

impl<'a> VirtqPackedRings<'a> {
    /// Masks both the `Avail` and the `Used` flag for comparing against expected values
    /// (`fetch_wrap_bits`) and allows flipping `fetch_wrap_bits` and `store_wrap_bits` easily by
    /// XOR-ing them with this value
    const VIRTQ_WRAP_MASK: u16 = VirtqDescFlag::Avail as u16 | VirtqDescFlag::Used as u16;

    /// Given the ring addresses, creates a `VirtqPackedRings` object
    pub fn from_addresses(
        addr: &VirtqRingAddresses,
        guest_mem: &'a GuestMemory,
        queue_size: usize,
        avail_base: u32,
        features: u64,
        existing: Option<&Self>,
    ) -> BlockResult<Self> {
        assert!(features & (VhostUserFeature::RingPacked as u64) != 0);

        let descriptor = guest_mem.map_user::<VirtqPackedDesc>(addr.descriptor_ring, queue_size)?;

        let device_event_suppression =
            guest_mem.map_user::<VirtqEventSuppress>(addr.used_ring, VirtqEventSuppress::SIZE)?;
        let device_event_suppression = &mut device_event_suppression[0];

        let driver_event_suppression = guest_mem
            .map_user::<VirtqEventSuppress>(addr.available_ring, VirtqEventSuppress::SIZE)?;
        let driver_event_suppression = &mut driver_event_suppression[0];

        let (fetch_index, store_index, device_wrap_counter, fetch_wrap_bits, store_wrap_bits) =
            if let Some(existing) = existing {
                (
                    existing.fetch_index,
                    existing.store_index,
                    existing.device_wrap_counter,
                    existing.fetch_wrap_bits,
                    existing.store_wrap_bits,
                )
            } else {
                let fetch_index: u16 = (avail_base & 0xffff).try_into().unwrap();
                let store_index: u16 = (avail_base >> 16).try_into().unwrap();

                let device_wrap_counter = store_index & 0x8000;

                let fetch_wrap_bits = if device_wrap_counter != 0 {
                    VirtqDescFlag::Avail as u16
                } else {
                    VirtqDescFlag::Used as u16
                };

                let store_wrap_bits = if device_wrap_counter != 0 {
                    VirtqDescFlag::Used as u16 | VirtqDescFlag::Avail as u16
                } else {
                    0
                };

                let fetch_index = (fetch_index & 0x7fff) as usize;
                let store_index = (store_index & 0x7fff) as usize;

                (
                    fetch_index,
                    store_index,
                    device_wrap_counter,
                    fetch_wrap_bits,
                    store_wrap_bits,
                )
            };

        let rings = VirtqPackedRings {
            descriptor,
            device_event_suppression,
            driver_event_suppression,

            guest_mem,

            queue_mask: queue_size - 1,

            device_wrap_counter,

            fetch_wrap_bits,
            store_wrap_bits,

            fetch_index,
            store_index,
        };

        Ok(rings)
    }

    /// Fetch available descriptors and process them (i.e. create virtio requests based off of
    /// them).  If there are none, and if `set_event` is true, set up notifications for new
    /// descriptors becoming available.
    pub fn poll_avail<T: VirtioDeviceThreadData>(
        &mut self,
        device: &'a T,
        in_flight: &mut LinkedList<InFlight<'a>>,
        set_event: bool,
    ) -> bool {
        self.device_event_suppression.flags = (RingEventFlag::Disable as u16).into();

        let mut i = self.fetch_index;
        let qm = self.queue_mask;
        loop {
            let flags: u16 = self.descriptor[i].flags.into();
            if flags & Self::VIRTQ_WRAP_MASK != self.fetch_wrap_bits {
                if i == self.fetch_index {
                    if set_event {
                        self.device_event_suppression.flags = (RingEventFlag::Enable as u16).into();
                        // Double-check
                        std::sync::atomic::fence(Ordering::SeqCst);
                        let flags: u16 = self.descriptor[i].flags.into();
                        if flags & Self::VIRTQ_WRAP_MASK != self.fetch_wrap_bits {
                            return false;
                        }
                        // In the meantime, a new descriptor arrived, so go on
                        self.device_event_suppression.flags =
                            (RingEventFlag::Disable as u16).into();
                    } else {
                        return false;
                    }
                } else {
                    self.fetch_index = i;
                    return true;
                }
            }

            // The guest driver must make descriptors available from back to front so that once we
            // see the first descriptor as available, all the descriptos in its chain will be
            let req = self.new_request(device, i);
            i = i.wrapping_add(req.buffers.len()) & qm;
            in_flight.push_back(req);
        }
    }

    /// Create a new request beginning from the given descriptor index
    fn new_request<T: VirtioDeviceThreadData>(
        &mut self,
        device: &'a T,
        desc_i: usize,
    ) -> InFlight<'a> {
        // Interestingly not necessary in `VirtqSplitRings::new_request()`, presumably because
        // `self.descriptor` is a mutable slice here (but not there)
        let descriptor_ring = unsafe {
            std::mem::transmute::<&'_ [VirtqPackedDesc], &'a [VirtqPackedDesc]>(self.descriptor)
        };

        let mut it = PackedDescriptorIterator {
            guest_mem: self.guest_mem,
            current_region: None,
            descriptor_ring,

            current_index: Some(desc_i),
            indirect: None,
            qm: self.queue_mask,

            buffers: ReqBuffers::Empty,
        };

        let req: BlockFutureResult<()> = match device.request(&mut it) {
            Ok(x) => x,
            Err(e) => Box::pin(async move { Err(e) }),
        };

        InFlight {
            buffers: it.get_buffers(),
            req,
        }
    }

    /// Call this function when reaching the end of the descriptor ring to toggle our wrap counter
    fn toggle_device_wrap_counter(&mut self) {
        self.device_wrap_counter ^= 1u16 << 15;
        self.fetch_wrap_bits ^= Self::VIRTQ_WRAP_MASK;
        self.store_wrap_bits ^= Self::VIRTQ_WRAP_MASK;
    }

    /// Settle the given descriptor (i.e. mark as used)
    pub fn settle(&mut self, id: u16, len: usize) {
        let used_entry = &mut self.descriptor[self.store_index];

        used_entry.id = id.into();
        used_entry.len = (len as u32).into();
        std::sync::atomic::fence(Ordering::SeqCst);
        used_entry.flags = self.store_wrap_bits.into();

        self.store_index += 1;

        if (self.store_index & !self.queue_mask) != 0 {
            self.toggle_device_wrap_counter();
            self.store_index &= self.queue_mask;
        }
    }

    /// Check whether we need to notify (call) the guest driver about a number of completed
    /// (settled) descriptors; return true if so.
    pub fn notify_guest(&mut self, completed: usize) -> bool {
        let flags = u16::from(self.driver_event_suppression.flags);
        if flags & (RingEventFlag::Disable as u16) == 0 {
            if flags & (RingEventFlag::Desc as u16) == 0 {
                true
            } else {
                let desc = u16::from(self.driver_event_suppression.desc);
                let idx = (desc as usize) & self.queue_mask;
                let age = self.store_index.wrapping_sub(idx).wrapping_sub(1) & self.queue_mask;
                if age < completed {
                    match self.store_index > idx {
                        // No wrap-around, wrap counter must match
                        true => desc & (1u16 << 15) == self.device_wrap_counter,

                        // Wrap-around, wrap counter must differ
                        false => desc & (1u16 << 15) != self.device_wrap_counter,
                    }
                } else {
                    false
                }
            }
        } else {
            false
        }
    }

    pub fn get_base(&self) -> u32 {
        (self.fetch_index as u16 as u32)
            | ((self.store_index as u16 as u32) << 16)
            | (self.device_wrap_counter as u32)
            | ((self.device_wrap_counter as u32) << 16)
    }
}
