/**
 * Pre-defined virtio- and vhost-related structures, enums, and types (e.g. le_u*)
 */
use crate::helpers::FlatSize;
use crate::{flat_size, numerical_enum};

flat_size! {
    #[repr(C, packed)]
    pub struct VhostUserMessageHeader {
        pub request: u32,
        pub flags: u32,
        pub size: u32,
    }
}

numerical_enum! {
    pub enum VhostUserMessageFlag as u32 {
        Reply = 0x4,
        NeedReply = 0x8,
    }
}

impl VhostUserMessageHeader {
    pub fn new_reply<T: FlatSize>(reply_for: u32) -> Self {
        VhostUserMessageHeader {
            request: reply_for,
            flags: 0x01 | (VhostUserMessageFlag::Reply as u32),
            size: T::SIZE as u32,
        }
    }
}

numerical_enum! {
    pub enum VhostUserRequest as u32 {
        GetFeatures = 1,
        SetFeatures = 2,
        SetOwner = 3,
        ResetOwner = 4,
        SetMemTable = 5,
        SetLogBase = 6,
        SetLogFd = 7,
        SetVringNum = 8,
        SetVringAddr = 9,
        SetVringBase = 10,
        GetVringBase = 11,
        SetVringKick = 12,
        SetVringCall = 13,
        SetVringErr = 14,
        GetProtocolFeatures = 15,
        SetProtocolFeatures = 16,
        GetQueueNum = 17,
        SetVringEnable = 18,
        SendRarp = 19,
        NetSetMtu = 20,
        SetSlaveReqFd = 21,
        IotlbMsg = 22,
        SetVringEndian = 23,
        GetConfig = 24,
        SetConfig = 25,
        CreateCryptoSession = 26,
        CloseCryptoSession = 27,
        PostcopyAdvise = 28,
        PostcopyListen = 29,
        PostcopyEnd = 30,
        GetInflightFd = 31,
        SetInflightFd = 32,
        GpuSetSocket = 33,
        ResetDevice = 34,
        VringKick = 35,
        GetMaxMemSlots = 36,
        AddMemReg = 37,
        RemMemReg = 38,
    }
}

numerical_enum! {
    pub enum VhostUserProtocolFeature as u64 {
        Mq = 0x1,
        LogShmfd = 0x2,
        Rarp = 0x4,
        ReplyAck = 0x8,
        NetMtu = 0x10,
        SlaveReq = 0x20,
        CrossEndian = 0x40,
        CryptoSession = 0x80,
        Pagefault = 0x100,
        Config = 0x200,
        SlaveSendFd = 0x400,
        HostNotifier = 0x800,
        InflightShmfd = 0x1000,
        ResetDevice = 0x2000,
        InbandNotifications = 0x4000,
        ConfigureMemSlots = 0x8000,
    }
}

numerical_enum! {
    pub enum VhostUserFeature as u64 {
        RingIndirectDesc = 0x1000_0000,
        RingEventIdx = 0x2000_0000,
        ProtocolFeatures = 0x4000_0000,
        Version1 = 0x1_0000_0000,
        AccessPlatform = 0x2_0000_0000,
        RingPacked = 0x4_0000_0000,
        InOrder = 0x8_0000_0000,
        OrderPlatform = 0x10_0000_0000,
        SrIov = 0x20_0000_0000,
        NotificationData = 0x40_0000_0000,
    }
}

#[repr(C, packed)]
pub struct VhostUserMessage<T: Sized + FlatSize> {
    pub header: VhostUserMessageHeader,
    pub value: T,
}

impl<T: Sized + FlatSize> VhostUserMessage<T> {
    pub fn new_reply(reply_for: u32, value: T) -> Self {
        VhostUserMessage {
            header: VhostUserMessageHeader::new_reply::<T>(reply_for),
            value,
        }
    }
}

impl<T: Sized + FlatSize> FlatSize for VhostUserMessage<T> {
    const SIZE: usize = VhostUserMessageHeader::SIZE + T::SIZE;
}

numerical_enum! {
    pub enum VringCallFlag as u64 {
        InvalidFd = 0x100,
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct VirtioDeviceConfigSpaceHeader {
        pub offset: u32,
        pub size: u32,
        pub flags: u32,
    }
}

numerical_enum! {
    pub enum VirtioDeviceConfigSpaceFlag as u32 {
        WritableFields = 0x0,
        LiveMigration = 0x1,
    }
}

pub const VIRTIO_BLK_SECTOR_BITS: usize = 9;

flat_size! {
    #[derive(Default)]
    #[repr(C, packed)]
    pub struct VirtioBlkGeometry {
        pub cylinders: le_u16,
        pub heads: u8,
        pub sectors: u8,
    }
}

flat_size! {
    #[derive(Default)]
    #[repr(C, packed)]
    pub struct VirtioBlkTopology {
        pub physical_block_exp: u8,
        pub alignment_offset: u8,
        pub min_io_size: le_u16,
        pub opt_io_size: le_u32,
    }
}

flat_size! {
    #[derive(Default)]
    #[repr(C, packed)]
    pub struct VirtioBlkConfig {
        pub capacity: le_u64,
        pub size_max: le_u32,
        pub seg_max: le_u32,
        pub geometry: VirtioBlkGeometry,
        pub blk_size: le_u32,
        pub topology: VirtioBlkTopology,
        pub writeback: u8,
        pub _unused0: [u8; 1],
        pub num_queues: le_u16,
        pub max_discard_sectors: le_u32,
        pub max_discard_seg: le_u32,
        pub discard_sector_alignment: le_u32,
        pub max_write_zeroes_sectors: le_u32,
        pub max_write_zeroes_seg: le_u32,
        pub write_zeroes_may_unmap: u8,
        pub _unused1: [u8; 3],
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct VhostMemRegionHeader {
        pub num_regions: u32,
        pub _padding: u32,
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct VhostMemRegion {
        pub guest_address: u64,
        pub size: u64,
        pub user_address: u64,
        pub mmap_offset: u64,
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct SingleVhostMemRegion {
        pub _padding: u64,
        pub region: VhostMemRegion,
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct VhostVringState {
        pub index: u32,
        pub num: u32,
    }
}

flat_size! {
    #[derive(Default)]
    #[repr(C, packed)]
    pub struct VhostVringAddress {
        pub index: u32,
        pub flags: u32,
        pub descriptor: u64,
        pub used: u64,
        pub available: u64,
        pub log: u64,
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct VirtqSplitDesc {
        pub addr: le_u64,
        pub len: le_u32,
        pub flags: le_u16,
        pub next: le_u16,
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct VirtqPackedDesc {
        pub addr: le_u64,
        pub len: le_u32,
        pub id: le_u16,
        pub flags: le_u16,
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct VirtqUsedElem {
        pub id: le_u32,
        pub len: le_u32,
    }
}

flat_size! {
    #[repr(C, packed)]
    pub struct VirtqEventSuppress {
        pub desc: le_u16,
        pub flags: le_u16,
    }
}

numerical_enum! {
    pub enum RingEventFlag as u16 {
        Enable = 0x0,
        Disable = 0x1,
        Desc = 0x2,
    }
}

macro_rules! impl_le_type {
    ($plain_type:ty, $le_type:ident) => {
        #[allow(non_camel_case_types)]
        #[derive(Copy, Clone, Default, Eq, PartialEq)]
        pub struct $le_type($plain_type);

        impl From<$plain_type> for $le_type {
            fn from(x: $plain_type) -> $le_type {
                $le_type(<$plain_type>::to_le(x))
            }
        }

        impl From<$le_type> for $plain_type {
            fn from(x: $le_type) -> $plain_type {
                <$plain_type>::from_le(x.0)
            }
        }

        impl FlatSize for $le_type {
            const SIZE: usize = <$plain_type>::SIZE;
        }
    };
}

impl_le_type!(u16, le_u16);
impl_le_type!(u32, le_u32);
impl_le_type!(u64, le_u64);

flat_size! {
    #[repr(C, packed)]
    pub struct VirtioBlkReq {
        pub ty: le_u32,
        pub _reserved: le_u32,
        pub sector: le_u64,
    }
}

numerical_enum! {
    pub enum VirtioBlkType as u32 {
        In = 0,
        Out = 1,
        Flush = 4,
        GetId = 8,
        Discard = 11,
        WriteZeroes = 13,
    }
}

numerical_enum! {
    pub enum VirtioBlkStatus as u8 {
        Ok = 0,
        Ioerr = 1,
        Unsupp = 2,
    }
}

numerical_enum! {
    pub enum VirtioBlkFeature as u64 {
        SizeMax = 0x2,
        SegMax = 0x4,
        Geometry = 0x10,
        Ro = 0x20,
        BlkSize = 0x40,
        Flush = 0x200,
        Topology = 0x400,
        Mq = 0x1000,
        Discard = 0x2000,
        WriteZeroes = 0x4000,
    }
}

numerical_enum! {
    pub enum VirtqDescFlag as u16 {
        Next = 0x1,
        Write = 0x2,
        Indirect = 0x4,
        Avail = 0x80,
        Used = 0x8000,
    }
}
