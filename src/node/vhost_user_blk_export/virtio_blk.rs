/**
 * Virtio-blk implementation for use with the vhost-user export code
 */
use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, FlatSize, IoBufferMut, IoVectorMut, SendOnDrop};
use crate::monitor::broadcast_event;
use crate::monitor::qmp::{self, BlockErrorAction, Events, IoOperationType};
use crate::node::vhost_user_blk_export::backend::VirtqMonitorRequest;
use crate::node::vhost_user_blk_export::protocol::{
    VirtioBlkConfig, VirtioBlkFeature, VirtioBlkReq, VirtioBlkStatus, VirtioBlkTopology,
    VirtioBlkType, VIRTIO_BLK_SECTOR_BITS,
};
use crate::node::vhost_user_blk_export::traits::{
    DescriptorIterator, VirtioDevice, VirtioDeviceThreadData, VirtioDeviceThreadParam,
};
use crate::node::{IoQueue, NodeUser};
use std::cell::{Cell, UnsafeCell};
use std::cmp::Ordering::{Equal, Greater, Less};
use std::sync::{Arc, Mutex};

/// Core data for a virtio-blk device
pub struct VirtioBlk {
    /// Exported node
    node: Mutex<Arc<NodeUser>>,
    /// Drop watchdog for the node
    drop_watchdog: Arc<SendOnDrop<()>>,
    /// Whether this device is supposed to be read-only
    read_only: bool,
    /// Virtio-blk device's configuration space
    config: Mutex<VirtioBlkConfig>,

    num_queues: u16,
    logical_block_size: u16,
    physical_block_size: u32,
}

/// Necessary parameters to create a `VirtioBlkThreadData` object (this struct here must implement
/// `Send + Sync + 'static`, the other does not).
pub struct VirtioBlkThreadParam {
    /// Node from which to get `IoQueue`s
    node: Arc<NodeUser>,
    /// Drop watchdog for the node
    drop_watchdog: Arc<SendOnDrop<()>>,
    /// Whether the node is to be treated as read-only
    read_only: bool,
}

/// Per-thread data for a virtio-blk device (pinned to a virt queue thread)
pub struct VirtioBlkThreadData {
    /// Exported node
    node: Cell<Option<Arc<NodeUser>>>,
    /// Queue through which to perform I/O requests
    queue: UnsafeCell<IoQueue>,
    /// Drop watchdog for queue and node
    _drop_watchdog: Arc<SendOnDrop<()>>,
    /// Whether the queue is to be treated as read-only
    read_only: bool,

    /// For `VirtqMonitorRequest::ChangeChild*`: Attributes before `ChangeChildDo`, for potential roll-back
    pre_move_node: Cell<Option<Arc<NodeUser>>>,
    pre_move_queue: Cell<Option<IoQueue>>,
}

impl VirtioBlk {
    /// Create a new virtio-blk device
    pub fn new(
        node: Arc<NodeUser>,
        drop_watchdog: Arc<SendOnDrop<()>>,
        read_only: bool,
        num_queues: usize,
        logical_block_size: u16,
        physical_block_size: u32,
    ) -> BlockResult<Self> {
        let this = VirtioBlk {
            node: Mutex::new(node),
            drop_watchdog,
            read_only,
            config: Default::default(),
            num_queues: num_queues.try_into()?,
            logical_block_size,
            physical_block_size,
        };

        this.refresh_config();

        Ok(this)
    }

    fn refresh_config(&self) {
        let size = {
            let node = self.node.lock().unwrap();
            node.node().size()
        };

        let physical_block_exp =
            (self.physical_block_size / self.logical_block_size as u32).trailing_zeros() as u8;

        let config = VirtioBlkConfig {
            capacity: (size >> VIRTIO_BLK_SECTOR_BITS).into(),
            seg_max: (128 - 2).into(),
            num_queues: self.num_queues.into(),
            blk_size: (self.logical_block_size as u32).into(),
            topology: VirtioBlkTopology {
                physical_block_exp,
                alignment_offset: 0,
                min_io_size: self.logical_block_size.into(),
                opt_io_size: self.physical_block_size.into(),
            },
            ..Default::default()
        };

        *self.config.lock().unwrap() = config;
    }
}

impl VirtioDevice for VirtioBlk {
    type ThreadParam = VirtioBlkThreadParam;

    fn get_features(&self) -> u64 {
        let mut features = VirtioBlkFeature::Mq as u64
            | VirtioBlkFeature::BlkSize as u64
            | VirtioBlkFeature::Flush as u64
            | VirtioBlkFeature::Topology as u64
            | VirtioBlkFeature::SegMax as u64;
        if self.read_only {
            features |= VirtioBlkFeature::Ro as u64;
        }
        features
    }

    fn set_features(&self, _features: u64) {}

    fn get_queue_num(&self) -> u64 {
        self.num_queues as u64
    }

    fn get_config(&self, offset: usize, buf: &mut [u8]) -> usize {
        let (offset, size) = if offset >= VirtioBlkConfig::SIZE {
            (0, 0)
        } else if VirtioBlkConfig::SIZE - offset < buf.len() {
            (offset, VirtioBlkConfig::SIZE - offset)
        } else {
            (offset, buf.len())
        };

        let config = self.config.lock().unwrap();
        let ptr = unsafe {
            std::mem::transmute::<&VirtioBlkConfig, &[u8; VirtioBlkConfig::SIZE]>(&config)
        };

        buf[0..size].copy_from_slice(&ptr[offset..(offset + size)]);
        size
    }

    fn get_thread_param(&self) -> Self::ThreadParam {
        VirtioBlkThreadParam {
            node: Arc::clone(&self.node.lock().unwrap()),
            drop_watchdog: Arc::clone(&self.drop_watchdog),
            read_only: self.read_only,
        }
    }

    fn move_node(&self, node: Arc<NodeUser>) -> Arc<NodeUser> {
        let old_node = std::mem::replace(&mut *self.node.lock().unwrap(), node);
        self.refresh_config();
        old_node
    }
}

impl VirtioDeviceThreadParam for VirtioBlkThreadParam {
    type ThreadData = VirtioBlkThreadData;

    fn into_data(self) -> BlockResult<VirtioBlkThreadData> {
        let queue = self.node.new_queue()?;
        Ok(VirtioBlkThreadData {
            node: Cell::new(Some(self.node)),
            queue: UnsafeCell::new(queue),
            _drop_watchdog: self.drop_watchdog,
            read_only: self.read_only,
            pre_move_node: Cell::new(None),
            pre_move_queue: Cell::new(None),
        })
    }
}

impl VirtioBlkThreadData {
    /// Only safe when `set_queue()` is used only while no requests are in flight.  `set_queue()`
    /// is marked unsafe, which is enough to have that condition be guaranteed.
    fn get_queue(&self) -> &IoQueue {
        unsafe { &*self.queue.get() }
    }

    /// Only safe when no requests are in flight.
    unsafe fn set_queue(&self, new_queue: IoQueue) -> IoQueue {
        std::mem::replace(unsafe { &mut *self.queue.get() }, new_queue)
    }

    /// Issue a request to the `IoQueue`.  Caller must have fetched the request header already.
    /// @req_hdr: Request header
    /// @buf: First unused buffer past the header (may be only a part of one descriptor, e.g. if
    ///       header and data are laid out consecutively in memory, such that the first descriptor
    ///       points to both)
    /// @iter: Available descriptor iterator for potential buffers past `buf`
    /// Return an async request (i.e. a `BlockFutureResult`) and a pointer/reference to where the
    /// request result is to be stored.
    fn do_request<'a, I: DescriptorIterator<'a>>(
        &'a self,
        req_hdr: &VirtioBlkReq,
        buf: &'a mut [u8],
        iter: &mut I,
    ) -> (BlockFutureResult<'a, ()>, IoOperationType, &'a mut u8) {
        match iter.next() {
            Some(next_buf) => {
                if iter.is_empty() && next_buf.len() == 1 {
                    // One 1-byte buffer past `buf`: `buf` is the whole data buffer, and the next
                    // one is the status byte
                    let (fut, op_type) =
                        self.do_simple_request(req_hdr, IoBufferMut::from_slice(buf));
                    (fut, op_type, &mut next_buf[0])
                } else {
                    // Not so cleanly split, so enter a loop to collect more buffers
                    let mut vec: IoVectorMut<'a> = IoVectorMut::with_capacity(8);
                    vec.push(buf);

                    // Keep one buffer not pushed (putting it into the vector requires converting
                    // it to an `IoSliceMut`, and popping it back somehow seems to destroy the
                    // lifetime, i.e. the Rust compiler then considers it a local variable/slice,
                    // so we cannot pass it to `do_vectored_request()`).
                    let mut back_buf: &'a mut [u8] = next_buf;
                    while let Some(buf) = iter.next() {
                        vec.push(back_buf);
                        if iter.is_empty() && buf.len() == 1 {
                            // Slightly fast path: This final 1-byte buffer is the status byte, the
                            // buffers before it are all the data buffers
                            let (fut, op_type) = self.do_vectored_request(req_hdr, vec);
                            return (fut, op_type, &mut buf[0]);
                        }
                        back_buf = buf;
                    }

                    let back_buf_len = back_buf.len();
                    if back_buf_len == 1 {
                        // Final buffer is a 1-byte buffer (the status byte).  We can get here if
                        // the device gave us 0-byte buffers, which are skipped by `iter.next()`,
                        // but still make `iter.is_empty()` return false.
                        let (fut, op_type) = self.do_vectored_request(req_hdr, vec);
                        (fut, op_type, &mut back_buf[0])
                    } else {
                        // Slow path: Inexplicably, the device gave us vectored data buffers, but
                        // still decided to include the status byte in the final buffer.
                        let (buf, status) = back_buf.split_at_mut(back_buf_len - 1);
                        vec.push(buf);
                        let (fut, op_type) = self.do_vectored_request(req_hdr, vec);
                        (fut, op_type, &mut status[0])
                    }
                }
            }

            None => {
                // Only a single buffer past the request header: Split it into the data part and
                // the status byte
                let (buf, status) = buf.split_at_mut(buf.len() - 1);
                let (fut, op_type) = self.do_simple_request(req_hdr, IoBufferMut::from_slice(buf));
                (fut, op_type, &mut status[0])
            }
        }
    }

    /// Issue a single-buffer (not vectored) request to the device
    /// @req_hdr: Virtio-blk request header
    /// @buf: Single data buffer
    fn do_simple_request<'a>(
        &'a self,
        req_hdr: &VirtioBlkReq,
        buf: IoBufferMut<'a>,
    ) -> (BlockFutureResult<'a, ()>, IoOperationType) {
        let req: BlockResult<VirtioBlkType> = u32::from(req_hdr.ty).try_into();
        let offset = u64::from(req_hdr.sector) << VIRTIO_BLK_SECTOR_BITS;

        match req {
            Ok(VirtioBlkType::In) => {
                let fut = self.get_queue().read(buf, offset);
                (fut, IoOperationType::Read)
            }
            Ok(VirtioBlkType::Out) => {
                let fut = if !self.read_only {
                    self.get_queue().write(buf.into_ref(), offset)
                } else {
                    Box::pin(async { Err(std::io::ErrorKind::ReadOnlyFilesystem.into()) })
                };
                (fut, IoOperationType::Write)
            }
            Ok(VirtioBlkType::Flush) => {
                let fut = self.get_queue().flush();
                (fut, IoOperationType::Flush)
            }
            Ok(VirtioBlkType::GetId) => {
                let fut = Box::pin(async {
                    let id_string = b"RSD\0";
                    let (head, tail) = buf.split_at(id_string.len());
                    head.into_slice().copy_from_slice(id_string);
                    tail.into_slice().fill(0);
                    Ok(())
                });
                (fut, IoOperationType::Other)
            }

            _ => {
                let fut = Box::pin(async { Err(std::io::ErrorKind::Unsupported.into()) });
                (fut, IoOperationType::Other)
            }
        }
    }

    /// Issue a multi-buffer (vectored) request to the device
    /// @req_hdr: Virtio-blk request header
    /// @bufv: Vector of data buffers
    fn do_vectored_request<'a>(
        &'a self,
        req_hdr: &VirtioBlkReq,
        bufv: IoVectorMut<'a>,
    ) -> (BlockFutureResult<'a, ()>, IoOperationType) {
        let req: BlockResult<VirtioBlkType> = u32::from(req_hdr.ty).try_into();
        let offset = u64::from(req_hdr.sector) << VIRTIO_BLK_SECTOR_BITS;

        match req {
            Ok(VirtioBlkType::In) => {
                let fut = self.get_queue().readv(bufv, offset);
                (fut, IoOperationType::Read)
            }
            Ok(VirtioBlkType::Out) => {
                let fut = if !self.read_only {
                    self.get_queue().writev(bufv.into_const(), offset)
                } else {
                    Box::pin(async { Err(std::io::ErrorKind::ReadOnlyFilesystem.into()) })
                };
                (fut, IoOperationType::Write)
            }
            Ok(VirtioBlkType::Flush) => {
                let fut = self.get_queue().flush();
                (fut, IoOperationType::Flush)
            }
            Ok(VirtioBlkType::GetId) => {
                let fut = Box::pin(async move {
                    let id_string = b"RSD\0";
                    let (mut head, mut tail) = bufv.split_at(id_string.len() as u64);
                    head.copy_from_slice(id_string);
                    tail.fill(0);
                    Ok(())
                });
                (fut, IoOperationType::Other)
            }

            _ => {
                let fut = Box::pin(async { Err(std::io::ErrorKind::Unsupported.into()) });
                (fut, IoOperationType::Other)
            }
        }
    }
}

impl VirtioDeviceThreadData for VirtioBlkThreadData {
    /// Fetch the request header and run the requested operation on the associated `IoQueue`
    fn request<'a, I: DescriptorIterator<'a>>(
        &'a self,
        iter: &mut I,
    ) -> BlockResult<BlockFutureResult<'a, ()>> {
        let first_buf = iter.next().ok_or(std::io::ErrorKind::InvalidInput)?;

        let (block_request, op_type, status) = match first_buf.len().cmp(&VirtioBlkReq::SIZE) {
            Equal => {
                // Safe because the buffer has the right size
                let req_hdr = unsafe { &*(first_buf.as_ptr() as *const VirtioBlkReq) };
                let buf = iter.next().ok_or(std::io::ErrorKind::InvalidInput)?;
                self.do_request(req_hdr, buf, iter)
            }

            Greater => {
                // Safe because the buffer is longer
                let req_hdr = unsafe { &*(first_buf.as_ptr() as *const VirtioBlkReq) };
                self.do_request(req_hdr, &mut first_buf[VirtioBlkReq::SIZE..], iter)
            }

            Less => {
                let mut buf = first_buf;
                let mut req_hdr_buf = [0u8; VirtioBlkReq::SIZE];
                let mut req_hdr_buf_ofs = 0;

                while req_hdr_buf_ofs < VirtioBlkReq::SIZE {
                    let this_size = std::cmp::min(buf.len(), VirtioBlkReq::SIZE - req_hdr_buf_ofs);
                    req_hdr_buf[req_hdr_buf_ofs..(req_hdr_buf_ofs + this_size)]
                        .copy_from_slice(&buf[..this_size]);
                    req_hdr_buf_ofs += this_size;
                    if req_hdr_buf_ofs < VirtioBlkReq::SIZE || this_size == buf.len() {
                        buf = iter.next().ok_or(std::io::ErrorKind::InvalidInput)?;
                    }
                }

                // Safe because the buffer has the right size
                let req_hdr = unsafe { &*(req_hdr_buf.as_ptr() as *const VirtioBlkReq) };
                self.do_request(req_hdr, buf, iter)
            }
        };

        Ok(Box::pin(async move {
            let result = block_request.await;

            match &result {
                Ok(()) => *status = VirtioBlkStatus::Ok as u8,
                Err(e) => {
                    let event = Events::BlockIoError {
                        device: String::from(self.get_queue().node_user().parent_name()),
                        node_name: self.get_queue().node().name.clone(),
                        operation: op_type,
                        action: BlockErrorAction::Report,
                        nospace: e.get_inner().kind() == std::io::ErrorKind::StorageFull,
                        reason: e.to_string(),
                    };
                    broadcast_event(qmp::Event::new(event));

                    if e.get_inner().kind() == std::io::ErrorKind::Unsupported {
                        *status = VirtioBlkStatus::Unsupp as u8;
                    } else {
                        *status = VirtioBlkStatus::Ioerr as u8;
                    }
                }
            }

            result
        }))
    }

    fn handle_monitor_request(&self, req: VirtqMonitorRequest) {
        match req {
            VirtqMonitorRequest::Quiesce(_) => unreachable!(),
            VirtqMonitorRequest::Unquiesce => unreachable!(),
            VirtqMonitorRequest::Stop(_) => unreachable!(),

            VirtqMonitorRequest::ChangeChildDo {
                node: new_node_user,
                read_only,
                result: ack,
                connected,
            } => {
                if read_only != self.read_only {
                    ack.try_send(Err(
                        "Cannot change vhost-user-blk export's read-only state".into()
                    ))
                    .unwrap();
                    return;
                }

                let old_node_user = self.node.take().unwrap();
                let new_node = new_node_user.node();
                let old_node = old_node_user.node();

                // When not connected, we still need to adjust the config space.  backend.rs will
                // take care of that by calling `VirtioBlk::move_node()`.
                if old_node.size() != new_node.size() && connected {
                    let msg =
                        format!(
                        "vhost-user-blk cannot change exported node's metadata after creating the \
                         export, but the new node's (\"{}\") size ({}) differs from that of the \
                         current one (\"{}\", {})",
                        new_node.name, new_node.size(), old_node.name, old_node.size()
                    );
                    self.node.replace(Some(old_node_user));
                    ack.try_send(Err(msg.into())).unwrap();
                    return;
                }

                if new_node.request_align() > old_node.request_align() {
                    let msg = format!(
                        "vhost-user-blk cannot change exported node's metadata after creating the \
                         export, but the new node's (\"{}\") request alignment ({}) would exceed \
                         that of the current one (\"{}\", {})",
                        new_node.name,
                        new_node.request_align(),
                        old_node.name,
                        old_node.request_align()
                    );
                    self.node.replace(Some(old_node_user));
                    ack.try_send(Err(msg.into())).unwrap();
                    return;
                }

                let result = match new_node_user.new_queue() {
                    Ok(q) => {
                        self.node.replace(Some(new_node_user));
                        // Safe because the monitor guarantees it has quiesced this export
                        let old_queue = unsafe { self.set_queue(q) };

                        self.pre_move_node.replace(Some(old_node_user));
                        self.pre_move_queue.replace(Some(old_queue));

                        Ok(())
                    }
                    Err(err) => {
                        self.node.replace(Some(old_node_user));
                        Err(err)
                    }
                };
                ack.try_send(result).unwrap();
            }

            VirtqMonitorRequest::ChangeChildClean(ack) => {
                self.pre_move_node.take();
                self.pre_move_queue.take();
                ack.try_send(()).unwrap();
            }

            VirtqMonitorRequest::ChangeChildRollBack(ack) => {
                // Either both are set (we have successfully changed the nodes, but other queues
                // did not) or none are (we failed to change the node, so there is nothing to roll
                // back)
                match (self.pre_move_node.take(), self.pre_move_queue.take()) {
                    (Some(old_node), Some(old_queue)) => {
                        self.node.replace(Some(old_node));
                        // Safe because the monitor guarantees it has quiesced this export
                        unsafe { self.set_queue(old_queue) };
                    }

                    (None, None) => (),

                    _ => unreachable!(),
                }

                ack.try_send(()).unwrap();
            }
        }
    }
}
