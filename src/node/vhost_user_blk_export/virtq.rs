/**
 * Code to operate a vhost-user backend virtq, above the actual vrings (which are implemented in
 * packed_vq.rs and split_vq.rs).
 */
use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, FlatSize, OwnedFd, ThreadBound};
use crate::monitor;
use crate::node::vhost_user_blk_export::backend::VirtqMonitorRequest;
use crate::node::vhost_user_blk_export::guest_memory::GuestMemory;
use crate::node::vhost_user_blk_export::packed_vq::VirtqPackedRings;
use crate::node::vhost_user_blk_export::protocol::VhostUserFeature;
use crate::node::vhost_user_blk_export::split_vq::VirtqSplitRings;
use crate::node::vhost_user_blk_export::traits::{
    VirtioDevice, VirtioDeviceThreadData, VirtioDeviceThreadParam,
};
use std::collections::LinkedList;
use std::future::Future;
use std::pin::Pin;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use std::task::{Context, Poll};
use tokio::io::unix::AsyncFd;
use tokio::sync::mpsc;

/// The addresses we get from the vhost-user front-end (these are host user-space addresses, i.e.
/// as mapped into qemu)
#[derive(Clone, Debug)]
pub struct VirtqRingAddresses {
    /// The descriptor ring
    pub descriptor_ring: u64,
    /// Device-writable ring.  For a split virt queue, this is the used ring.  For a packed virt
    /// queue, this is the device's event suppression field.
    pub used_ring: u64,
    /// Driver-writable ring.  For a split virt queue, this is the available ring.  For a packed
    /// virt queue, this is the driver's event suppression field.
    pub available_ring: u64,
}

/// Notifications to be sent to a virt queue handling thread
#[derive(Clone, Debug)]
pub enum VirtqThreadNotification {
    /// Announce the features negotiated with the guest drivers
    SetFeatures(u64),
    /// Invalidate the current guest memory mappings
    InvalidateMemory,
    /// Update the virt queue ring addresses
    UpdateRings(VirtqRingAddresses),
    /// Set the fetch/store index
    SetBase(u32),
    /// Get the fetch index
    GetBase(mpsc::Sender<u32>),
    /// Set the kick FD (i.e. the eventfd on which to listen for notifications from the guest
    /// driver)
    SetKickFd(Option<OwnedFd>),
    /// Set the call FD (i.e. the eventfd on which the guest driver listens for notifications)
    SetCallFd(Option<OwnedFd>),
    /// Set the error FD
    SetErrFd(Option<OwnedFd>),
    /// Pass guest memory mappings
    SetGuestMemory(Arc<GuestMemory>),
    /// Set the virt queue queue size (number of descriptors in any ring)
    SetQueueSize(usize),
    /// Stop the virt queue
    Stop,
    /// Invalidate client-related variables (i.e. FDs)
    InvalidateClient,
    /// Enable or disable the virt queue (enabled by default)
    Enable(bool),
    /// Quit and drop the virt queue
    Quit,
}

/// Unifies both possible virtq vring implementations (split or packed)
enum VirtqRings<'a> {
    Split(VirtqSplitRings<'a>),
    Packed(VirtqPackedRings<'a>),
}

/// Configuration for possible call batching
#[derive(Clone)]
pub struct CallBatching {
    /// Number of buffers to complete before sending a notification to the guest
    pub batch_size: usize,

    /// Number of seconds to wait before sending a notification to the guest, regardless of whether
    /// the batch size has been reached or not
    pub max_latency: f32,
}

/// An operational running virt queue
pub struct VirtqOperational<'a, T: VirtioDeviceThreadData> {
    /// Reference to the associated device
    device: &'a T,

    /// Requests sent to the vhost-user-blk export from the monitor
    monitor_request: mpsc::UnboundedReceiver<VirtqMonitorRequest>,

    /// Technical parts of the virt queue
    rings: VirtqRings<'a>,
    /// `AsyncFd` that waits on notifications from the guest driver about available descriptors
    kickfd: AsyncFd<OwnedFd>,
    /// Event FD for notifying the guest driver about used descriptors
    callfd: Option<OwnedFd>,

    /// Requests currently in flight (oldest at the front, newest at the back)
    in_flight: LinkedList<InFlight<'a>>,
    /// Whether the queue is to be quiesced
    quiesced: bool,
    /// Once a quiesced state is reached, acknowledge it through here
    quiesce_ack: Option<mpsc::Sender<()>>,

    /// Call batching parameters (for delaying notifications to the guest driver until a certain
    /// number of descriptors can be collectively reported as used, or until a certain latency has
    /// been reached)
    call_batching: CallBatching,
    /// Polling duration (i.e. how many seconds to wait after something has happened before
    /// returning `Poll::Pending` from the polling function, in order to reduce latency and improve
    /// throughput, at the cost of CPU usage)
    poll_duration: f32,

    /// Handle on guest memory
    guest_mem: Arc<GuestMemory>,
}

/// General data for a virt queue thread, regardless of whether it is running or not
pub struct VirtqThread<'a, T: VirtioDeviceThreadData> {
    /// If the virtq is running, this field is set
    virtq: Option<VirtqOperational<'a, T>>,
    /// Reference to the associated device (better than ownership, because this way the owner of
    /// the `VirtqThread` object must guarantee this reference lives long enough)
    device: &'a T,

    /// Requests sent to the vhost-user-blk export from the monitor (moved to `virtq` once created)
    monitor_request: Option<mpsc::UnboundedReceiver<VirtqMonitorRequest>>,

    /// Requests from the virtio backend
    backend_request: mpsc::UnboundedReceiver<(VirtqThreadNotification, mpsc::Sender<()>)>,

    /// Virtq ring addresses
    ring_addrs: Option<VirtqRingAddresses>,

    /// Event FD for notifications from guest driver to device
    kickfd: Option<AsyncFd<OwnedFd>>,
    /// Whether `callfd` has been set or not; we can work without a `callfd`, but we should not
    /// start the virt queue before we know whether the guest driver wants notifications or not
    callfd_set: bool,
    /// Event FD for notifications from device to guest driver
    callfd: Option<OwnedFd>,

    /// Features as negotiated with the guest driver
    features: u64,
    /// Queue size (number of descriptors in any ring)
    queue_size: usize,
    /// Initial fetch/store index
    avail_base: u32,
    /// Whether the device is enabled
    enabled: bool,

    /// Call batching parameters (see `VirtqOperational.call_batching`)
    call_batching: CallBatching,
    /// Polling duration (see `VirtqOperational.poll_duration`)
    poll_duration: f32,

    /// Handle on guest memory
    guest_mem: Option<Arc<GuestMemory>>,

    /// Whether the queue is to be quiesced.  Not auto-set while the queue is running, so care must
    /// be taken to copy it from the queue when the queue is stopped.
    quiesced: bool,
}

/// Describes buffers (descriptors) used by a request (i.e. one descriptor chain), which are to be
/// returned to the guest as used descriptors when the request is done.  Contains only root
/// descriptors, i.e. not indirectly reference descriptors.
pub enum ReqBuffers {
    /// No descripts (used only for initialization, as every request has at least one buffer)
    Empty,
    /// A single buffer, with its ID (packed vq: ID; split VQ: avail index) and total number of
    /// writable/written bytes
    Single((u16, usize)),
    /// List of buffers with ID and number of writable bytes each
    List(Vec<(u16, usize)>),
}

/// Describes an in-flight request
pub struct InFlight<'a> {
    /// Descriptor chain to be returned to the guest when the request is done
    pub buffers: ReqBuffers,
    /// The ongoing request
    pub req: BlockFutureResult<'a, ()>,
}

/// Handle on a virt queue to be used by the vhost-user backend implementation
pub struct VhostUserVirtq {
    /// Handle to the thread in which this queue runs
    _thread_handle: monitor::ThreadHandle,
    /// A notifier object to communicate with the virt queue thread
    thread_notifier: mpsc::UnboundedSender<(VirtqThreadNotification, mpsc::Sender<()>)>,
}

impl<'a> VirtqRings<'a> {
    /// Fetch available descriptors and launch new requests.
    /// If `set_event` is false, do not enable event notifications from the guest upon the
    /// availability of new descriptors.  If true, enable such notifications, but preferably only
    /// if no new requests have been fetched this time.  (The polling loop will run this function
    /// until no requests were fetched, and then enable notifications before returning
    /// `Poll::Pending`.  If at this point new requests are fetched, the loop will continue, so
    /// that enabling notifications is not necessary yet.)
    /// Return true if any descriptors were processed.
    fn poll_avail<T: VirtioDeviceThreadData>(
        &mut self,
        device: &'a T,
        in_flight: &mut LinkedList<InFlight<'a>>,
        set_event: bool,
    ) -> bool {
        match self {
            VirtqRings::Split(split) => split.poll_avail(device, in_flight, set_event),
            VirtqRings::Packed(packed) => packed.poll_avail(device, in_flight, set_event),
        }
    }

    /// Mark the given descriptor as used
    fn settle(&mut self, id: u16, len: usize) {
        match self {
            VirtqRings::Split(split) => split.settle(id, len),
            VirtqRings::Packed(packed) => packed.settle(id, len),
        }
    }

    /// Check whether we need to notify (call) the guest driver about a number of completed
    /// (settled) descriptors; return true if so.
    fn notify_guest(&mut self, completed: usize) -> bool {
        match self {
            VirtqRings::Split(split) => split.notify_guest(completed),
            VirtqRings::Packed(packed) => packed.notify_guest(completed),
        }
    }

    fn get_base(&self) -> u32 {
        match self {
            VirtqRings::Split(split) => split.get_base(),
            VirtqRings::Packed(packed) => packed.get_base(),
        }
    }
}

impl<'a, T: VirtioDeviceThreadData> VirtqOperational<'a, T> {
    /// Helper function wrapping `VirtqRings.poll_avail()`.  Fetches available descriptors and
    /// launches new requests.
    fn poll_avail(&mut self, set_event: bool) -> bool {
        self.rings
            .poll_avail(self.device, &mut self.in_flight, set_event)
    }

    /// Notify the guest driver that some descriptors have been marked as used.
    fn call(&self) {
        if let Some(callfd) = self.callfd.as_ref() {
            let buf = [1u64];
            unsafe {
                libc::write(**callfd, buf.as_ptr() as *const libc::c_void, u64::SIZE);
            }
        }
    }

    /// Polling function for the virt queue.  Fetch new descriptors from the guest, create new
    /// requests, poll all in-flight requests, and return finished requests to the guest.
    /// NOTE: In contrast to Rust convention, this function will block for `self.poll_duration`
    /// (and possibly `self.call_batching.max_latency` if greater).
    fn poll(&mut self, cx: &mut Context<'_>) -> Poll<()> {
        let mut last_activity = std::time::Instant::now();

        if !self.quiesced {
            while let Poll::Ready(Ok(mut guard)) = self.kickfd.poll_read_ready(cx) {
                guard.clear_ready();
                self.poll_avail(false);
            }
        }

        let mut last_completion = last_activity;

        let mut completed: usize = 0;

        // Interleave settling ongoing requests and fetching new requests from the guest until the
        // guest has no new requests for us anymore.  (If `self.poll_duration` is set, we will
        // continue polling even if the guest has no new requests for us.)
        loop {
            let loop_now = std::time::Instant::now();

            let mut in_flight_cursor = self.in_flight.cursor_front_mut();
            while let Some(in_flight) = in_flight_cursor.current() {
                if let Poll::Ready(res) = Future::poll(in_flight.req.as_mut(), cx) {
                    let success = res.is_ok();

                    let mut in_flight = in_flight_cursor.remove_current().unwrap();
                    match &mut in_flight.buffers {
                        ReqBuffers::Empty => unreachable!(),

                        ReqBuffers::Single((id, len)) => {
                            // TODO: Find out actual amount written
                            let len = if success { *len } else { 0 };
                            self.rings.settle(*id, len);
                            completed += 1;
                        }

                        ReqBuffers::List(list) => {
                            let entries = list.len();
                            for (id, len) in list {
                                // TODO: Find out actual amount written
                                let len = if success { *len } else { 0 };
                                // Note: With a packed virtq, we are not required to write any but
                                // the first element in the list, but from the specification, I do
                                // not quite understand how to calculate `len` in sucha case, so we
                                // just write everything here
                                self.rings.settle(*id, len);
                            }
                            completed += entries;
                        }
                    }

                    last_completion = loop_now;
                } else {
                    in_flight_cursor.move_next();
                }
            }

            if completed >= self.call_batching.batch_size
                || (completed > 0
                    && (loop_now - last_completion).as_secs_f32() >= self.call_batching.max_latency)
            {
                std::sync::atomic::fence(Ordering::SeqCst);
                if self.rings.notify_guest(completed) {
                    self.call();
                }

                completed = 0;
                last_activity = loop_now;
            }

            if !self.quiesced {
                if self.poll_avail(false) {
                    last_activity = loop_now;
                } else if completed == 0 {
                    let elapsed = loop_now - last_activity;
                    if elapsed.as_secs_f32() >= self.poll_duration && !self.poll_avail(true) {
                        break;
                    }
                }
            } else if completed == 0 {
                break;
            }

            if VirtqThread::poll_monitor_requests(None, Some(self), cx).is_ready() {
                return Poll::Ready(());
            }
        }

        if self.in_flight.is_empty() {
            if let Some(ack) = self.quiesce_ack.take() {
                ack.try_send(()).unwrap();
            }
        }

        Poll::Pending
    }
}

impl<'a, T: VirtioDeviceThreadData> VirtqThread<'a, T> {
    /// Attempt to start the virt queue.  There are three possible outcomes:
    /// - `Ok(false)`: Virt queue cannot be started, e.g. because it is already running, or because
    ///                some configuration parameters are yet to be received over the vhost-user
    ///                control channel.
    /// - `Err(_)`:    Virt queue should be startable, but attempting to do so produced an error.
    /// - `Ok(true)`:  Virt queue started successfully.
    fn try_start(&mut self) -> BlockResult<bool> {
        if self.virtq.is_some() {
            return Ok(false);
        }

        if self.ring_addrs.is_none()
            || self.kickfd.is_none()
            || !self.callfd_set
            || self.guest_mem.is_none()
        {
            return Ok(false);
        }

        let guest_mem = self.guest_mem.take().unwrap();

        // Safe as long as the `guest_mem` object in `VirtqOperational` is only updated through
        // `update_memory()`
        let guest_mem_ref =
            unsafe { std::mem::transmute::<&'_ GuestMemory, &'a GuestMemory>(guest_mem.as_ref()) };

        let rings = if self.features & (VhostUserFeature::RingPacked as u64) != 0 {
            VirtqPackedRings::from_addresses(
                self.ring_addrs.as_ref().unwrap(),
                guest_mem_ref,
                self.queue_size,
                self.avail_base,
                self.features,
                None,
            )
            .map(VirtqRings::Packed)
        } else {
            VirtqSplitRings::from_addresses(
                self.ring_addrs.as_ref().unwrap(),
                guest_mem_ref,
                self.queue_size,
                self.avail_base,
                self.features,
                None,
            )
            .map(VirtqRings::Split)
        };

        let rings = match rings {
            Ok(r) => r,
            Err(e) => {
                self.guest_mem.replace(guest_mem);
                return Err(e);
            }
        };

        let virtq = VirtqOperational {
            device: self.device,

            monitor_request: self.monitor_request.take().unwrap(),

            rings,
            kickfd: self.kickfd.take().unwrap(),
            callfd: self.callfd.take(),

            in_flight: LinkedList::new(),
            quiesced: self.quiesced,
            quiesce_ack: None,

            call_batching: self.call_batching.clone(),
            poll_duration: self.poll_duration,

            guest_mem,
        };

        self.virtq.replace(virtq);

        Ok(true)
    }

    /// If the virt queue is operational, update the memory mappings
    fn update_memory(&mut self, guest_mem: Arc<GuestMemory>) -> BlockResult<()> {
        let virtq = match self.virtq.as_mut() {
            Some(virtq) => virtq,
            None => return Ok(()),
        };

        // Safe as long as the `guest_mem` object in `VirtqOperational` is only updated through
        // this function
        let guest_mem_ref =
            unsafe { std::mem::transmute::<&'_ GuestMemory, &'a GuestMemory>(guest_mem.as_ref()) };

        let rings = if self.features & (VhostUserFeature::RingPacked as u64) != 0 {
            let previous = match &virtq.rings {
                VirtqRings::Packed(r) => Some(r),
                _ => None,
            };

            VirtqPackedRings::from_addresses(
                self.ring_addrs.as_ref().unwrap(),
                guest_mem_ref,
                self.queue_size,
                self.avail_base,
                self.features,
                previous,
            )
            .map(VirtqRings::Packed)
        } else {
            let previous = match &virtq.rings {
                VirtqRings::Split(r) => Some(r),
                _ => None,
            };

            VirtqSplitRings::from_addresses(
                self.ring_addrs.as_ref().unwrap(),
                guest_mem_ref,
                self.queue_size,
                self.avail_base,
                self.features,
                previous,
            )
            .map(VirtqRings::Split)
        };

        match rings {
            Ok(r) => virtq.rings = r,
            Err(e) => {
                self.stop();
                return Err(e);
            }
        };

        virtq.guest_mem = guest_mem;

        Ok(())
    }

    /// Stop the virt queue if running.
    fn stop(&mut self) {
        if let Some(mut virtq) = self.virtq.take() {
            self.kickfd.replace(virtq.kickfd);
            if let Some(callfd) = virtq.callfd.take() {
                self.callfd.replace(callfd);
            }

            self.guest_mem.replace(virtq.guest_mem);

            self.monitor_request.replace(virtq.monitor_request);
            self.quiesced = virtq.quiesced;

            self.avail_base = virtq.rings.get_base();

            // TODO: Cancel ongoing requests (implicitly done the Rust way by dropping `virtq` and
            // `virtq.in_flight` with it, but we should probably have explicit cancel
            // infrastructure instead of relying on dropped requests being auto-cancelled just
            // fine).
        }
    }

    /// Either of `thread` or `virtq` must be present.
    fn poll_monitor_requests(
        mut thread: Option<&mut Self>,
        mut virtq: Option<&mut VirtqOperational<'a, T>>,
        cx: &mut Context<'_>,
    ) -> Poll<()> {
        loop {
            let req = if let Some(virtq) = virtq.as_mut() {
                virtq.monitor_request.poll_recv(cx)
            } else {
                thread
                    .as_mut()
                    .unwrap()
                    .monitor_request
                    .as_mut()
                    .unwrap()
                    .poll_recv(cx)
            };

            let req = match req {
                Poll::Pending => return Poll::Pending,
                Poll::Ready(Some(req)) => req,
                Poll::Ready(None) => return Poll::Ready(()),
            };

            match req {
                VirtqMonitorRequest::Quiesce(ack) => {
                    if let Some(virtq) = virtq.as_mut() {
                        virtq.quiesced = true;
                        virtq.quiesce_ack.replace(ack);
                    } else {
                        ack.try_send(()).unwrap();
                        thread.as_mut().unwrap().quiesced = true;
                    }
                }

                VirtqMonitorRequest::Unquiesce => {
                    if let Some(virtq) = virtq.as_mut() {
                        virtq.quiesced = false;
                    } else {
                        thread.as_mut().unwrap().quiesced = false;
                    }
                }

                VirtqMonitorRequest::Stop(ack) => {
                    let _: Result<(), _> = ack.try_send(());
                    return Poll::Ready(());
                }

                other => {
                    if let Some(virtq) = virtq.as_mut() {
                        virtq.device.handle_monitor_request(other);
                    } else {
                        thread
                            .as_mut()
                            .unwrap()
                            .device
                            .handle_monitor_request(other);
                    }
                }
            }
        }
    }
}

impl<'a, T: VirtioDeviceThreadData> Future for VirtqThread<'a, T> {
    type Output = ();

    /// Polling function for the virt queue thread.  Fetch and process notifications from the
    /// control thread, and then poll the virt queue itself if running.
    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
        while let Poll::Ready(req) = self.backend_request.poll_recv(cx) {
            let (req, ack) = req.unwrap();
            let try_start = match req {
                VirtqThreadNotification::SetFeatures(features) => {
                    self.stop();
                    self.features = features;

                    false
                }

                VirtqThreadNotification::InvalidateMemory => {
                    self.stop();
                    self.guest_mem.take();

                    false
                }

                VirtqThreadNotification::UpdateRings(addrs) => {
                    self.stop();
                    self.ring_addrs.replace(addrs);

                    true
                }

                VirtqThreadNotification::GetBase(sender) => {
                    let _: () = sender.try_send(self.avail_base).unwrap();
                    false
                }

                VirtqThreadNotification::SetBase(base) => {
                    self.stop();

                    self.avail_base = base;

                    false
                }

                VirtqThreadNotification::SetKickFd(Some(fd)) => {
                    let fd = AsyncFd::new(fd).unwrap();
                    if let Some(virtq) = self.virtq.as_mut() {
                        virtq.kickfd = fd;
                    } else {
                        self.kickfd.replace(fd);
                    }

                    true
                }

                VirtqThreadNotification::SetKickFd(None) => {
                    todo!("Pure polling is not supported")
                }

                VirtqThreadNotification::SetCallFd(Some(fd)) => {
                    if let Some(virtq) = self.virtq.as_mut() {
                        virtq.callfd.replace(fd);
                    } else {
                        self.callfd.replace(fd);
                        self.callfd_set = true;
                    }

                    true
                }

                VirtqThreadNotification::SetCallFd(None) => {
                    if let Some(virtq) = self.virtq.as_mut() {
                        virtq.callfd.take();
                    }

                    self.callfd.take();
                    self.callfd_set = true;

                    true
                }

                VirtqThreadNotification::SetErrFd(_) => false,

                VirtqThreadNotification::SetGuestMemory(guest_mem) => {
                    if let Err(err) = self.update_memory(Arc::clone(&guest_mem)) {
                        eprintln!(
                            "[vhost-user] Error attempting to update a virt queue's \
                             memory mappings: {}",
                            err
                        );
                    }
                    self.guest_mem.replace(guest_mem);

                    true
                }

                VirtqThreadNotification::SetQueueSize(qs) => {
                    self.stop();
                    self.queue_size = qs;

                    true
                }

                VirtqThreadNotification::Stop => {
                    self.stop();

                    false
                }

                VirtqThreadNotification::InvalidateClient => {
                    self.stop();

                    self.ring_addrs.take();
                    self.kickfd.take();
                    self.callfd_set = false;
                    self.callfd.take();

                    false
                }

                VirtqThreadNotification::Enable(enable) => {
                    self.enabled = enable;

                    false
                }

                VirtqThreadNotification::Quit => {
                    self.stop();
                    let _: Result<(), _> = ack.try_send(());
                    return Poll::Ready(());
                }
            };

            // Caller is responsible to pass a channel that is large enough for all the acks it
            // expects
            let _: Result<(), _> = ack.try_send(());

            if try_start {
                if let Err(e) = self.try_start() {
                    eprintln!("[vhost-user] Error attempting to start a virt queue: {}", e);
                }
            }
        }

        match self.virtq.as_mut() {
            Some(virtq) => {
                if Self::poll_monitor_requests(None, Some(virtq), cx).is_ready() {
                    return Poll::Ready(());
                }
            }
            None => {
                if Self::poll_monitor_requests(Some(&mut self), None, cx).is_ready() {
                    return Poll::Ready(());
                }
            }
        }

        if self.enabled {
            if let Some(virtq) = self.virtq.as_mut() {
                virtq.poll(cx)
            } else {
                Poll::Pending
            }
        } else {
            Poll::Pending
        }
    }
}

impl VhostUserVirtq {
    /// Create a new virt queue thread, and return a handle to it for the vhost-user backend to use
    pub async fn new<V: VirtioDevice>(
        v: &V,
        monitor_request: mpsc::UnboundedReceiver<VirtqMonitorRequest>,
        thread_id: Option<String>,
        virtio_features: u64,
        call_batching: CallBatching,
        poll_duration: f32,
    ) -> BlockResult<Self> {
        let (backend_request_s, backend_request_r) = mpsc::unbounded_channel();

        let thread = monitor::monitor().get_thread_from_opt(&thread_id)?;

        let device_thread_param = v.get_thread_param();
        let wrapped_device_thread_data = thread
            .run(move || -> BlockFutureResult<_> {
                Box::pin(async move {
                    let data = device_thread_param.into_data()?;
                    // Safe because we will immediately unwrap this in `thread.spawn()`, and it
                    // then remains in that thread until it is dropped
                    Ok(unsafe { ThreadBound::new_unsafe(data) })
                })
            })
            .await?;

        thread.spawn(move || {
            Box::pin(async move {
                let device = wrapped_device_thread_data.unwrap();

                VirtqThread {
                    virtq: None,
                    device: &device,

                    monitor_request: Some(monitor_request),
                    backend_request: backend_request_r,

                    ring_addrs: None,

                    kickfd: None,
                    callfd_set: false,
                    callfd: None,

                    features: virtio_features,
                    queue_size: 1,
                    avail_base: 0,
                    enabled: true,

                    call_batching,
                    poll_duration,

                    guest_mem: None,

                    quiesced: false,
                }
                .await
            })
        });

        Ok(VhostUserVirtq {
            _thread_handle: thread,
            thread_notifier: backend_request_s,
        })
    }

    /// Send some notification to the virt queue thread
    fn do_notify(&self, notification: VirtqThreadNotification, ack: mpsc::Sender<()>) {
        let _: Result<(), _> = self.thread_notifier.send((notification, ack));
    }

    /// Send a notification to the virt queue thread, and wait for it to be acknowledged
    pub async fn notify(&self, notification: VirtqThreadNotification) {
        let (ack_s, mut ack_r) = mpsc::channel::<()>(1);
        self.do_notify(notification, ack_s);
        // Ignore if the channel was closed before an ACK was sent
        let _: Option<()> = ack_r.recv().await;
    }

    pub async fn broadcast<'a, I: Iterator<Item = &'a Self> + ExactSizeIterator>(
        queues: I,
        notification: VirtqThreadNotification,
    ) {
        let count = queues.len();
        let (ack_s, mut ack_r) = mpsc::channel::<()>(count);
        for q in queues {
            q.do_notify(notification.clone(), ack_s.clone());
        }
        for _ in 0..count {
            // Ignore if the channel was closed before more events are available
            let _: Option<()> = ack_r.recv().await;
        }
    }
}

impl Drop for VhostUserVirtq {
    fn drop(&mut self) {
        let (ack_s, _) = mpsc::channel::<()>(1);
        let _: Result<(), _> = self
            .thread_notifier
            .send((VirtqThreadNotification::Quit, ack_s));
    }
}

impl ReqBuffers {
    /// Return the number of buffers (descriptors) in this object
    pub fn len(&self) -> usize {
        match self {
            ReqBuffers::Empty => 0,
            ReqBuffers::Single(_) => 1,
            ReqBuffers::List(list) => list.len(),
        }
    }
}
