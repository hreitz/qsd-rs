use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, IoBuffer, IoBufferMut, IoBufferRef, ThreadBound};
use crate::monitor::{self, qmp, ThreadHandle};
use crate::node::exports::{self, BackgroundOpReq};
use crate::node::{IoQueue, NodePerm, NodeUser, NodeUserBuilder, PollableNodeStopMode};
use crate::server::ServerNode;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::future::Future;
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};
use tokio::sync::{mpsc, oneshot};

pub type Config = exports::Config<BenchConfig>;
pub type Data = exports::Data<BenchConfig>;

#[derive(Copy, Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum IoRequestType {
    Read,
    Write,
    Flush,
}

/// Options that cannot be changed via reopen
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct BenchConfig {
    request_type: IoRequestType,
    simultaneous: Option<usize>,
    total: Option<usize>,
    request_size: Option<usize>,
    #[serde(default)]
    exit_when_done: bool,
    #[serde(default)]
    random: bool,
    #[serde(default)]
    pattern: Option<u8>,
    offset: Option<u64>,
    step: Option<u64>,

    iothread: Option<String>,
}

pub struct BenchBackgroundOpData {
    rio: ThreadBound<RandomIo>,
    thread: ThreadHandle,
}

pub struct RequestSlot {
    future: Option<BlockFutureResult<'static, ()>>,
    buffer: IoBuffer,
}

impl RequestSlot {
    fn with_buf(alignment: usize, length: usize, pattern: u8) -> BlockResult<Self> {
        let mut buffer = IoBuffer::new(length, alignment)?;
        buffer.as_mut().into_slice().fill(pattern);

        Ok(RequestSlot {
            future: None,
            buffer,
        })
    }
}

pub struct RandomIo {
    export_id: String,
    queue: IoQueue,
    pre_reopen_queue: Option<IoQueue>,

    reqs: Vec<RequestSlot>,
    simultaneous: usize,
    poll_index: usize,

    request_type: IoRequestType,
    request_size: usize,
    total: usize,
    remaining_to_submit: usize,
    remaining_to_settle: usize,

    random: Option<fastrand::Rng>,

    offset: u64,
    step: u64,
    max_offset: u64,
    compare_pattern: Option<u8>,

    channel: mpsc::UnboundedReceiver<BackgroundOpReq>,
    quiesced: bool,
    quiesce_ack: Option<oneshot::Sender<()>>,

    exit_when_done: bool,
}

impl RandomIo {
    fn poll_node_channel(&mut self, cx: &mut Context<'_>) -> Poll<usize> {
        while let Poll::Ready(req) = self.channel.poll_recv(cx) {
            match req.unwrap() {
                BackgroundOpReq::Quiesce(ack) => {
                    let existing = self.quiesce_ack.replace(ack);
                    assert!(existing.is_none());
                    self.quiesced = true;
                }

                BackgroundOpReq::Unquiesce => {
                    self.quiesced = false;
                }

                BackgroundOpReq::Stop(mode, ack) => match mode {
                    PollableNodeStopMode::Safe => {
                        ack.send(Err("'safe' stop mode not supported by bench node".into()))
                            .unwrap();
                    }
                    PollableNodeStopMode::Hard => {
                        ack.send(Ok(())).unwrap();
                        return Poll::Ready(self.total - self.remaining_to_settle);
                    }
                    PollableNodeStopMode::CopyComplete { switch_over: _ } => {
                        ack.send(Err(
                            "'copy-complete' stop mode not supported by bench node".into()
                        ))
                        .unwrap();
                    }
                },

                BackgroundOpReq::ChangeChildDo {
                    exported: node,
                    read_only,
                    result: ack,
                } => {
                    if read_only && self.request_type == IoRequestType::Write {
                        ack.send(Err("Cannot make a writing bench node read-only".into()))
                            .unwrap();
                        continue;
                    }

                    let result = match node.new_queue() {
                        Ok(new_queue) => {
                            let old_queue = std::mem::replace(&mut self.queue, new_queue);
                            self.pre_reopen_queue.replace(old_queue);
                            Ok(())
                        }
                        Err(err) => Err(err),
                    };
                    ack.send(result).unwrap();
                }

                BackgroundOpReq::ChangeChildClean(ack) => {
                    self.pre_reopen_queue.take();
                    ack.send(()).unwrap();
                }

                BackgroundOpReq::ChangeChildRollBack(ack) => {
                    if let Some(old_queue) = self.pre_reopen_queue.take() {
                        self.queue = old_queue;
                    }
                    ack.send(()).unwrap();
                }
            }
        }

        Poll::Pending
    }

    async fn run(self) -> BlockResult<()> {
        let export_id = self.export_id.clone();
        let exit_when_done = self.exit_when_done;

        let start = std::time::Instant::now();
        let done = self.await;
        let end = std::time::Instant::now();

        let duration = end - start;

        let event = qmp::Events::BenchmarkDone {
            id: export_id,
            total: done,
            duration: duration.as_secs_f64(),
            iops: done as f64 / duration.as_secs_f64(),
        };
        monitor::broadcast_event(qmp::Event::new(event));

        if exit_when_done {
            std::process::exit(0);
        }

        Ok(())
    }
}

impl Future for RandomIo {
    type Output = usize;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let request_type = self.request_type;

        let mut continuously_pending = 0;
        // Incremented on each request slot checked.  When this reaches `self.simultaneous`, we
        // will poll `self.request`, and reset this to 0, so `self.request` is checked regularly,
        // but not too often.
        let mut poll_req_counter = 0;

        if let Poll::Ready(c) = self.poll_node_channel(cx) {
            return Poll::Ready(c);
        }

        while continuously_pending < self.simultaneous && self.remaining_to_settle > 0 {
            if poll_req_counter == self.simultaneous {
                poll_req_counter = 0;
                if let Poll::Ready(c) = self.poll_node_channel(cx) {
                    return Poll::Ready(c);
                }
            }

            let i = self.poll_index;
            self.poll_index = (self.poll_index + 1) % self.simultaneous;
            continuously_pending += 1;
            poll_req_counter += 1;

            if let Some(fut) = self.reqs[i].future.as_mut() {
                match Future::poll(fut.as_mut(), cx) {
                    Poll::Pending => continue,
                    Poll::Ready(Err(e)) => eprintln!("I/O error: {}", e),
                    Poll::Ready(Ok(())) => (),
                }
                self.remaining_to_settle -= 1;
                self.reqs[i].future.take();
            }

            if self.remaining_to_submit == 0 || self.quiesced {
                continue;
            }

            let max_ofs = self.max_offset;
            let rs = self.request_size;
            let step = self.step;
            let ofs = match self.random.as_mut() {
                Some(rng) => rng.u64(..=max_ofs) & !(rs as u64 - 1),
                None => {
                    let ofs = self.offset;
                    self.offset += step;
                    if self.offset > max_ofs {
                        self.offset = 0;
                    }
                    ofs
                }
            };

            self.remaining_to_submit -= 1;

            let fut = match request_type {
                IoRequestType::Read => {
                    let buf = self.reqs[i].buffer.as_mut();
                    let buf = unsafe {
                        std::mem::transmute::<IoBufferMut<'_>, IoBufferMut<'static>>(buf)
                    };
                    match self.compare_pattern {
                        Some(reference) => {
                            let buf_slice = unsafe {
                                std::mem::transmute::<&'_ [u8], &'static [u8]>(
                                    self.reqs[i].buffer.as_ref().into_slice(),
                                )
                            };
                            let fut = self.queue.read(buf, ofs);
                            Box::pin(async move {
                                fut.await?;
                                let mut mismatch = false;
                                for (i, value) in buf_slice.iter().enumerate() {
                                    if *value != reference {
                                        eprintln!(
                                            "Pattern mismatch at offset 0x{:x}: Expected {}, found {}",
                                            ofs + i as u64,
                                            reference,
                                            value,
                                        );
                                        mismatch = true;
                                    }
                                }
                                if mismatch {
                                    Err("Pattern mismatch".into())
                                } else {
                                    Ok(())
                                }
                            })
                        }
                        None => self.queue.read(buf, ofs),
                    }
                }
                IoRequestType::Write => {
                    let buf = self.reqs[i].buffer.as_ref();
                    let buf = unsafe {
                        std::mem::transmute::<IoBufferRef<'_>, IoBufferRef<'static>>(buf)
                    };
                    self.queue.write(buf, ofs)
                }
                IoRequestType::Flush => self.queue.flush(),
            };
            let fut = unsafe {
                std::mem::transmute::<BlockFutureResult<'_, ()>, BlockFutureResult<'static, ()>>(
                    fut,
                )
            };
            self.reqs[i].future.replace(fut);

            continuously_pending = 0;
        }

        // Are we actually quiesced, i.e. are 0 requests in flight?
        if self.quiesced && self.remaining_to_settle - self.remaining_to_submit == 0 {
            if let Some(ack) = self.quiesce_ack.take() {
                ack.send(()).unwrap();
            }
        }

        if self.remaining_to_settle == 0 {
            Poll::Ready(self.total - self.remaining_to_settle)
        } else {
            Poll::Pending
        }
    }
}

#[async_trait(?Send)]
impl exports::ImmutableExportConfig for BenchConfig {
    type BackgroundOpData = BenchBackgroundOpData;

    fn construct_node_user(
        &self,
        init: NodeUserBuilder,
        read_only: bool,
    ) -> BlockResult<NodeUserBuilder> {
        if read_only && self.request_type == IoRequestType::Write {
            return Err("Cannot make a writing bench node read-only".into());
        }

        let mut node_user = init.block(NodePerm::Resize);

        if self.request_type == IoRequestType::Write {
            node_user = node_user
                .block(NodePerm::ConsistentRead)
                .require(NodePerm::Write);
        }

        Ok(node_user)
    }

    async fn create_background_op_data(
        &self,
        node_name: &str,
        exported: &Arc<NodeUser>,
        channel_r: mpsc::UnboundedReceiver<BackgroundOpReq>,
        read_only: bool,
    ) -> BlockResult<Self::BackgroundOpData> {
        if read_only && self.request_type == IoRequestType::Write {
            return Err("Cannot make a writing bench node read-only".into());
        }

        let exit_when_done = self.exit_when_done;
        let total = self.total.unwrap_or(1048576);
        let simultaneous = self.simultaneous.unwrap_or(16);
        let request_type = self.request_type;
        let request_size = self.request_size.unwrap_or(4096);

        let random = if self.random {
            Some(fastrand::Rng::new())
        } else {
            None
        };

        if simultaneous > total {
            return Err(format!(
                "Number of simultaneous requests ({}) must not exceed the total number \
                   of requests ({})",
                simultaneous, total
            )
            .into());
        }

        let child_len = exported.node().size();
        let buf_pattern = match request_type {
            IoRequestType::Write => self.pattern.unwrap_or(0),
            _ => !self.pattern.unwrap_or(0),
        };
        let compare_pattern = (request_type == IoRequestType::Read)
            .then_some(self.pattern)
            .flatten();
        let initial_offset = match self.offset {
            Some(offset) => {
                if random.is_some() {
                    return Err("Cannot set an initial offset in random mode".into());
                }
                offset
            }
            None => 0,
        };
        let step = match self.step {
            Some(step) => {
                if random.is_some() {
                    return Err("Cannot set a step in random mode".into());
                }
                if step > child_len {
                    return Err(format!(
                        "step ({}) must not exceed the node's size ({})",
                        step, child_len
                    )
                    .into());
                }
                step
            }
            None => request_size as u64,
        };

        let thread = monitor::monitor().get_thread_from_opt(&self.iothread)?;
        let exported = Arc::clone(exported);

        let rio = thread
            .run(move || -> BlockFutureResult<_> {
                Box::pin(async move {
                    let queue = exported.new_queue()?;

                    // Use the maximum alignment that fits the request size
                    let align = if request_size != 0 {
                        1usize << request_size.trailing_zeros()
                    } else {
                        1usize
                    };

                    let rio = RandomIo {
                        export_id: node_name.to_string(),
                        queue,
                        pre_reopen_queue: None,

                        reqs: (0..simultaneous)
                            .map(|_| RequestSlot::with_buf(align, request_size, buf_pattern))
                            .collect::<BlockResult<Vec<RequestSlot>>>()?,
                        simultaneous,
                        poll_index: 0,

                        total,
                        request_type,
                        request_size,
                        remaining_to_submit: total,
                        remaining_to_settle: total,

                        random,

                        offset: initial_offset,
                        step,
                        max_offset: child_len - step,
                        compare_pattern,

                        channel: channel_r,
                        quiesced: false,
                        quiesce_ack: None,

                        exit_when_done,
                    };

                    // Safe because we will either start the background operation, thus consuming
                    // this; or we will correctly drop this in `async_drop()`.
                    Ok(unsafe { ThreadBound::new_unsafe(rio) })
                })
            })
            .await?;

        Ok(BenchBackgroundOpData { rio, thread })
    }
}

#[async_trait(?Send)]
impl exports::BackgroundOpData for BenchBackgroundOpData {
    fn background_operation(self) -> (BlockFutureResult<'static, ()>, Option<ServerNode>) {
        (
            Box::pin(
                self.thread
                    .owned_run(move || Box::pin(self.rio.unwrap().run())),
            ),
            None,
        )
    }

    async fn async_drop(self) {
        self.thread
            .owned_run(move || {
                Box::pin(async move {
                    #[allow(clippy::let_underscore_future)]
                    let _: RandomIo = self.rio.unwrap();
                })
            })
            .await;
    }
}
