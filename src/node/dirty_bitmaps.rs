use crate::error::BlockResult;
use std::sync::{Arc, Mutex};
use tokio::sync::oneshot;

// TODO: Could use any shift here; using this value only makes sense if there'd be optimization
// between layers (i.e. specifically because one bit on an upper layer corresponds to one usize on
// the next)
const GRANULARITY_STEP: u32 = usize::BITS.trailing_zeros();

pub struct DirtyBitmap {
    layers: Vec<FlatBitmap>,
    enabled: bool,

    dirty_notifiers: Vec<oneshot::Sender<()>>,
}

struct FlatBitmap {
    data: Vec<usize>,
    length: u64,
    dirty_count: u64,

    ld_granularity: u32,
    shift_to_usize: u32,
    in_usize_mask: usize,
}

pub struct DirtyBitmapIterator {
    bitmap: Arc<Mutex<DirtyBitmap>>,
    offset: u64,
    max_area_length: u64,
    clear: bool,
}

pub struct DirtyBitmapArea {
    pub offset: u64,
    pub length: u64,
}

impl DirtyBitmap {
    pub fn new(length: u64, mut granularity: u64, enabled: bool) -> BlockResult<DirtyBitmap> {
        let mut layers: Vec<FlatBitmap> = Vec::new();
        layers.push(FlatBitmap::new(length, granularity)?);

        while layers.last().unwrap().data.len() >= 8 {
            granularity = match granularity.checked_shl(GRANULARITY_STEP) {
                Some(g) => g,
                None => break,
            };

            layers.push(FlatBitmap::new(length, granularity)?);
        }

        Ok(DirtyBitmap {
            layers,
            enabled,
            dirty_notifiers: Vec::new(),
        })
    }

    pub fn len(&self) -> u64 {
        self.layers[0].length
    }

    pub fn set_enabled(&mut self, enable: bool) {
        self.enabled = enable;
    }

    pub fn dirty(&mut self, start: u64, length: u64) {
        if !self.enabled {
            return;
        }

        for notifier in self.dirty_notifiers.drain(..) {
            let _: Result<(), _> = notifier.send(());
        }

        for layer in &mut self.layers {
            if !layer.dirty(start, length) {
                break;
            }
        }
    }

    pub fn clear(&mut self, start: u64, length: u64) {
        for layer in &mut self.layers {
            if !layer.clear(start, length) {
                break;
            }
        }
    }

    pub fn full_clear(&mut self) {
        for layer in &mut self.layers {
            layer.full_clear();
        }
    }

    pub fn add_dirty_notifier(&mut self) -> oneshot::Receiver<()> {
        let (notifier, notifiee) = oneshot::channel();
        self.dirty_notifiers.push(notifier);
        notifiee
    }

    pub fn merge(&mut self, other: &Self) {
        for layer in &mut self.layers {
            layer.merge(&other.layers[0])
        }
    }

    pub fn granularity(&self) -> u64 {
        1u64 << self.layers[0].ld_granularity
    }

    pub fn dirty_count(&self) -> u64 {
        self.layers[0].dirty_count
    }

    pub fn dirty_bytes(&self) -> u64 {
        self.dirty_count() << self.layers[0].ld_granularity
    }

    pub fn is_empty(&self) -> bool {
        self.layers[0].dirty_count == 0
    }

    pub fn dirty_in_range(&self, offset: u64, length: u64) -> bool {
        let clean_length = self.layers[0].get_clean_area(offset, length);
        // If `offset + clean_length == self.layers[0].length`, `length` just exceeds the length of
        // the bitmap, but we ignore everything past its end, so let that area count as clean here
        clean_length < length && offset + clean_length < self.layers[0].length
    }

    pub fn clean_in_range(&self, offset: u64, length: u64) -> bool {
        let dirty_length = self.layers[0].get_dirty_area(offset, length);
        // If `offset + dirty_length == self.layers[0].length`, `length` just exceeds the length of
        // the bitmap, but we ignore everything past its end, so let that area count as dirty here
        dirty_length < length && offset + dirty_length < self.layers[0].length
    }

    pub fn get_clean_area(&self, offset: u64, length: u64) -> u64 {
        self.layers[0].get_clean_area(offset, length)
    }

    pub fn get_dirty_area(&self, offset: u64, length: u64) -> u64 {
        self.layers[0].get_dirty_area(offset, length)
    }
}

macro_rules! bitmap_operation {
    ($name:ident, $element_operation:ident, $dirty:literal) => {
        fn $name(&mut self, start: u64, length: u64) -> bool {
            if length == 0 {
                return false;
            }

            assert!(start < self.length);
            let end = start.checked_add(length).unwrap();
            // End must either be less than the bitmap length, or be aligned exactly to the length
            // rounded up to match the granularity
            assert!(
                end <= self.length
                    || end
                        == (self.length + (1 << self.ld_granularity) - 1)
                            & !((1 << self.ld_granularity) - 1)
            );

            let (start_bit, end_bit): (usize, usize) = if $dirty {
                // Round up if dirtying
                (
                    (start >> self.ld_granularity) as usize,
                    ((start + length - 1) >> self.ld_granularity) as usize,
                )
            } else {
                // Round down if clearing
                let gran_size = 1u64 << self.ld_granularity;
                let start_bit = ((start + gran_size - 1) >> self.ld_granularity) as usize;
                let end_bit = if start + length == self.length {
                    ((self.length - 1) >> self.ld_granularity) as usize
                } else {
                    match (start + length).checked_sub(gran_size) {
                        Some(x) => (x >> self.ld_granularity) as usize,
                        None => return false,
                    }
                };
                if start_bit > end_bit {
                    return false;
                }

                (start_bit, end_bit)
            };

            let start_usize = start_bit >> self.shift_to_usize;
            let end_usize = end_bit >> self.shift_to_usize;

            let mut modified = 0;

            if start_usize == end_usize {
                let start_bit = (start_bit & self.in_usize_mask) as u32;
                let end_bit = (end_bit & self.in_usize_mask) as u32;

                let mask =
                    2usize.wrapping_shl(end_bit).wrapping_sub(1) & !((1usize << start_bit) - 1);

                modified += self.$element_operation(start_usize, mask);
            } else {
                let start_bit = (start_bit & self.in_usize_mask) as u32;
                let mask = usize::MAX & !((1usize << start_bit) - 1);

                modified += self.$element_operation(start_usize, mask);

                for usize_i in (start_usize + 1)..end_usize {
                    modified += self.$element_operation(usize_i, usize::MAX);
                }

                let end_bit = (end_bit & self.in_usize_mask) as u32;
                let mask = 2usize.wrapping_shl(end_bit).wrapping_sub(1);

                modified += self.$element_operation(end_usize, mask);
            }

            if $dirty {
                self.dirty_count += modified;
            } else {
                self.dirty_count -= modified;
            }
            modified != 0
        }
    };
}

macro_rules! bitmap_get_area {
    ($name:ident, $check_usize:ident, $in_condition_bit_count:ident) => {
        fn $name(&self, offset: u64, max_len: u64) -> u64 {
            let max_len = std::cmp::min(max_len, self.length - offset);

            let bit_index = (offset >> self.ld_granularity) as usize;

            let mut in_usize_index = (bit_index & self.in_usize_mask) as u32;
            let mut usize_index = bit_index >> self.shift_to_usize;
            let mut len = 0u64;
            let len_per_usize = 1u64 << (self.ld_granularity + self.shift_to_usize);

            while len < max_len {
                if let Some(partial) = self.$check_usize(usize_index, in_usize_index) {
                    len += (partial as u64) << self.ld_granularity;
                    break;
                }

                usize_index += 1;
                if in_usize_index == 0 {
                    len += len_per_usize;
                } else {
                    len += (usize::BITS as u64 - in_usize_index as u64) << self.ld_granularity;
                    in_usize_index = 0;
                }
            }

            std::cmp::min(len, max_len)
        }
    };
}

impl FlatBitmap {
    bitmap_operation!(dirty, usize_dirty, true);

    bitmap_operation!(clear, usize_clear, false);

    bitmap_get_area!(get_clean_area, get_clean_area_single_usize, trailing_zeros);

    bitmap_get_area!(get_dirty_area, get_dirty_area_single_usize, trailing_ones);

    fn new(length: u64, granularity: u64) -> BlockResult<FlatBitmap> {
        if !granularity.is_power_of_two() {
            return Err(format!(
                "Bitmap granularity must be a power of two; {} is not",
                granularity
            )
            .into());
        }

        let ld_granularity = granularity.trailing_zeros();
        let bit_count = (length.checked_add(granularity).ok_or_else(|| {
            format!(
                "Bitmap size ({}) must not exceed u64::MAX - granularity ({})",
                length,
                u64::MAX - granularity
            )
        })? - 1)
            >> ld_granularity;

        if bit_count > usize::MAX as u64 {
            return Err(format!(
                "Bitmap size ({}) divided by granularity ({}), rounded up, must not exceed \
                 usize::MAX ({}); try a bigger granularity",
                length,
                granularity,
                usize::MAX
            )
            .into());
        }

        let shift_to_usize = usize::BITS.trailing_zeros();
        let in_usize_mask: usize = (usize::BITS - 1).try_into().unwrap();

        let usize_count = (bit_count + usize::BITS as u64 - 1) >> shift_to_usize;

        let mut data = Vec::new();
        data.resize_with(usize_count.try_into().unwrap(), Default::default);

        Ok(FlatBitmap {
            data,
            length,
            dirty_count: 0,
            ld_granularity,
            shift_to_usize,
            in_usize_mask,
        })
    }

    fn usize_dirty(&mut self, index: usize, mask: usize) -> u64 {
        let to_set = !self.data[index] & mask;
        if to_set != 0 {
            self.data[index] |= mask;
            to_set.count_ones() as u64
        } else {
            0
        }
    }

    fn usize_clear(&mut self, index: usize, mask: usize) -> u64 {
        let to_clear = self.data[index] & mask;
        if to_clear != 0 {
            self.data[index] &= !mask;
            to_clear.count_ones() as u64
        } else {
            0
        }
    }

    fn get_dirty_area_single_usize(&self, usize_index: usize, in_usize_index: u32) -> Option<u32> {
        let lower_set: usize = (1usize << in_usize_index) - 1;
        let masked = self.data[usize_index] | lower_set;
        if masked != usize::MAX {
            Some(masked.trailing_ones() - in_usize_index)
        } else {
            None
        }
    }

    fn get_clean_area_single_usize(&self, usize_index: usize, in_usize_index: u32) -> Option<u32> {
        let lower_cleared: usize = !((1usize << in_usize_index) - 1);
        let masked = self.data[usize_index] & lower_cleared;
        if masked != 0 {
            Some(masked.trailing_zeros() - in_usize_index)
        } else {
            None
        }
    }

    fn full_clear(&mut self) {
        self.data.fill(0);
        self.dirty_count = 0;
    }

    fn merge(&mut self, other: &Self) {
        match other.ld_granularity.cmp(&self.ld_granularity) {
            std::cmp::Ordering::Equal => self.merge_same_granularity(other),
            std::cmp::Ordering::Less => self.merge_smaller_granularity(other),
            std::cmp::Ordering::Greater => self.merge_greater_granularity(other),
        }

        self.clean_tail();
        self.recalc_dirty_count();
    }

    fn clean_tail(&mut self) {
        if self.length == 0 {
            assert!(self.data.is_empty());
            return;
        }

        let past_last_bit_index = ((self.length - 1) >> self.ld_granularity) + 1;
        let past_last_usize = past_last_bit_index >> self.shift_to_usize;
        if past_last_usize >= self.data.len() as u64 {
            return;
        }

        // Keep all bits excluding `past_last_bit_index`, clean the rest
        let mask = (1usize
            .wrapping_shl((past_last_bit_index & (self.in_usize_mask as u64)) as u32))
        .wrapping_sub(1);
        self.data[past_last_usize as usize] &= mask;
    }

    fn recalc_dirty_count(&mut self) {
        self.dirty_count = 0;
        for word in self.data.iter() {
            self.dirty_count += word.count_ones() as u64;
        }
    }

    fn merge_same_granularity(&mut self, other: &Self) {
        assert!(self.ld_granularity == other.ld_granularity);
        let len = std::cmp::min(self.data.len(), other.data.len());
        for i in 0..len {
            self.data[i] |= other.data[i];
        }
    }

    fn merge_smaller_granularity(&mut self, other: &Self) {
        assert!(self.ld_granularity > other.ld_granularity);

        // Area covered by a single bit in `self`
        let self_bit_area_length = 1u64 << self.ld_granularity;

        let mut i = 0;
        while i < other.data.len() {
            let mut word = other.data[i];
            if word == 0 {
                i += 1;
                continue;
            }

            let mut j = 0;
            while j < usize::BITS {
                if word & (1usize << j) != 0 {
                    let offset =
                        (((i as u64) << other.shift_to_usize) | j as u64) << other.ld_granularity;

                    let offset_rounded_down = offset & !((1u64 << self.ld_granularity) - 1);
                    if offset_rounded_down >= self.length {
                        return;
                    }
                    // `self` has a greater granularity, so anything in `1..=self_bit_area_length`
                    // has the same effect (but this does not need to be checked against
                    // `self.length`)
                    self.dirty(offset_rounded_down, 1);

                    let next_offset = offset_rounded_down + self_bit_area_length;
                    j = ((next_offset >> other.ld_granularity) & (other.in_usize_mask as u64))
                        as u32;
                    i = (next_offset >> (other.ld_granularity + other.shift_to_usize)) as usize;

                    if i >= other.data.len() {
                        return;
                    }
                    word = other.data[i];
                    if word == 0 {
                        break;
                    }
                } else {
                    j += 1;
                }
            }

            i += 1;
        }
    }

    fn merge_greater_granularity(&mut self, other: &Self) {
        assert!(self.ld_granularity < other.ld_granularity);

        // Area covered by a single bit in `other`
        let other_bit_area_length = 1u64 << other.ld_granularity;

        for i in 0..other.data.len() {
            let word = other.data[i];
            if word == 0 {
                continue;
            }

            for j in 0..usize::BITS {
                if word & (1usize << j) != 0 {
                    let offset =
                        (((i as u64) << other.shift_to_usize) | j as u64) << other.ld_granularity;
                    if offset >= self.length {
                        return;
                    }
                    self.dirty(
                        offset,
                        std::cmp::min(other_bit_area_length, self.length - offset),
                    );
                }
            }
        }
    }
}

impl DirtyBitmapIterator {
    pub fn new(bitmap: Arc<Mutex<DirtyBitmap>>, max_area_length: u64, clear: bool) -> Self {
        DirtyBitmapIterator {
            bitmap,
            offset: 0,
            max_area_length,
            clear,
        }
    }
}

impl Iterator for DirtyBitmapIterator {
    type Item = DirtyBitmapArea;

    fn next(&mut self) -> Option<DirtyBitmapArea> {
        let mut bitmap = self.bitmap.lock().unwrap();
        if self.offset >= bitmap.layers[0].length {
            return None;
        }

        let mut layer = std::cmp::min(
            ((self.offset.trailing_zeros() - bitmap.layers[0].ld_granularity) / GRANULARITY_STEP)
                as usize,
            bitmap.layers.len() - 1,
        );

        let mut next_layer_granularity = if layer + 1 < bitmap.layers.len() {
            1u64 << bitmap.layers[layer + 1].ld_granularity
        } else {
            u64::MAX
        };

        while self.offset < bitmap.layers[0].length {
            let clean_len = bitmap.layers[layer].get_clean_area(self.offset, u64::MAX);
            if clean_len == 0 {
                if layer == 0 {
                    let dirty_len =
                        bitmap.layers[0].get_dirty_area(self.offset, self.max_area_length);
                    if self.clear {
                        bitmap.layers[0].clear(self.offset, dirty_len);
                        if dirty_len >= next_layer_granularity {
                            let mut upper_layer = 1;
                            while upper_layer < bitmap.layers.len() {
                                if bitmap.layers[upper_layer].clear(self.offset, dirty_len) {
                                    break;
                                }
                                upper_layer += 1;
                            }
                        }
                    }

                    let area = DirtyBitmapArea {
                        offset: self.offset,
                        length: dirty_len,
                    };
                    self.offset += dirty_len;

                    return Some(area);
                }
                next_layer_granularity = 1u64 << bitmap.layers[layer].ld_granularity;
                layer -= 1;
                continue;
            } else if clean_len >= next_layer_granularity {
                let mut upper_layer = layer + 1;
                while upper_layer < bitmap.layers.len() {
                    if !bitmap.layers[upper_layer].clear(self.offset, clean_len) {
                        break;
                    }
                    upper_layer += 1;
                }
            }

            self.offset += clean_len;
        }

        None
    }
}
