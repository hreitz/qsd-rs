use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, IoVector, IoVectorMut, ThreadMap};
use crate::node::{
    IoQueueDriverData, Node, NodeBasicInfo, NodeCacheConfig, NodeConfig, NodeDriverData,
    NodeLimits, NodePermPair,
};
use async_trait::async_trait;
use blkio_async::AsyncBlkioq;
use serde::{Deserialize, Serialize};
use std::collections::LinkedList;
use std::rc::Rc;
use std::sync::atomic::{AtomicU64, AtomicUsize};
use std::sync::{Arc, Mutex};

struct MutData {
    blkio: blkio::Blkio,
    unused_queues: LinkedList<blkio::Blkioq>,
    in_use_queues: ThreadMap<Rc<AsyncBlkioq>>,
}

pub struct Data {
    mut_data: Arc<Mutex<MutData>>,
    pre_reopen: Mutex<Option<MutData>>,
}

// Unsafe, but currently libblkio leaves us no choice but to do this, because neither `Blkio` nor
// `Blkioq` implement any of these traits
unsafe impl Send for MutData {}
unsafe impl Sync for MutData {}

pub struct Queue {
    queue: Rc<AsyncBlkioq>,

    mut_data: Arc<Mutex<MutData>>,

    pre_reopen_queue: Option<Rc<AsyncBlkioq>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub filename: String,
    pub queue_count: Option<usize>,
}

impl Config {
    #[allow(clippy::ptr_arg)]
    pub fn split_tree(&mut self, _vec: &mut Vec<NodeConfig>) -> BlockResult<()> {
        Ok(())
    }
}

impl Data {
    pub async fn new(
        _name: &str,
        opts: Config,
        read_only: bool,
        cache: &NodeCacheConfig,
    ) -> BlockResult<Box<Self>> {
        let mut_data = MutData::new(opts, read_only, cache)?;
        Ok(Box::new(Data {
            mut_data: Arc::new(Mutex::new(mut_data)),
            pre_reopen: Default::default(),
        }))
    }
}

impl MutData {
    fn new(opts: Config, read_only: bool, cache: &NodeCacheConfig) -> BlockResult<MutData> {
        // Ever since blkio supports adding queues at runtime, we can allow the user to set this to
        // 0.  Still, keep the default of 1 pre-created queue.
        let queue_count = opts.queue_count.unwrap_or(1);

        let mut blkio = blkio::Blkio::new("io_uring")?;
        blkio.set_str("path", &opts.filename)?;
        blkio.set_bool("direct", cache.direct)?;
        blkio.set_bool("read-only", read_only)?;
        blkio.connect()?;
        assert!(!(blkio.get_bool("needs-mem-regions")?));
        blkio.set_i32("num-queues", queue_count.try_into()?)?;
        let mut start_outcome = blkio.start()?;

        Ok(MutData {
            blkio,
            unused_queues: start_outcome.queues.drain(..).collect(),
            in_use_queues: Default::default(),
        })
    }

    fn new_queue(&mut self) -> BlockResult<Rc<AsyncBlkioq>> {
        if let Some(queue) = self.in_use_queues.get() {
            Ok(Rc::clone(queue))
        } else {
            let blkioq = match self.unused_queues.pop_front() {
                Some(q) => q,
                None => self.blkio.add_queue(false)?,
            };
            let queue = Rc::new(AsyncBlkioq::new(blkioq));
            self.in_use_queues.insert(Rc::clone(&queue));
            Ok(queue)
        }
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        let data = self.mut_data.lock().unwrap();
        let req_align: usize = data.blkio.get_i32("request-alignment")?.try_into()?;
        let buf_align: usize = data.blkio.get_i32("buf-alignment")?.try_into()?;

        if !req_align.is_power_of_two() {
            return Err(format!(
                "Request alignment reported by blkio ({}) is not a power of two",
                req_align
            )
            .into());
        }
        if !buf_align.is_power_of_two() {
            return Err(format!(
                "Memory alignment reported by blkio ({}) is not a power of two",
                buf_align
            )
            .into());
        }

        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(data.blkio.get_u64("capacity")?),
                request_alignment: AtomicUsize::new(req_align),
                memory_alignment: AtomicUsize::new(buf_align),
                enforced_memory_alignment: AtomicUsize::new(buf_align),
            },
        })
    }

    fn new_queue(&self) -> BlockResult<Box<dyn IoQueueDriverData>> {
        let mut data = self.mut_data.lock().unwrap();
        data.new_queue().map(|queue| -> Box<dyn IoQueueDriverData> {
            Box::new(Queue {
                queue,
                mut_data: Arc::clone(&self.mut_data),
                pre_reopen_queue: None,
            })
        })
    }

    fn get_children(&self) -> Vec<Arc<Node>> {
        Vec::new() // no children
    }

    fn get_children_after_reopen(&self, _opts: &NodeConfig) -> BlockResult<Vec<Arc<Node>>> {
        Ok(Vec::new()) // no children
    }

    fn reopen_do(
        &self,
        opts: NodeConfig,
        _perms: NodePermPair,
        read_only: bool,
    ) -> BlockFutureResult<()> {
        Box::pin(async move {
            use crate::node::file;

            let cache = &opts.cache;
            let opts: file::Config = opts.driver.try_into()?;
            let opts: file::AioConfig = opts.into();
            let opts: Config = opts.try_into()?;

            let new_data = MutData::new(opts, read_only, cache)?;
            let old_data = std::mem::replace(&mut *self.mut_data.lock().unwrap(), new_data);
            self.pre_reopen.lock().unwrap().replace(old_data);

            Ok(())
        })
    }

    fn reopen_clean(&self) {
        self.pre_reopen.lock().unwrap().take();
    }

    fn reopen_roll_back(&self) {
        if let Some(old_data) = self.pre_reopen.lock().unwrap().take() {
            *self.mut_data.lock().unwrap() = old_data;
        }
    }
}

#[async_trait]
impl IoQueueDriverData for Queue {
    fn readv<'a>(&'a self, bufv: IoVectorMut<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        let fut = self
            .queue
            .readv_owned(offset, bufv.into_inner(), blkio::ReqFlags::empty());

        Box::pin(async move {
            fut.await?;
            Ok(())
        })
    }

    fn writev<'a>(&'a self, bufv: IoVector<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        let fut = self
            .queue
            .writev_owned(offset, bufv.into_inner(), blkio::ReqFlags::empty());

        Box::pin(async move {
            fut.await?;
            Ok(())
        })
    }

    fn flush(&self) -> BlockFutureResult<'_, ()> {
        let fut = self.queue.flush(blkio::ReqFlags::empty());

        Box::pin(async move {
            fut.await?;
            Ok(())
        })
    }

    fn reopen_do(&mut self) -> BlockResult<()> {
        let new_queue = self.mut_data.lock().unwrap().new_queue()?;

        let old_queue = std::mem::replace(&mut self.queue, new_queue);
        self.pre_reopen_queue.replace(old_queue);

        Ok(())
    }

    fn reopen_clean(&mut self) {
        self.pre_reopen_queue.take();
    }

    fn reopen_roll_back(&mut self) {
        let old_queue = self.pre_reopen_queue.take().unwrap();
        self.queue = old_queue;
    }
}
