use crate::error::{BlockError, BlockResult};
use crate::helpers::nbd::*;
use crate::helpers::{
    BlockFutureResult, FlatSize, IoBuffer, IoBufferMut, IoBufferRef, NetReadHalf, NetStream,
    NetWriteHalf, ThreadBound,
};
use crate::monitor::{self, ThreadHandle};
use crate::node::exports::{self, BackgroundOpReq};
use crate::node::{IoQueue, NodePerm, NodeUser, NodeUserBuilder, PollableNodeStopMode};
use crate::server::nbd::NodeData;
use crate::server::ServerNode;
use async_trait::async_trait;
use bincode::Options;
use serde::{Deserialize, Serialize};
use std::cell::{Cell, UnsafeCell};
use std::collections::LinkedList;
use std::future::Future;
use std::pin::Pin;
use std::rc::Rc;
use std::sync::atomic::{AtomicU32, AtomicU64, AtomicUsize, Ordering};
use std::sync::Arc;
use std::task::{Context, Poll};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::sync::{mpsc, oneshot};

pub type Config = exports::Config<NbdConfig>;
pub type Data = exports::Data<NbdConfig>;

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct NbdConfig {
    server: Option<String>,

    #[serde(default)]
    writable: bool,
    name: Option<String>,

    iothread: Option<String>,
}

pub struct NbdBackgroundOpData {
    export_poller: ThreadBound<ExportPoller>,
    thread: ThreadHandle,
    server: ServerNode,
}

const REQUEST_BUFFER_SIZE: usize = 4096;

#[derive(Debug)]
pub struct ExportInfo {
    name: String,
    size: AtomicU64,
    req_align: AtomicU32,
}

#[derive(Debug)]
pub struct Export {
    read_only: bool,
    info: ExportInfo,
    add_connection: mpsc::UnboundedSender<NetStream>,
    quit: mpsc::Sender<()>,
}

/// Export state shared between connections for that export
struct ConnectionExportState {
    queue: UnsafeCell<IoQueue>,
    pre_reopen_queue: Cell<Option<IoQueue>>,
    read_only: bool,
    mem_alignment: usize,
    quiesced: Cell<bool>,
    in_flight: AtomicUsize,
}

struct ExportPoller {
    export: Arc<Export>,
    export_state: Rc<ConnectionExportState>,

    connections: LinkedList<EstablishedConnection>,
    new_connection: mpsc::UnboundedReceiver<NetStream>,

    channel: mpsc::UnboundedReceiver<BackgroundOpReq>,
    quit: mpsc::Receiver<()>,
    quiesce_ack: Option<oneshot::Sender<()>>,
}

struct RequestBuffer {
    alignment: usize,

    // Always-present buffer of `REQUEST_BUFFER_SIZE` for small requests
    fixed: IoBuffer,
    // Allocated only temporarily for larger requests
    heap: Option<IoBuffer>,
}

struct Request<'a> {
    // Present unless bound to a future
    buffer: Option<RequestBuffer>,

    receiver: Option<BlockFutureResult<'a, (RequestBuffer, NetReadHalf, NbdRequest)>>,
    worker: Option<BlockFutureResult<'a, (RequestBuffer, bool)>>,
}

struct EstablishedConnection {
    future: BlockFutureResult<'static, ()>,
}

#[async_trait(?Send)]
impl exports::ImmutableExportConfig for NbdConfig {
    type BackgroundOpData = NbdBackgroundOpData;

    fn construct_node_user(
        &self,
        init: NodeUserBuilder,
        read_only: bool,
    ) -> BlockResult<NodeUserBuilder> {
        if self.writable && read_only {
            return Err("Cannot set both writable and read-only".into());
        }

        let mut node_user = init
            .require(NodePerm::ConsistentRead)
            .block(NodePerm::Write)
            .block(NodePerm::Resize);
        if self.writable {
            node_user = node_user.require(NodePerm::Write);
        }
        Ok(node_user)
    }

    async fn create_background_op_data(
        &self,
        node_name: &str,
        exported: &Arc<NodeUser>,
        channel_r: mpsc::UnboundedReceiver<BackgroundOpReq>,
        read_only: bool,
    ) -> BlockResult<NbdBackgroundOpData> {
        if self.writable && read_only {
            return Err("Cannot set both writable and read-only".into());
        }

        let (quit_s, quit_r) = mpsc::channel::<()>(1);
        let (new_con_s, new_con_r) = mpsc::unbounded_channel::<NetStream>();

        let export = Arc::new(Export {
            read_only: !self.writable,
            info: ExportInfo {
                name: self
                    .name
                    .as_ref()
                    .unwrap_or_else(|| &exported.node().name)
                    .clone(),
                size: AtomicU64::new(exported.node().size()),
                req_align: AtomicU32::new(exported.node().request_align().try_into()?),
            },
            add_connection: new_con_s,
            quit: quit_s,
        });

        let thread = monitor::monitor().get_thread_from_opt(&self.iothread)?;
        let exported = Arc::clone(exported);

        let server_id = self
            .server
            .as_ref()
            .cloned()
            .unwrap_or_else(|| DEFAULT_SERVER_NAME.into());
        let server_node_data = NodeData::new(Arc::clone(&export));
        let server_binding = ServerNode::new(&server_id, node_name, server_node_data)?;

        let export_poller = thread
            .run(move || -> BlockFutureResult<_> {
                Box::pin(async move {
                    let queue = exported.new_queue()?;
                    // TODO: Update when changed, and update all buffers
                    let mem_alignment = queue.mem_align();

                    let export_state = Rc::new(ConnectionExportState {
                        read_only: export.read_only,
                        mem_alignment,
                        queue: UnsafeCell::new(queue),
                        pre_reopen_queue: Cell::new(None),
                        quiesced: Cell::new(false),
                        in_flight: AtomicUsize::new(0),
                    });

                    let export_poller = ExportPoller {
                        export,
                        export_state,

                        connections: LinkedList::new(),
                        new_connection: new_con_r,

                        channel: channel_r,
                        quit: quit_r,
                        quiesce_ack: None,
                    };
                    // Safe because we will either start the background operation, thus consuming
                    // this; or we will correctly drop this in `async_drop()`.
                    Ok(unsafe { ThreadBound::new_unsafe(export_poller) })
                })
            })
            .await?;

        Ok(NbdBackgroundOpData {
            export_poller,
            thread,
            server: server_binding,
        })
    }
}

#[async_trait(?Send)]
impl exports::BackgroundOpData for NbdBackgroundOpData {
    fn background_operation(self) -> (BlockFutureResult<'static, ()>, Option<ServerNode>) {
        (
            Box::pin(
                self.thread
                    .owned_run(move || Box::pin(self.export_poller.unwrap())),
            ),
            Some(self.server),
        )
    }

    async fn async_drop(self) {
        self.thread
            .owned_run(move || {
                Box::pin(async move {
                    #[allow(clippy::let_underscore_future)]
                    let _: ExportPoller = self.export_poller.unwrap();
                })
            })
            .await;
    }
}

impl ExportPoller {
    fn poll_quit(&mut self, cx: &mut Context<'_>) -> Poll<BlockResult<()>> {
        if self.quit.poll_recv(cx).is_ready() {
            self.connections.clear();
            Poll::Ready(Ok(()))
        } else {
            Poll::Pending
        }
    }

    fn poll_node_channel(&mut self, cx: &mut Context<'_>) -> Poll<BlockResult<()>> {
        while let Poll::Ready(req) = self.channel.poll_recv(cx) {
            let req = match req {
                Some(req) => req,
                None => return Poll::Ready(Err("Connection closed".into())),
            };

            match req {
                BackgroundOpReq::Quiesce(ack) => {
                    self.export_state.quiesced.replace(true);
                    self.quiesce_ack.replace(ack);
                }

                BackgroundOpReq::Unquiesce => {
                    self.export_state.quiesced.take();
                }

                BackgroundOpReq::Stop(PollableNodeStopMode::Safe, ack) => {
                    // 2 is expected, namely our reference, the one in the exports vector.  All
                    // other references must come from connections.
                    let count = Arc::strong_count(&self.export);

                    if count <= 2 {
                        ack.send(Ok(())).unwrap();
                        return Poll::Ready(Ok(()));
                    } else {
                        let err = format!("{} connection(s) still active", count - 2);
                        ack.send(Err(err.into())).unwrap();
                    }
                }

                BackgroundOpReq::Stop(PollableNodeStopMode::Hard, ack) => {
                    ack.send(Ok(())).unwrap();
                    return Poll::Ready(Ok(()));
                }

                BackgroundOpReq::Stop(
                    PollableNodeStopMode::CopyComplete { switch_over: _ },
                    ack,
                ) => {
                    ack.send(Err(
                        "'copy-complete' stop mode not supported by NBD exports".into(),
                    ))
                    .unwrap();
                }

                BackgroundOpReq::ChangeChildDo {
                    exported: node,
                    read_only,
                    result: ack,
                } => {
                    if read_only && !self.export.read_only {
                        ack.send(Err("Cannot set both writable and read-only".into()))
                            .unwrap();
                        continue;
                    }

                    let old_size = self.export.info.size.load(Ordering::Relaxed);
                    let new_size = node.node().size();
                    let old_req_align = self.export.info.req_align.load(Ordering::Relaxed);
                    let new_req_align: u32 = match node.node().request_align().try_into() {
                        Ok(align) => align,
                        Err(err) => {
                            ack.send(Err(err.into())).unwrap();
                            continue;
                        }
                    };

                    if old_size != new_size && !self.connections.is_empty() {
                        let msg =
                            format!(
                            "NBD cannot change exported node's metadata while connected, but the \
                             new node's (\"{}\") size ({}) differs from that of the current one \
                             ({})",
                            node.node().name, new_size, old_size
                        );
                        ack.send(Err(msg.into())).unwrap();
                        continue;
                    }

                    if new_req_align > old_req_align && !self.connections.is_empty() {
                        let msg = format!(
                            "NBD cannot change exported node's metadata while connected, but the \
                             new node's (\"{}\") request alignment ({}) would exceed that of the \
                             current one ({})",
                            node.node().name,
                            new_req_align,
                            old_req_align
                        );
                        ack.send(Err(msg.into())).unwrap();
                        continue;
                    }

                    let result = match node.new_queue() {
                        Ok(q) => {
                            let old_queue = self.export_state.set_queue(q);
                            self.export_state.pre_reopen_queue.set(Some(old_queue));

                            self.export.info.size.store(new_size, Ordering::Relaxed);
                            self.export
                                .info
                                .req_align
                                .store(new_req_align, Ordering::Relaxed);

                            Ok(())
                        }
                        Err(err) => Err(err),
                    };
                    ack.send(result).unwrap();
                }

                BackgroundOpReq::ChangeChildClean(ack) => {
                    self.export_state.pre_reopen_queue.take();
                    ack.send(()).unwrap();
                }

                BackgroundOpReq::ChangeChildRollBack(ack) => {
                    if let Some(old_queue) = self.export_state.pre_reopen_queue.take() {
                        self.export_state.set_queue(old_queue);
                    }
                    ack.send(()).unwrap();
                }
            }
        }

        Poll::Pending
    }

    fn poll_new_connections(&mut self, cx: &mut Context<'_>) -> Poll<BlockResult<()>> {
        while let Poll::Ready(con) = self.new_connection.poll_recv(cx) {
            let con = match con {
                Some(con) => con,
                None => return Poll::Ready(Err("Connection closed".into())),
            };

            let (r, w) = con.into_split();

            let op_con = OperationalConnection {
                con: SplitConnection {
                    r: Some(r),
                    w: Arc::new(tokio::sync::Mutex::new(w)),
                },

                export_state: Rc::clone(&self.export_state),
                max_active_request_i: 0,
                requests: LinkedList::new(),
            };
            let con_fut = Box::pin(op_con);

            self.connections
                .push_back(EstablishedConnection { future: con_fut });
        }

        Poll::Pending
    }

    fn poll_connections(&mut self, cx: &mut Context<'_>) -> Poll<BlockResult<()>> {
        let mut con_cursor = self.connections.cursor_front_mut();

        loop {
            let result = {
                match con_cursor.current() {
                    Some(con) => Future::poll(con.future.as_mut(), cx),
                    None => break,
                }
            };

            match result {
                Poll::Ready(Ok(())) => {
                    con_cursor.remove_current();
                }
                Poll::Ready(Err(err)) => {
                    eprintln!("NBD client error: {}", err);
                    con_cursor.remove_current();
                }
                Poll::Pending => con_cursor.move_next(),
            }
        }

        if self.export_state.in_flight.load(Ordering::Relaxed) == 0 {
            if let Some(ack) = self.quiesce_ack.take() {
                ack.send(()).unwrap();
            }
        }

        Poll::Pending
    }
}

impl Future for ExportPoller {
    type Output = BlockResult<()>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if let Poll::Ready(result) = self.poll_quit(cx) {
            return Poll::Ready(result);
        }

        if let Poll::Ready(result) = self.poll_node_channel(cx) {
            return Poll::Ready(result);
        }

        if let Poll::Ready(result) = self.poll_new_connections(cx) {
            return Poll::Ready(result);
        }

        if let Poll::Ready(result) = self.poll_connections(cx) {
            return Poll::Ready(result);
        }

        Poll::Pending
    }
}

impl ConnectionExportState {
    // Safe to call if the queue will be put into a request whose lifetime is in turn bound by the
    // actual lifetime of the export
    unsafe fn get_queue<'a>(&self) -> &'a IoQueue {
        assert!(self.in_flight.load(Ordering::Relaxed) > 0);
        unsafe { &*self.queue.get() }
    }

    fn set_queue(&self, new_queue: IoQueue) -> IoQueue {
        assert!(self.in_flight.load(Ordering::Relaxed) == 0);
        unsafe { std::mem::replace(&mut *self.queue.get(), new_queue) }
    }
}

impl Drop for Export {
    fn drop(&mut self) {
        let _: Result<(), _> = self.quit.try_send(());
    }
}

struct SplitConnection {
    r: Option<NetReadHalf>,
    w: Arc<tokio::sync::Mutex<NetWriteHalf>>,
}

struct OperationalConnection<'a> {
    // `con.r` is present if no receiver is waiting
    con: SplitConnection,

    export_state: Rc<ConnectionExportState>,

    // Highest index in `requests` that is being used (no need to iterate further)
    max_active_request_i: usize,
    requests: LinkedList<Request<'a>>,
}

impl Future for OperationalConnection<'_> {
    type Output = BlockResult<()>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<BlockResult<()>> {
        // Otherwise we will run into trouble attempting to borrow `Pin<&mut Self>` multiple times
        // below
        let self_ref: &mut Self = self.as_mut().get_mut();

        let mut pending = false;
        let mut req_cursor = self_ref.requests.cursor_front_mut();
        let con = &mut self_ref.con;

        // Keeps track of the highest request slot index that has actually been found to be in use
        // (reset when the list is re-iterated from the beginning, so after that reset is handled,
        // this will always be less or equal to `req_index`, which allows simply overwriting it by
        // `req_index` to increase it without comparing both)
        let mut max_active_request_i_found = 0;

        loop {
            if con.r.is_none() {
                if let Some(i) = req_cursor.index() {
                    // There is nothing to poll left, and given that con_r is none, we have a
                    // receiver somewhere, so we do not need to find a new slot to put it in.  Wrap
                    // around to the beginning of the list.
                    if i > self_ref.max_active_request_i {
                        req_cursor = self_ref.requests.cursor_front_mut();
                        req_cursor.move_prev();
                    }
                }
            }

            let (req_index, req) = match req_cursor.index() {
                Some(index) => (index, req_cursor.current().unwrap()),
                None => {
                    if pending {
                        // Already did a loop and we have registered the waker somewhere
                        return Poll::Pending;
                    }

                    // Wrap-around, evaluate and reset `max_active_request_i_found`
                    if max_active_request_i_found < self_ref.max_active_request_i {
                        self_ref.max_active_request_i = max_active_request_i_found;
                    }
                    max_active_request_i_found = 0;

                    if con.r.is_some() {
                        // No request slot with receiving future, and apparently we could
                        // not put that future into any existing slot, create one
                        req_cursor.insert_after(Request::new(self_ref.export_state.mem_alignment));
                    }
                    // There must be at least one slot with the receiving future, so wrap
                    // around to the start of the list
                    req_cursor.move_next();
                    (req_cursor.index().unwrap(), req_cursor.current().unwrap())
                }
            };

            if req.receiver.is_some() || req.worker.is_some() {
                max_active_request_i_found = req_index;

                match req.try_poll_receiver(cx, con, &self_ref.export_state) {
                    Ok(false) => (),
                    Ok(true) => pending = true,
                    Err(err) => return Poll::Ready(Err(err)),
                }

                match req.try_poll_worker(cx, &self_ref.export_state) {
                    Ok((_, true)) => return Poll::Ready(Ok(())),
                    Ok((false, false)) => (),
                    Ok((true, false)) => pending = true,
                    Err(err) => return Poll::Ready(Err(err)),
                }
            }

            if req.receiver.is_none() && req.worker.is_none() && con.r.is_some() {
                let receiver = Box::pin(receive_request(
                    con.r.take().unwrap(),
                    req.buffer.take().unwrap(),
                ));
                req.receiver.replace(receiver);

                max_active_request_i_found = req_index;
                if req_index > self_ref.max_active_request_i {
                    self_ref.max_active_request_i = req_index;
                }

                // Do not move the cursor, stay at the current element to poll the receiver
            } else {
                req_cursor.move_next();
            }
        }
    }
}

impl<'a> Request<'a> {
    fn new(alignment: usize) -> Self {
        // TODO: Refresh buffers if alignment no longer matches child
        Request {
            buffer: Some(RequestBuffer {
                alignment,
                fixed: IoBuffer::new(REQUEST_BUFFER_SIZE, alignment).unwrap(),
                heap: None,
            }),

            receiver: None,
            worker: None,
        }
    }

    /// Returns: Whether the request is pending (i.e. the waker is used)
    fn try_poll_receiver(
        &mut self,
        cx: &mut Context<'_>,
        con: &mut SplitConnection,
        export: &ConnectionExportState,
    ) -> BlockResult<bool> {
        // If quiesced, stop polling the receiver so we do not create new requests.
        if export.quiesced.get() {
            return Ok(true);
        }

        let rcv = match self.receiver.as_mut() {
            Some(rcv) => rcv,
            None => return Ok(false),
        };

        match Future::poll(rcv.as_mut(), cx) {
            Poll::Ready(Ok((buffer, con_r, req_params))) => {
                // Drop receiver future
                self.receiver.take();
                // Return con_r
                con.r.replace(con_r);

                export.in_flight.fetch_add(1, Ordering::Relaxed);

                // Safe because this request is owned by the connection, which in turn is owned by
                // the export.  The request cannot outlive the export.
                let queue = unsafe { export.get_queue() };

                // Hand buffer and the request parameters over to the request worker
                let worker = Box::pin(request_worker(
                    queue,
                    export.read_only,
                    Arc::clone(&con.w),
                    req_params,
                    buffer,
                ));
                self.worker.replace(worker);

                Ok(false)
            }

            Poll::Ready(Err(err)) => {
                eprintln!("Invalid NBD request: {}", err);
                Err(err)
            }

            Poll::Pending => Ok(true),
        }
    }

    /// Returns:
    /// .0: Whether the request is pending (i.e. the waker is used)
    /// .1: Whether to close the connection
    fn try_poll_worker(
        &mut self,
        cx: &mut Context<'_>,
        export: &ConnectionExportState,
    ) -> BlockResult<(bool, bool)> {
        let w = match self.worker.as_mut() {
            Some(w) => w,
            None => return Ok((false, false)),
        };

        match Future::poll(w.as_mut(), cx) {
            Poll::Ready(Ok((mut buffer, disconnect))) => {
                self.worker.take();
                buffer.heap.take(); // release heap-allocated storage
                self.buffer.replace(buffer);
                export.in_flight.fetch_sub(1, Ordering::Relaxed);

                Ok((false, disconnect))
            }

            Poll::Ready(Err(err)) => {
                export.in_flight.fetch_sub(1, Ordering::Relaxed);
                eprintln!("Fatal NBD error: {}", err);
                Err(err)
            }

            Poll::Pending => Ok((true, false)),
        }
    }
}

impl RequestBuffer {
    fn get_mut_ref(&mut self, len: usize) -> IoBufferMut<'_> {
        if len > REQUEST_BUFFER_SIZE {
            if self.heap.is_none() {
                self.heap = Some(IoBuffer::new(len, self.alignment).unwrap());
            }
            self.heap.as_mut().unwrap().as_mut()
        } else {
            self.fixed.as_mut_range(0..len)
        }
    }

    /// The slice returned will contain whatever has been written to it before.  Hence, this
    /// assumes there was a prior call to `.get_mut_ref()` with the same `len` parameter.
    /// (`unsafe` because the caller must make that guarantee.)
    unsafe fn get_ref(&self, len: usize) -> IoBufferRef<'_> {
        if len > REQUEST_BUFFER_SIZE {
            let buf = self.heap.as_ref().unwrap().as_ref();
            assert!(buf.len() == len);
            buf
        } else {
            self.fixed.as_ref_range(0..len)
        }
    }
}

/// Returns:
/// .0: Buffer
/// .1: TCP connection's reading half
/// .2: The received request
async fn receive_request(
    mut con_r: NetReadHalf,
    mut buf: RequestBuffer,
) -> BlockResult<(RequestBuffer, NetReadHalf, NbdRequest)> {
    let bincode = bincode::DefaultOptions::new()
        .with_fixint_encoding()
        .with_big_endian();

    let request: NbdRequest = {
        let req_buf = buf.get_mut_ref(NbdRequest::SIZE).into_slice();
        con_r.read_exact(req_buf).await?;
        bincode.deserialize(req_buf)?
    };
    request.check()?;

    if request.request_type() == NbdRequestType::Write {
        let buf_slice = buf.get_mut_ref(request.length as usize);
        con_r.read_exact(buf_slice.into_slice()).await?;
    }

    Ok((buf, con_r, request))
}

/// Returns:
/// .0: Buffer
/// .1: Whether to close the connection
async fn request_worker(
    queue: &IoQueue,
    read_only: bool,
    con_w: Arc<tokio::sync::Mutex<NetWriteHalf>>,
    req_params: NbdRequest,
    mut buf: RequestBuffer,
) -> BlockResult<(RequestBuffer, bool)> {
    let bincode = bincode::DefaultOptions::new()
        .with_fixint_encoding()
        .with_big_endian();

    match req_params.request_type() {
        NbdRequestType::Read => {
            let req_len = req_params.length as usize;
            let buf_slice = buf.get_mut_ref(req_len);

            let (reply, send_buffer) = match queue.read(buf_slice, req_params.offset).await {
                Ok(()) => (NbdSimpleReply::new(req_params, NbdResult::Ok), true),
                Err(err) => (NbdSimpleReply::new(req_params, err.into()), false),
            };

            let reply = bincode.serialize(&reply).unwrap();

            let mut con_w = con_w.lock().await;
            con_w.write_all(&reply).await?;
            if send_buffer {
                // We have just read this data, so this is safe
                let buf_slice = unsafe { buf.get_ref(req_len) }.into_slice();
                con_w.write_all(buf_slice).await?;
            }

            Ok((buf, false))
        }

        NbdRequestType::Write => {
            if read_only {
                let err: BlockError = std::io::ErrorKind::ReadOnlyFilesystem.into();
                let reply = NbdSimpleReply::new(req_params, err.into());
                let reply = bincode.serialize(&reply).unwrap();
                let mut con_w = con_w.lock().await;
                con_w.write_all(&reply).await?;
                return Ok((buf, false));
            }

            // Safe because `receive_request()` has read this data from the client
            let buf_slice = unsafe { buf.get_ref(req_params.length as usize) };

            let reply = match queue.write(buf_slice, req_params.offset).await {
                Ok(()) => NbdSimpleReply::new(req_params, NbdResult::Ok),
                Err(err) => NbdSimpleReply::new(req_params, err.into()),
            };

            let reply = bincode.serialize(&reply).unwrap();

            let mut con_w = con_w.lock().await;
            con_w.write_all(&reply).await?;

            Ok((buf, false))
        }

        NbdRequestType::Flush => {
            let reply = match queue.flush().await {
                Ok(()) => NbdSimpleReply::new(req_params, NbdResult::Ok),
                Err(err) => NbdSimpleReply::new(req_params, err.into()),
            };

            let reply = bincode.serialize(&reply).unwrap();

            let mut con_w = con_w.lock().await;
            con_w.write_all(&reply).await?;

            Ok((buf, false))
        }

        NbdRequestType::Disc => Ok((buf, true)),
    }
}

impl Export {
    pub fn name(&self) -> &str {
        &self.info.name
    }

    pub fn quit(&self) {
        // We do not care whether the channel is already full, one quit suffices
        let _: Result<(), _> = self.quit.try_send(());
    }

    pub fn nbd_info(&self) -> NbdInfoExport {
        let mut flags = NbdFlag::HasFlags as u16;
        if self.read_only {
            flags |= NbdFlag::ReadOnly as u16;
        }

        NbdInfoExport {
            info_type: 0,
            export_size: self.info.size.load(Ordering::Relaxed),
            transmission_flags: flags,
        }
    }

    pub fn nbd_info_block_size(&self) -> NbdInfoBlockSize {
        let req_align = self.info.req_align.load(Ordering::Relaxed);
        NbdInfoBlockSize::new(req_align, req_align, (u32::MAX / 2).next_power_of_two())
    }

    pub fn add_connection(&self, con: NetStream) {
        let _: Result<(), _> = self.add_connection.send(con);
    }
}
