use crate::error::BlockResult;
use crate::helpers::{FileExt, IoVector, IoVectorMut};
use std::cmp::Ordering::{Equal, Greater, Less};
use std::io;
use std::os::unix::io::{AsRawFd, RawFd};


/// Encapsulates a raw fd that is closed when dropped
#[derive(Debug)]
pub struct OwnedFd {
    fd: RawFd,
}

impl From<RawFd> for OwnedFd {
    fn from(fd: RawFd) -> Self {
        OwnedFd { fd }
    }
}

impl std::ops::Deref for OwnedFd {
    type Target = RawFd;

    fn deref(&self) -> &RawFd {
        &self.fd
    }
}

impl AsRawFd for OwnedFd {
    fn as_raw_fd(&self) -> RawFd {
        self.fd
    }
}

impl Clone for OwnedFd {
    fn clone(&self) -> Self {
        let duped = unsafe { libc::dup(self.fd) };
        assert!(duped >= 0);
        OwnedFd { fd: duped }
    }
}

impl Drop for OwnedFd {
    fn drop(&mut self) {
        unsafe { libc::close(self.fd) };
    }
}


/// Encapsulates a Copy-able handle on an open file (e.g. an integer).  On Unix, this is a `RawFd`.
#[derive(Clone, Copy, Debug)]
pub struct FileRef(RawFd);

impl<F: AsRawFd> From<&F> for FileRef {
    fn from(file: &F) -> Self {
        FileRef(file.as_raw_fd())
    }
}

impl AsRawFd for FileRef {
    fn as_raw_fd(&self) -> RawFd {
        self.0
    }
}


impl<F: AsRawFd> FileExt for F {
    fn read_exact_vectored_at(
        &self,
        mut bufv: IoVectorMut<'_>,
        mut offset: u64,
    ) -> BlockResult<()> {
        let fd = self.as_raw_fd();
        while !bufv.is_empty() {
            // Safe because the vector elements will only be used in `preadv()`, and we keep `bufv`
            // until after it returns
            let iovec = unsafe { bufv.as_iovec() };
            let bytes_read = unsafe {
                libc::preadv(
                    fd,
                    iovec.as_ptr(),
                    iovec.len().try_into()?,
                    offset.try_into()?,
                )
            };

            match bytes_read.cmp(&0) {
                Greater => {
                    // Success, go on
                    bufv = bufv.split_tail_at(bytes_read as u64);
                    offset += bytes_read as u64;
                }

                Equal => {
                    // Early EOF
                    bufv.fill(0);
                    break;
                }

                Less => {
                    // Error
                    let err = io::Error::last_os_error();
                    // Ignore EINTR, just retry then
                    if err.raw_os_error() != Some(libc::EINTR) {
                        return Err(err.into());
                    }
                }
            }
        }

        Ok(())
    }

    fn write_all_vectored_at(&self, mut bufv: IoVector<'_>, mut offset: u64) -> BlockResult<()> {
        let fd = self.as_raw_fd();
        while !bufv.is_empty() {
            // Safe because the vector elements will only be used in `pwritev()`, and we keep
            // `bufv` until after it returns
            let iovec = unsafe { bufv.as_iovec() };
            let bytes_written = unsafe {
                libc::pwritev(
                    fd,
                    iovec.as_ptr(),
                    iovec.len().try_into()?,
                    offset.try_into()?,
                )
            };

            match bytes_written.cmp(&0) {
                Greater => {
                    // Success, go on
                    bufv = bufv.split_tail_at(bytes_written as u64);
                    offset += bytes_written as u64;
                }

                Equal => {
                    // Not quite clear what happened, but definitely wrong
                    return Err(format!(
                        "Failed to write any data at offset {}, still had {} bytes left",
                        offset,
                        bufv.len(),
                    )
                    .into());
                }

                Less => {
                    // Error
                    let err = io::Error::last_os_error();
                    // Ignore EINTR, just retry then
                    if err.raw_os_error() != Some(libc::EINTR) {
                        return Err(err.into());
                    }
                }
            }
        }

        Ok(())
    }
}
