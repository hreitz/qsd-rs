use crate::error::BlockResult;
use crate::helpers::{FileExt, IoVector, IoVectorMut};
use std::io;
use std::os::windows::io::{AsRawHandle, RawHandle};


/// Encapsulates a raw fd that is closed when dropped
#[derive(Debug)]
pub struct OwnedFd {
    handle: RawHandle,
}

impl From<RawHandle> for OwnedFd {
    fn from(handle: RawHandle) -> Self {
        OwnedFd { handle }
    }
}

impl std::ops::Deref for OwnedFd {
    type Target = RawHandle;

    fn deref(&self) -> &RawHandle {
        &self.handle
    }
}

impl AsRawHandle for OwnedFd {
    fn as_raw_handle(&self) -> RawHandle {
        self.handle
    }
}

impl Drop for OwnedFd {
    fn drop(&mut self) {
        unsafe {
            windows_sys::Win32::Foundation::CloseHandle(
                self.handle as windows_sys::Win32::Foundation::HANDLE,
            )
        };
    }
}


#[derive(Clone, Copy, Debug)]
pub struct FileRef(RawHandle);

// Blocked because Win32 handles are pointers, but they can still be used across threads
unsafe impl Send for FileRef {}
unsafe impl Sync for FileRef {}

impl<F: AsRawHandle> From<&F> for FileRef {
    fn from(file: &F) -> Self {
        FileRef(file.as_raw_handle())
    }
}

impl AsRawHandle for FileRef {
    fn as_raw_handle(&self) -> RawHandle {
        self.0
    }
}


impl<F: AsRawHandle> FileExt for F {
    fn read_exact_vectored_at(&self, bufv: IoVectorMut<'_>, mut offset: u64) -> BlockResult<()> {
        let handle = self.as_raw_handle() as windows_sys::Win32::Foundation::HANDLE;
        let buffers = bufv.into_inner();

        // Done without `for .. in` so we can zero-fill remaining buffers on early EOF
        let mut buffer_iter = buffers.into_iter();
        while let Some(mut buf) = buffer_iter.next() {
            let mut buf_slice: &mut [u8] = &mut *buf;

            while !buf_slice.is_empty() {
                let mut bytes_read: u32 = 0;
                let mut overlapped = windows_sys::Win32::System::IO::OVERLAPPED {
                    Internal: 0,
                    InternalHigh: 0,
                    Anonymous: windows_sys::Win32::System::IO::OVERLAPPED_0 {
                        Anonymous: windows_sys::Win32::System::IO::OVERLAPPED_0_0 {
                            Offset: offset as u32,
                            OffsetHigh: (offset >> 32) as u32,
                        },
                    },
                    hEvent: 0,
                };

                // TODO: Put u32 limitation into block limits and let node.rs handle it
                let buf_len: u32 = buf_slice.len().try_into()?;
                let buf_ptr = buf_slice.as_mut_ptr();

                // Note: ReadFile() is one reason why we use windows-sys and not the higher-level
                // windows crate.  `windows::...::ReadFile()` force-casts many things without error
                // checking, e.g. unchecked `transmute()` on the buffer pointer to whatever pointer
                // type may be required, or using `lpbuffer.len() as _` (basically) for the
                // `nNumberOfBytesToRead` parameter, which is just unnecessarily unsafe.  Better to
                // use the low-level interface directly and safely do all necessary casts.
                let success = unsafe {
                    windows_sys::Win32::Storage::FileSystem::ReadFile(
                        handle,
                        // Note: windows-sys 0.48.1 will obsolete this cast
                        buf_ptr as *mut core::ffi::c_void,
                        buf_len,
                        &mut bytes_read as *mut _,
                        &mut overlapped as *mut _,
                    )
                };
                if success == windows_sys::Win32::Foundation::FALSE {
                    return Err(io::Error::last_os_error().into());
                }
                if bytes_read == 0 {
                    // Read zeroes post-EOF
                    buf_slice.fill(0);
                    break;
                }

                offset += bytes_read as u64;
                buf_slice = &mut buf_slice[(bytes_read as usize)..];
            }
        }

        // If buffers remain, we hit an early EOF, and fill those buffers with zeroes
        for mut buf in buffer_iter {
            buf.fill(0);
        }

        Ok(())
    }

    fn write_all_vectored_at(&self, bufv: IoVector<'_>, mut offset: u64) -> BlockResult<()> {
        let handle = self.as_raw_handle() as windows_sys::Win32::Foundation::HANDLE;
        let buffers = bufv.into_inner();

        for buf in buffers {
            let mut buf_slice: &[u8] = &*buf;

            while !buf_slice.is_empty() {
                let mut bytes_written: u32 = 0;
                let mut overlapped = windows_sys::Win32::System::IO::OVERLAPPED {
                    Internal: 0,
                    InternalHigh: 0,
                    Anonymous: windows_sys::Win32::System::IO::OVERLAPPED_0 {
                        Anonymous: windows_sys::Win32::System::IO::OVERLAPPED_0_0 {
                            Offset: offset as u32,
                            OffsetHigh: (offset >> 32) as u32,
                        },
                    },
                    hEvent: 0,
                };

                // TODO: Put u32 limitation into block limits and let node.rs handle it
                let buf_len: u32 = buf_slice.len().try_into()?;
                let buf_ptr = buf_slice.as_ptr();

                let success = unsafe {
                    windows_sys::Win32::Storage::FileSystem::WriteFile(
                        handle,
                        buf_ptr,
                        buf_len,
                        &mut bytes_written as *mut _,
                        &mut overlapped as *mut _,
                    )
                };
                if success == windows_sys::Win32::Foundation::FALSE {
                    return Err(io::Error::last_os_error().into());
                }
                if bytes_written == 0 {
                    return Err(format!(
                        "Failed to write any data at offset {}, still had at least {} bytes left",
                        offset,
                        buf_slice.len(),
                    )
                    .into());
                }

                offset += bytes_written as u64;
                buf_slice = &buf_slice[(bytes_written as usize)..];
            }
        }

        Ok(())
    }
}
