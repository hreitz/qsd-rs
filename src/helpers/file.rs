use crate::error::BlockResult;
use crate::helpers::{IoVector, IoVectorMut};

#[cfg(unix)]
mod unix;
#[cfg(windows)]
mod windows;

#[cfg(unix)]
pub use unix::*;
#[cfg(windows)]
pub use windows::*;


pub trait FileExt {
    /// Read data from this file into `bufv`, starting at `offset`, and do not stop until `bufv` is
    /// full or an error occurs.  If the end of file is encountered before `bufv` is full, fill the
    /// rest of it with zeroes, and return success.
    fn read_exact_vectored_at(&self, bufv: IoVectorMut<'_>, offset: u64) -> BlockResult<()>;

    /// Write data from `bufv` into this file, starting at `offset`, and do not stop until all data
    /// has been written or an error occurs.
    fn write_all_vectored_at(&self, bufv: IoVector<'_>, offset: u64) -> BlockResult<()>;
}
