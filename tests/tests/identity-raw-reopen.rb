#!/usr/bin/ruby

# Create an export on top of a raw node, on top of a protocol node, then create
# a writer on the raw node, and reopen the node with no changes (i.e. reusing
# its current child).  Regardless of whether raw reuses the existing `NodeUser`
# in such a case, or whether it is sufficiently equipped to juggle permissions
# such that adding a new user and dropping the old one won't interfere with
# itself, this simply should not fail.

# test: {
#     "args": [
#         ["blkio", "threads"]
#     ]
# }

require ENV['RSD_RB']
require 'shellwords'

protocol_driver = ARGV[0]

rsd = RSD.new(ENV['RSD'])

test_img = "#{ENV['IMGS_DIR']}/test.img"
system("truncate -s 256M #{test_img.shellescape}")

begin
    rsd.qmp.blockdev_add({
        node_name: 'fmt',
        driver: 'raw',
        file: {
            node_name: 'prot',
            driver: 'file',
            aio: protocol_driver,
            filename: test_img,
        },
    })
rescue QMPError => e
    desc = e.object['error']['desc']
    # TODO: Improve error message
    if desc.include?('data did not match any variant of untagged enum Config')
        puts '---'
        puts "File AIO mode #{protocol_driver} not supported"
        exit 2
    end
    raise e
end

rsd.qmp.server_start({
    id: 'nbd-server',
    type: 'nbd',
    addr: RSD.socket_obj(rsd.socket_addr('nbd.sock')),
})

rsd.qmp.block_export_add({
    id: 'exp0',
    type: 'nbd',
    server: 'nbd-server',
    node_name: 'fmt',
    writable: true,
})

rsd.qmp.blockdev_reopen({
    options: [
        {
            node_name: 'fmt',
            driver: 'raw',
            file: 'prot',
        },
    ],
})

rsd.qmp.quit
rsd.wait
