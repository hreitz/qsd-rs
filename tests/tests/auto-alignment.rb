#!/usr/bin/ruby

# Test auto-alignment of unaligned writes.  For this, we use the bench export, because it allows us
# to bypass the alignment requirements.  (If we used qemu-io over NBD, it would probably adhere to
# the limits announced by the NBD server, which are exactly the node's alignment requirements.)

# test: {
#     "args": [
#     ]
# }

require 'shellwords'

def random_ops(r, count, node_size)
    count.times.map { |_|
        ofs = (r.rand * node_size).to_i
        len = (r.rand * 16 * 1024).to_i
        if ofs + len > node_size
            len = node_size - ofs
        end
        [ofs, len]
    }
end

def run_qemu_io(ops, args, pat)
    cmd_file = "#{ENV['SOCK_DIR']}/qemu-io-cmds"
    out_file = "#{ENV['SOCK_DIR']}/qemu-io-out"
    IO.write(cmd_file, (ops.map { |op|
        "aio_write -P #{pat} #{op[0]} #{op[1]}"
    } + ['aio_flush']) * $/)
    if !system("qemu-io #{args} &>#{out_file.shellescape} <#{cmd_file.shellescape}")
        system("cat #{out_file.shellescape} >&2")
        return false
    end
    return true
end

def run_raw_qemu_io(ops, filename, pat)
    run_qemu_io(ops, "-f raw #{filename.shellescape}", pat)
end

def run_bench_io(ops, filename, pat, operation)
    out_file = "#{ENV['SOCK_DIR']}/rsd-out"
    # TODO: Abysmal performance
    ops.each do |op|
        if !system("#{ENV['RSD'].shellescape} --blockdev #{"{\"node-name\": \"exp0\", \"driver\": \"bench-export\", \"exported\": {\"node-name\": \"node0\", \"driver\": \"file\", \"filename\": \"#{filename}\", \"cache\": {\"direct\": true}}, \"request-type\": \"#{operation}\", \"offset\": #{op[0]}, \"request-size\": #{op[1]}, \"pattern\": #{pat}, \"total\": 1, \"simultaneous\": 1}".shellescape} 2>#{out_file.shellescape} >/dev/null") || File.size(out_file) > 0
            system("cat #{out_file.shellescape} >&2")
            return false
        end
    end
    return true
end

seed = ARGV[0]
if seed
    seed = Integer(seed)
else
    seed = Random.new_seed
end
puts("=== Seed: #{seed} ===")
r = Random.new(seed)

o_direct_dir = "#{File.dirname(__FILE__)}/test-#{$$}"
if !system("mkdir #{o_direct_dir.shellescape}")
    puts '---'
    puts "Failed to create O_DIRECT test dir #{o_direct_dir}"
    exit 2
end

test_img = "#{o_direct_dir}/test.img"
compare_img = "#{o_direct_dir}/compare.img"

system("truncate -s #{1 * 1024 * 1024} #{test_img.shellescape}")
system("truncate -s #{1 * 1024 * 1024} #{compare_img.shellescape}")

if !system("qemu-io -t none -f raw -c quit #{test_img.shellescape}")
    system("rm -rf #{o_direct_dir.shellescape}")

    puts '---'
    puts "Failed to open #{test_img} in O_DIRECT mode"
    exit 2
end

success = true

a_writes = random_ops(r, (r.rand * 64).to_i + 64, 1 * 1024 * 1024)
b_writes = random_ops(r, (r.rand * 64).to_i + 64, 1 * 1024 * 1024)
c_writes = random_ops(r, (r.rand * 64).to_i + 64, 1 * 1024 * 1024)

success &= run_bench_io(a_writes, compare_img, 0xa, 'write')
success &= run_bench_io(b_writes, compare_img, 0xb, 'write')
success &= run_bench_io(c_writes, compare_img, 0xc, 'write')

success &= run_raw_qemu_io(a_writes, test_img, 0xa)
success &= run_raw_qemu_io(b_writes, test_img, 0xb)
success &= run_raw_qemu_io(c_writes, test_img, 0xc)

success &= run_bench_io(c_writes, compare_img, 0xc, 'read')

if success
    puts('Comparing test.img with compare.img:')
    success &= system("qemu-img compare -f raw -F raw #{test_img.shellescape} #{compare_img.shellescape}")
end

system("rm -rf #{o_direct_dir.shellescape}")

if success
    exit 0
else
    exit 1
end
