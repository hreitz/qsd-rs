#!/usr/bin/ruby

# Create three raw nodes on top of each other, then a null node on the bottom,
# and a writing bench export on top.  Swap the bottom two raw nodes with a
# blockdev-reopen.  See that it works.
# (The argument controls whether the blockdev-reopen argument should be given as
# a single JSON object, or be separated per node.)

# test: {
#     "args": [
#         ["split", "joined"]
#     ]
# }

require ENV['RSD_RB']

reopen_mode = ARGV[0]

rsd = RSD.new(ENV['RSD'])

rsd.qmp.blockdev_add({
    node_name: 'raw-top',
    driver: 'raw',
    file: {
        node_name: 'raw-1',
        driver: 'raw',
        file: {
            node_name: 'raw-0',
            driver: 'raw',
            file: {
                node_name: 'prot',
                driver: 'null',
                size: 1 * 1024 * 1024 * 1024,
            },
        },
    },
})

rsd.qmp.block_export_add({
    id: 'exp0',
    type: 'bench',
    node_name: 'raw-top',
    request_type: 'write',
})

sleep(0.1)

raw0_options = {
    node_name: 'raw-0',
    driver: 'raw',
    file: 'raw-1',
}

raw1_options = {
    node_name: 'raw-1',
    driver: 'raw',
    file: 'prot',
}

raw_top_options = {
    node_name: 'raw-top',
    driver: 'raw',
    file: 'raw-0',
}

if reopen_mode == 'split'
    options = [
        raw0_options,
        raw1_options,
        raw_top_options,
    ]
else
    raw0_options[:file] = raw1_options
    raw_top_options[:file] = raw0_options
    options = [raw_top_options]
end

rsd.qmp.blockdev_reopen({ options: options })

sleep(0.1)

rsd.qmp.quit
rsd.wait
