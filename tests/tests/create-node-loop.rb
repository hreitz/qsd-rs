#!/usr/bin/ruby

# Create a raw node on top of a null node, then add another raw node on top.
# Then try to point the bottom raw node's `file` link to the top raw node.
# Verify that rsd reports an error.

# test: {
# }

require ENV['RSD_RB']


rsd = RSD.new(ENV['RSD'])

rsd.qmp.blockdev_add({
    node_name: 'prot',
    driver: 'null',
    size: 1 * 1024 * 1024 * 1024,
})

rsd.qmp.blockdev_add({
    node_name: 'raw-0',
    driver: 'raw',
    file: 'prot',
})

rsd.qmp.blockdev_add({
    node_name: 'raw-1',
    driver: 'raw',
    file: 'raw-0',
})

begin
    rsd.qmp.blockdev_reopen({
        options: [
            {
                node_name: 'raw-0',
                driver: 'raw',
                file: 'raw-1',
            },
        ],
    })

    $stderr.puts("Adding the node should have failed")
    exit 1
rescue QMPError => error
    if !error.object['error']['desc'].include?('create a cycle through')
        $stderr.puts("Unexpected error: #{error.inspect}")
        exit 1
    end
end

rsd.qmp.quit
rsd.wait
