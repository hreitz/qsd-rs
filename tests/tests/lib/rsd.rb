#!/usr/bin/ruby

# rsd: Management of rsd instances
# Copyright (C) 2017, 2022 Hanna Reitz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'json'
require 'shellwords'
require 'socket'
require (File.realpath(File.dirname(__FILE__)) + '/qmp.rb')


$qemu_input = ''


class RSD
    $instance_counter = 0

    def initialize(*command_line)
        @this_id = "#{Process.pid}-#{$instance_counter}"
        @next_port = 50413 + ENV['JOB_INDEX'].to_i * 256 + $instance_counter * 16

        $instance_counter += 1

        test_unix_chardev = {
            id: 'test',
            backend: {
                type: 'socket',
                data: {
                    addr: {
                        type: 'unix',
                    },
                },
            },
        }
        # If there is unix socket support, the error message will complain about the missing path ("missing field").  If
        # there is not, it will complain about type=unix ("unknown variant").
        @has_unix_sockets = `#{
            (command_line + ['--chardev', JSON.unparse(test_unix_chardev)])
                .map { |arg| arg.shellescape } * ' '
        } 2>&1`.include?('missing field')

        @qmp_socket_addr = self.socket_addr("rsd.rb-qmp-#{@this_id}")
        if @qmp_socket_addr.kind_of?(String)
            @qmp_socket = UNIXServer.new(@qmp_socket_addr)
        else
            @qmp_socket = TCPServer.new(@qmp_socket_addr)
        end

        c_stdin, @stdin = IO.pipe()
        @stdout, c_stdout = IO.pipe()

        command_line.map! do |arg|
            if arg.kind_of?(Hash)
                JSON.unparse(qmp_replace_underscores(arg))
            else
                arg
            end
        end

        @child = Process.fork()
        if !@child
            chardev = {
                id: 'char0',
                backend: {
                    type: 'socket',
                    data: {
                        addr: RSD.socket_obj(@qmp_socket_addr),
                    },
                },
            }

            monitor = {
                id: 'mon0',
                chardev: chardev[:id],
            }

            add_args = ['--chardev', JSON.unparse(chardev),
                        '--monitor', JSON.unparse(monitor)]

            STDIN.reopen(c_stdin)
            STDOUT.reopen(c_stdout)

            Process.exec(*command_line, *add_args)
            exit 1
        end

        @qmp = nil
    end

    # Returns some socket address for communication with RSD.  If unix sockets are supported, returns a string that is a
    # path name (based off of the @name given).  Otherwise, returns an integer that is (supposed to be) a free port to
    # use for TCP.
    def socket_addr(name)
        if @has_unix_sockets
            "#{ENV['SOCK_DIR']}/#{name}"
        else
            port = @next_port
            @next_port += 1
            port
        end
    end

    # Returns an addr object for use with QMP.  @socket_addr is the value returned by `socket_addr()`.
    def self.socket_obj(socket_addr)
        if socket_addr.kind_of?(String)
            {
                type: 'unix',
                path: socket_addr,
            }
        else
            {
                type: 'inet',
                host: '127.0.0.1',
                port: socket_addr.to_s,
            }
        end
    end

    # Returns a qemu URL for an NBD socket on the given address, as returned by `socket_addr()`.
    def self.nbd_socket_url(socket_addr, export_name)
        if socket_addr.kind_of?(String)
            "nbd+unix:///#{export_name}?socket=#{socket_addr}"
        else
            "nbd+tcp://127.0.0.1:#{socket_addr}/#{export_name}"
        end
    end

    def qmp
        if !@qmp
            @qmp_con = @qmp_socket.accept()
            @qmp = QMP.new(@qmp_con)
        end

        @qmp
    end

    def stdin
        @stdin
    end

    def stdout
        @stdout
    end

    def cleanup()
        @stdout.close if @stdout

        @child = nil
        begin
            File.delete(@qmp_socket_fname)
        rescue
        end
    end

    # If wait is not set, the caller has to call it (or .cleanup)
    # manually.
    def kill(signal='KILL', wait=true)
        Process.kill(signal, @child) if @child
        self.wait() if wait
    end

    def wait()
        Process.wait(@child) if @child
        self.cleanup()
    end

    def pid
        @child
    end

    def has_unix_sockets?
        @has_unix_sockets
    end
end
