#!/usr/bin/ruby

# Create a vhost-user-blk device on a node that already has a writer on it.
# See that a permission error is announced.

# test: {
#     "args": [
#         ["blkio", "sync", "threads", "null"]
#     ]
# }

require ENV['RSD_RB']

rsd = RSD.new(ENV['RSD'])

if !rsd.has_unix_sockets?
    puts '---'
    puts 'This test requires vhost-user-blk, but RSD has no unix socket suppot'
    exit 2
end

protocol_driver = ARGV[0]

img = "#{ENV['IMGS_DIR']}/test.img"
system("truncate -s 256M #{img.shellescape}")

opts = {
    node_name: 'prot',
}
if ['blkio', 'sync', 'threads'].include?(protocol_driver)
    opts[:driver] = 'file'
    opts[:aio] = protocol_driver
    opts[:filename] = img
end
if protocol_driver == 'null'
    opts[:driver] = 'null'
    opts[:size] = 256 * 1024 * 1024
end
rsd.qmp.blockdev_add(opts)

rsd.qmp.object_add({
    id: 'thread0',
    qom_type: 'iothread',
})

rsd.qmp.server_start({
    id: 'nbd-server',
    type: 'nbd',
    addr: {
        type: 'unix',
        path: "#{ENV['SOCK_DIR']}/nbd.sock",
    },
})

rsd.qmp.block_export_add({
    id: 'nbd-exp',
    type: 'nbd',
    server: 'nbd-server',
    node_name: 'prot',
    writable: true,
    iothread: 'thread0',
})

begin
    rsd.qmp.block_export_add({
        id: 'vblk-exp',
        type: 'vhost-user-blk',
        node_name: 'prot',
        iothreads: ['thread0'],
        addr: {
            type: 'unix',
            path: ENV['SOCK_DIR'] + '/vhost-user-blk.sock',
        },
    })
    $stderr.puts('block-export-add should have failed')
    exit 1
rescue QMPError => e
    if e.object['error']['desc'] != 'New parent node ":export:vblk-exp" (as child "exported") of node "prot" would block permissions already taken by node ":export:nbd-exp" (as child "exported"): write'
        $stderr.puts('Unexpected error message from block-export-add')
        exit 1
    end
end

rsd.qmp.quit
rsd.wait
