#!/usr/bin/ruby

# Create a file without write permissions, then open a block device on it, with
# either read-only=true or auto-read-only=true.  Then give or do not give write
# permissions to the file, and create a writing bench export on top.  Maybe it
# works, maybe it does not.  (It should only work when we have given write
# permissions and set auto-read-only.)

# test: {
#     "args": [
#         ["blkio", "sync", "threads"],
#         ["read-only", "auto-read-only"],
#         ["+w", "-w"],
#         ["raw", "no-raw"]
#     ]
# }

require ENV['RSD_RB']

protocol_driver = ARGV[0]
ro_mode = ARGV[1]
file_mode = ARGV[2]
raw = ARGV[3] == 'raw'

rsd = RSD.new(ENV['RSD'])

img = "#{ENV['IMGS_DIR']}/test.img"
system("truncate -s 256M #{img.shellescape}")
system("chmod -w #{img.shellescape}")

begin
    rsd.qmp.blockdev_add({
        node_name: 'protocol-node',
        driver: 'file',
        aio: protocol_driver,
        filename: img,
        ro_mode => true,
    })
rescue QMPError => e
    desc = e.object['error']['desc']
    # TODO: Improve error message
    if desc.include?('data did not match any variant of untagged enum Config')
        puts '---'
        puts "File AIO mode #{protocol_driver} not supported"
        exit 2
    end
    raise e
end

if raw
    rsd.qmp.blockdev_add({
        node_name: 'format-node',
        driver: 'raw',
        file: 'protocol-node',
    })
end

system("chmod #{file_mode.shellescape} #{img.shellescape}")

begin
    rsd.qmp.block_export_add({
        id: 'bench-exp',
        type: 'bench',
        node_name: raw ? 'format-node' : 'protocol-node',
        request_type: 'write',
    })
    if [ro_mode, file_mode] == ['auto-read-only', '+w']
        # pass, but give time for I/O errors and the like
        sleep(0.2)
    else
        $stderr.puts('Adding the writing bench export should have failed')
        exit 1
    end
rescue QMPError => e
    desc = e.object['error']['desc']
    case [ro_mode, file_mode]
    when ['read-only', '+w']
        if desc != 'Reopening node "protocol-node": Node "protocol-node" is read-only'
            $stderr.puts('Unexpected error message from block-export-add')
            exit 1
        end
    when ['read-only', '-w']
        if desc != 'Reopening node "protocol-node": Node "protocol-node" is read-only'
            $stderr.puts('Unexpected error message from block-export-add')
            exit 1
        end
    when ['auto-read-only', '+w']
        $stderr.puts('Adding the writing bench export should have succeeded')
        exit 1
    when ['auto-read-only', '-w']
        # Do not check for "Permission denied" -- that one depends on the locale and OS
        if !desc.start_with?('Reopening node "protocol-node": ')# || !desc.include?('Permission denied')
            $stderr.puts('Unexpected error message from block-export-add')
            exit 1
        end
    end
end

rsd.qmp.quit
rsd.wait
