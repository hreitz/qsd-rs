#!/usr/bin/ruby

# Create an export on top of a protocol node, either in one or more ephemeral
# threads, in a single named threads, or in multiple named threads.  In 'active'
# mode, create a reader on top of the export.  Then quit.  (Which should work,
# and should be done without significant delay.)

# test: {
#     "args": [
#         ["bench", "nbd", "vhost-user-blk"],
#         ["ephemeral-threads", "single-named-thread", "multiple-named-threads"],
#         ["null", "blkio", "file-async"],
#         ["active", "inactive"]
#     ]
# }

require ENV['RSD_RB']

export_type = ARGV[0]
thread_config = ARGV[1]
protocol_type = ARGV[2]
export_active = ARGV[3]

# Configurations not worth testing
if export_type == 'bench' && thread_config == 'multiple-named-threads'
    exit 0
end
if export_type == 'bench' && export_active == 'inactive'
    exit 0
end

rsd = RSD.new(ENV['RSD'])

if thread_config == 'ephemeral-threads'
    case export_type
    when 'bench'
        queue_count = 1
    when 'nbd'
        queue_count = 1
    when 'vhost-user-blk'
        queue_count = 24
    else
        $stderr.puts("Bad export type '#{export_type}'")
        exit 1
    end
    named_threads = nil
elsif thread_config == 'single-named-thread'
    queue_count = 1
    named_threads = ['thr0']
else
    case export_type
    when 'nbd'
        queue_count = 1
        named_threads = 2.times.map { |i| "thr#{i}" }
    when 'vhost-user-blk'
        queue_count = 24
        named_threads = 24.times.map { |i| "thr#{i}" }
    else
        $stderr.puts("Cannot use multiple-named-threads with export type '#{export_type}'")
        exit 1
    end
end

if named_threads
    named_threads.each do |thread|
        rsd.qmp.object_add({
            id: thread,
            qom_type: 'iothread',
        })
    end
end

begin
    case protocol_type
    when 'null'
        rsd.qmp.blockdev_add({
            node_name: 'prot',
            driver: 'null',
            size: 1 * 1024 * 1024,
        })
    when 'blkio'
        img = "#{ENV['IMGS_DIR']}/test.img"
        system("truncate -s 1M #{img.shellescape}")
        rsd.qmp.blockdev_add({
            node_name: 'prot',
            driver: 'file',
            aio: 'blkio',
            filename: img,
        })
    when 'file-async'
        img = "#{ENV['IMGS_DIR']}/test.img"
        system("truncate -s 1M #{img.shellescape}")
        rsd.qmp.blockdev_add({
            node_name: 'prot',
            driver: 'file',
            aio: 'threads',
            filename: img,
        })
    end
rescue QMPError => e
    desc = e.object['error']['desc']
    # TODO: Improve error message
    if desc.include?('data did not match any variant of untagged enum Config')
        puts '---'
        puts "Protocol type #{protocol_type} not supported"
        exit 2
    end
    raise e
end

rsd.qmp.blockdev_add({
    node_name: 'fmt',
    driver: 'raw',
    file: 'prot',
})

child = nil

case export_type
when 'bench'
    params = {
        id: 'exp0',
        type: 'bench',
        node_name: 'fmt',
        request_type: 'read',
    }
    if named_threads
        params[:iothread] = named_threads[0]
    end
    rsd.qmp.block_export_add(params)
when 'nbd'
    nbd_addr = rsd.socket_addr('nbd.sock')
    params = {
        id: 'nbd-server',
        type: 'nbd',
        addr: RSD.socket_obj(nbd_addr),
    }
    if named_threads
        params[:iothread] = named_threads[0]
    end
    rsd.qmp.server_start(params)

    params = {
        id: 'exp0',
        type: 'nbd',
        server: 'nbd-server',
        node_name: 'fmt',
        writable: true,
    }
    if named_threads
        params[:iothread] = named_threads[-1]
    end
    rsd.qmp.block_export_add(params)

    if export_active == 'active'
        child = fork
        if !child
            exec(ENV['QEMU_IMG'], 'bench', '-f', 'nbd', RSD.nbd_socket_url(nbd_addr, 'fmt'))
        end
        # Wait for start
        sleep 0.1
    end
when 'vhost-user-blk'
    if !rsd.has_unix_sockets?
        puts '---'
        puts 'No support for unix sockets, cannot use vhost-user-blk'
        exit 2
    end

    if export_active == 'active'
        error = `#{ENV['QEMU_IMG'].shellescape} bench --image-opts driver=virtio-blk-vhost-user 2>&1`
        if error.include?('Unknown driver')
            puts '---'
            puts 'qemu-img lacks virtio-blk-vhost-user support, try pointing $QEMU_IMG to a build that has it'
            exit 2
        end
    end

    socket = "#{ENV['SOCK_DIR']}/vhost-user-blk.sock"

    params = {
        id: 'exp0',
        type: 'vhost-user-blk',
        node_name: 'fmt',
        num_queues: queue_count,
        writable: true,
        addr: {
            type: 'unix',
            path: socket,
        },
    }
    if named_threads
        params[:iothreads] = named_threads
    end
    rsd.qmp.block_export_add(params)

    if export_active == 'active'
        child = fork
        if !child
            exec(ENV['QEMU_IMG'], 'bench', '--image-opts',
                 "driver=virtio-blk-vhost-user,path=#{socket},cache.direct=on")
        end
        # Wait for start
        sleep 0.1
    end
else
    $stderr.puts("Bad export type '#{export_type}'")
    exit 1
end

rsd.qmp.quit
rsd.wait

if child
    Process.kill('KILL', child)
    Process.wait(child)
end
