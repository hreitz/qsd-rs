#!/usr/bin/ruby

# Test simple copy cases.
# Parameter 1: Which copy mode to test
# Parameter 2: Whether to pivot to the target or not (in non-CBW modes)
# Parameter 3: Whether to have the copy node disappear automatically or manually.
# Parameter 4: Whether to have the copy job disappear automatically or manually.
# Parameter 5: Whether the add all nodes at once; or whether to create the structure without the
#              copy node first, and then insert it; or whether to use qemu's "legacy" commands.

# test: {
#     "args": [
#         ["lazy", "copy-after-write", "copy-before-write"],
#         ["pivot", "no-pivot"],
#         ["auto", "manual-finalize"],
#         ["auto", "manual-dismiss"],
#         ["all-at-once", "insert", "legacy"],
#         ["raw", "qcow2"]
#     ]
# }

require ENV['RSD_RB']

def random_ops(r, count, node_size)
    count.times.map { |_|
        ofs = (r.rand * node_size).to_i
        len = (r.rand * 1024 * 1024).to_i
        if ofs + len > node_size
            len = node_size - ofs
        end
        [ofs, len]
    }
end

def run_qemu_io(ops, args, pat)
    cmd_file = "#{ENV['SOCK_DIR']}/qemu-io-cmds"
    out_file = "#{ENV['SOCK_DIR']}/qemu-io-out"
    IO.write(cmd_file, (ops.map { |op|
        "aio_write -P #{pat} #{op[0]} #{op[1]}"
    } + ['aio_flush']) * $/)
    if !system("qemu-io #{args} &>#{out_file.shellescape} <#{cmd_file.shellescape}")
        system("cat #{out_file.shellescape} >&2")
        exit 1
    end
end

def run_nbd_qemu_io(ops, nbd_addr, exp_name, pat)
    run_qemu_io(ops, "-f nbd #{RSD.nbd_socket_url(nbd_addr, exp_name).shellescape}", pat)
end

def run_raw_qemu_io(ops, filename, pat)
    run_qemu_io(ops, "-f #{$format} #{filename.shellescape}", pat)
end

copy_mode = ARGV[0]
switch_over = ARGV[1] == 'pivot'
manual_finalize = ARGV[2] == 'manual-finalize'
manual_dismiss = ARGV[3] == 'manual-dismiss'
all_at_once = ARGV[4] == 'all-at-once'
legacy = ARGV[4] == 'legacy'
$format = ARGV[5]

need_completion = copy_mode != 'copy-before-write'

seed = ARGV[6]
if seed
    seed = Integer(seed)
else
    seed = Random.new_seed
end
puts("=== Seed: #{seed} ===")
r = Random.new(seed)

rsd = RSD.new(ENV['RSD'])

template = "#{ENV['IMGS_DIR']}/template.img"
test = "#{ENV['IMGS_DIR']}/test.img"
target = "#{ENV['IMGS_DIR']}/target.img"

system("dd if=/dev/urandom of=#{template.shellescape} bs=64k count=#{1024 * 1024 * 1024 / (64 * 1024)}")
system("#{ENV['QEMU_IMG'].shellescape} convert -f raw -O #{$format} #{template.shellescape} #{test.shellescape}")
system("cp #{test.shellescape} #{template.shellescape}")
system("#{ENV['QEMU_IMG'].shellescape} create -f #{$format} #{target.shellescape} #{1024 * 1024 * 1024}")

pre_writes = random_ops(r, (r.rand * 512).to_i + 512, 1024 * 1024 * 1024)
post_ready_writes = random_ops(r, (r.rand * 512).to_i + 512, 1024 * 1024 * 1024)
post_writes = random_ops(r, (r.rand * 512).to_i + 512, 1024 * 1024 * 1024)

nbd_addr = rsd.socket_addr('nbd.sock')
rsd.qmp.server_start({
    id: 'nbd-server',
    type: 'nbd',
    addr: RSD.socket_obj(nbd_addr),
})

nbd_node = {
    node_name: 'nbd',
    driver: 'nbd-export',
    server: 'nbd-server',
    name: 'some-export',
    writable: true,
}
copy_node = {
    node_name: 'copy',
    driver: 'copy',
    job_id: 'copy-job',
    mode: copy_mode,
    auto_finalize: !manual_finalize,
    auto_dismiss: !manual_dismiss,
}
source_node = {
    node_name: 'source',
    driver: $format,
    file: {
        node_name: 'source-proto',
        driver: 'file',
        filename: test,
    },
}
target_node = {
    node_name: 'target',
    driver: $format,
    file: {
        node_name: 'target-proto',
        driver: 'file',
        filename: target,
    },
}
if legacy
    rsd.qmp.blockdev_add({
        **nbd_node,
        exported: source_node,
    })
    rsd.qmp.blockdev_add({
        **target_node,
    })
    if copy_mode == 'copy-before-write'
        rsd.qmp.blockdev_backup({
            job_id: 'copy-job',
            device: 'source',
            target: 'target',
            sync: 'full',
            auto_finalize: !manual_finalize,
            auto_dismiss: !manual_dismiss,
        })
    else
        rsd.qmp.blockdev_mirror({
            job_id: 'copy-job',
            device: 'source',
            target: 'target',
            sync: 'full',
            copy_mode: copy_mode == 'copy-after-write' ? 'write-blocking' : 'background',
            auto_finalize: !manual_finalize,
            auto_dismiss: !manual_dismiss,
        })
    end
elsif all_at_once
    begin
        rsd.qmp.blockdev_add({
            **nbd_node,
            exported: {
                **copy_node,
                active: true,
                file: source_node,
                target: target_node,
            },
        })
    rescue QMPError => e
        if manual_finalize
            raise e
        end
        if e.object['error']['desc'] == 'Cannot use auto-finalize when active is immediately set'
            # Reasonable, but cannot continue
            rsd.qmp.quit
            rsd.wait
            exit 0
        end
        $stderr.puts('Unexpected error message from blockdev-add')
        exit 1
    end
else
    rsd.qmp.blockdev_add({
        **nbd_node,
        exported: source_node,
    })
    rsd.qmp.blockdev_add({
        **copy_node,
        active: false,
        file: 'source',
        target: target_node,
    })
    rsd.qmp.blockdev_reopen(options: [{
        **nbd_node,
        exported: {
            **copy_node,
            active: true,
            file: 'source',
            target: 'target',
        },
    }])
end

run_nbd_qemu_io(pre_writes, nbd_addr, 'some-export', 1)

# In CBW mode (!need_completion) without manual finalize, the job may already be gone
if need_completion || manual_finalize
    jobs = rsd.qmp.query_jobs
    if jobs.length != 1 || jobs[0]['id'] != 'copy-job'
        $stderr.puts('Expected exactly one job (the copy job) in the query-jobs result')

        rsd.qmp.quit
        rsd.wait
        exit 1
    end

    if need_completion
        if !['running', 'ready'].include?(jobs[0]['status'])
            $stderr.puts('Expected the copy job to be in running or ready state')

            rsd.qmp.quit
            rsd.wait
            exit 1
        end
    end
end

if need_completion
    rsd.qmp.event_wait('BLOCK_JOB_READY')
    run_nbd_qemu_io(post_ready_writes, nbd_addr, 'some-export', 2)
    if legacy
        if switch_over
            rsd.qmp.block_job_complete(device: 'copy-job')
        else
            rsd.qmp.block_job_cancel(device: 'copy-job')
        end
    else
        rsd.qmp.blockdev_stop({
            node_name: 'copy',
            mode: {
                copy_complete: {
                    switch_over: switch_over,
                }
            }
        })
    end
end

if manual_finalize
    rsd.qmp.event_wait('BLOCK_JOB_PENDING')
else
    rsd.qmp.event_wait('BLOCK_JOB_COMPLETED')
end
run_nbd_qemu_io(post_writes, nbd_addr, 'some-export', 3)
if manual_finalize
    if legacy
        rsd.qmp.block_job_finalize(id: 'copy-job')
        rsd.qmp.event_wait('NODE_FADED')
    else
        rsd.qmp.blockdev_reopen(options: [{
            **nbd_node,
            exported: switch_over ? 'target' : 'source',
        }])
        rsd.qmp.blockdev_del({
            node_name: 'copy',
        })
    end
else
    rsd.qmp.event_wait('NODE_FADED')
end

while rsd.qmp.event_wait('JOB_STATUS_CHANGE')['data']['status'] != 'concluded'
end

if manual_dismiss
    jobs = rsd.qmp.query_jobs
    if jobs.length != 1 || jobs[0]['id'] != 'copy-job'
        $stderr.puts('Expected exactly one job (the copy job) in the query-jobs result')

        rsd.qmp.quit
        rsd.wait
        exit 1
    end

    if jobs[0]['status'] != 'concluded'
        $stderr.puts('Expected the copy job to be in concluded state')

        rsd.qmp.quit
        rsd.wait
        exit 1
    end

    rsd.qmp.block_job_dismiss(id: 'copy-job')
end

while rsd.qmp.event_wait('JOB_STATUS_CHANGE')['data']['status'] != 'null'
end

if !rsd.qmp.query_jobs.empty?
    $stderr.puts('Expected no jobs in the query-jobs result')

    rsd.qmp.quit
    rsd.wait
    exit 1
end

rsd.qmp.quit
rsd.wait

if copy_mode == 'copy-before-write'
    puts("Comparing template.img with target.img:")
    if !system("qemu-img compare #{target.shellescape} #{template.shellescape}")
        exit 1
    end
    run_raw_qemu_io(pre_writes, template, 1)
    # no post-ready phase in CBW mode
    run_raw_qemu_io(post_writes, template, 3)
    puts("Comparing template.img with test.img:")
    if !system("qemu-img compare #{test.shellescape} #{template.shellescape}")
        exit 1
    end
else
    run_raw_qemu_io(pre_writes, template, 1)
    run_raw_qemu_io(post_ready_writes, template, 2)
    puts("Comparing template.img with dropped image (#{switch_over ? 'test.img' : 'target.img'}):")
    if !system("qemu-img compare #{(switch_over ? test : target).shellescape} #{template.shellescape}")
        exit 1
    end
    run_raw_qemu_io(post_writes, template, 3)
    puts("Comparing template.img with pivotee image (#{switch_over ? 'target.img' : 'test.img'}):")
    if !system("qemu-img compare #{(switch_over ? target : test).shellescape} #{template.shellescape}")
        exit 1
    end
end
