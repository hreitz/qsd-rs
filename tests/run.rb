#!/usr/bin/ruby

require 'json'
require 'shellwords'
require 'tmpdir'

if ENV['VERBOSE']
    $verbose = true
else
    $verbose = false
end

$all_counter = 0
$pass_counter = 0
$fail_counter = 0
$skip_counter = 0

$failed_tests = []
$skipped_tests = []

$running_tests = {}
$jobs = 1

# When running multiple jobs simultaneously, we want to give each one a currently-unique index that it can use to
# uniquify itself against other concurrently running jobs.  At the same time, these indices should be as small as
# possible (so they can be used e.g. to offset TCP ports).  This array blocks indices that are currently in use.
$job_indices = []

$status_screen = {}

def await_single_test()
    pid = Process.waitpid
    result = $?

    test = $running_tests[pid]

    if result.success?
        $pass_counter += 1
    elsif result.exitstatus == 2
        $skip_counter += 1
    else
        $fail_counter += 1
    end

    if !$verbose
        $status_screen[test[:test_file]][:done] += 1
    end

    if result.success?
        if $verbose
            puts("\033[32;1mSUCCESS\033[0m")
            puts
        else
            $status_screen[test[:test_file]][:line] += '.'
        end
    elsif result.exitstatus == 2
        if $verbose
            puts("\033[1mSKIP\033[0m")
            puts
        else
            skip_msg = IO.read(test[:stdout]).split($/)
            while skip_msg.shift.strip != '---'
            end
            $skipped_tests << {
                test: test[:test_file],
                args: test[:args],
                skip_msg: skip_msg * $/,
            }
            $status_screen[test[:test_file]][:line] += "\033[1ms\033[0m"
        end
    else
        if $verbose
            puts("\033[31;1mFAILURE\033[0m")
            puts
        else
            begin
                stdout = IO.read(test[:stdout])
            rescue
                stdout = nil
            end
            begin
                stderr = IO.read(test[:stderr])
            rescue Exception => e
                stderr = nil
            end

            $failed_tests << {
                test: test[:test_file],
                args: test[:args],
                stdout: stdout,
                stderr: stderr,
            }
            $status_screen[test[:test_file]][:line] += "\033[31;1mF\033[0m"
        end
    end

    system("rm -rf #{test[:sock_tmpdir].shellescape}")
    system("rm -rf #{test[:imgs_tmpdir].shellescape}")

    # Delete key only after removing the directories, so that Ctrl-C can still
    # delete everything if the rm -rf is interrupted
    $running_tests.delete(pid)

    $job_indices[test[:job_index]] = nil

    if !$verbose
        # Clear screen
        $stdout.write("\e[2J\e[;H")
        $status_screen.each do |test, progress|
            puts("#{test} (#{progress[:fmt] % progress[:done]}/#{progress[:total]}): #{progress[:line]}")
        end
    end
end

def await_single_test_interruptible()
    begin
        await_single_test()
    rescue Interrupt => e # Catch Ctrl-C
        $running_tests.each_key do |pid|
            begin
                Process.kill('INT', pid)
            rescue
                # Ignore exceptions
            end
        end
        Process.waitall
        $running_tests.each_value do |test|
            system("rm -rf #{test[:sock_tmpdir].shellescape}")
            system("rm -rf #{test[:imgs_tmpdir].shellescape}")
        end

        print_results_and_exit()
    end
end

def await_test_slot()
    while $running_tests.length >= $jobs
        await_single_test_interruptible()
    end
end

def await_all_tests()
    while !$running_tests.empty?
        await_single_test_interruptible()
    end
end

def print_results_and_exit()
    if $all_counter > 0
        puts()
        puts()
    end

    if $fail_counter == 0 && $skip_counter == 0
        puts("\033[32;1m#{$pass_counter}/#{$all_counter} PASSED\033[0m")
    elsif $fail_counter == 0 && $skip_counter > 0
        puts("\033[32;1m#{$pass_counter}/#{$all_counter} PASSED\033[0m, \033[1m#{$skip_counter}/#{$all_counter} SKIPPED\033[0m")
    elsif $fail_counter > 0 && $skip_counter == 0
        puts("\033[31;1m#{$fail_counter}/#{$all_counter} FAILED\033[0m, #{$pass_counter}/#{$all_counter} PASSED")
    else
        puts("\033[31;1m#{$fail_counter}/#{$all_counter} FAILED\033[0m, #{$pass_counter}/#{$all_counter} PASSED, \033[1m#{$skip_counter}/#{$all_counter} SKIPPED\033[0m")
    end

    if $skip_counter > 0 || $fail_counter > 0
        puts('---')
    end

    $skipped_tests.each { |test|
        puts("\033[1mSkipped\033[0m: #{test[:test]} #{test[:args].map { |arg| arg.shellescape } * ' '}")
        puts(test[:skip_msg])
        puts
    }

    $failed_tests.each { |test|
        puts("\033[31;1mFailed\033[0m: #{test[:test]} #{test[:args].map { |arg| arg.shellescape } * ' '}")
        puts("\033[1mstdout\033[0m:")
        puts(test[:stdout])
        puts
        puts("\033[1mstderr\033[0m:")
        puts(test[:stderr])
        puts
    }

    if $fail_counter > 0
        exit 1
    else
        exit 0
    end
end

def run_test(test_file, interpreter, test_info, cmdline)
    args = test_info['args']
    if !args
        args = []
    end

    if args.empty?
        args = [[]]
    else
        args = args[0].product(*args[1..])
    end

    if cmdline && cmdline.length > 1
        args = [cmdline[1..-1]]
    end

    if !$verbose
        $status_screen[test_file] = {
            total: args.length,
            fmt: "%#{(Math.log10(args.length) + 1).to_i}i",
            done: 0,
            line: '',
        }
    end

    args.each.with_index do |args, i|
        await_test_slot()

        $all_counter += 1

        job_index = $job_indices.find_index(nil)
        if !job_index
            job_index = $job_indices.length
        end
        $job_indices[job_index] = true

        if $verbose
            puts("\033[1mRunning\033[0m: #{test_file} #{args.map { |arg| arg.shellescape } * ' '}")
        end

        sock_tmpdir = Dir.mktmpdir('rsd-sock')
        imgs_tmpdir = Dir.mktmpdir('rsd-imgs')

        stdout_file = $verbose ? nil : "#{sock_tmpdir}/stdout"
        stderr_file = $verbose ? nil : "#{sock_tmpdir}/stderr"

        ENV['SOCK_DIR'] = sock_tmpdir
        ENV['IMGS_DIR'] = imgs_tmpdir
        ENV['JOB_INDEX'] = job_index.to_s
        pid = fork
        if !pid
            STDOUT.reopen(stdout_file) if stdout_file
            STDERR.reopen(stderr_file) if stderr_file

            exec(interpreter, test_file, *args)
        end

        $running_tests[pid] = {
            test_file: test_file,
            args: args,
            sock_tmpdir: sock_tmpdir,
            imgs_tmpdir: imgs_tmpdir,
            stdout: stdout_file,
            stderr: stderr_file,
            job_index: job_index,
        }
    end

    if !$verbose
        puts
    end
end

not_to_run = []
cmdline = nil
to_run = []

args = ARGV.to_a
while !args.empty?
    arg = args.shift
    case arg
    when '-s'
        not_to_run << args.shift
    when '-j'
        $jobs = Integer(args.shift)
    when '-c'
        cmdline = args
        break
    else
        to_run << arg
    end
end

if to_run.empty?
    to_run = nil
end

test_dir = File.realpath(File.dirname(__FILE__))

ENV['RSD_RB'] = test_dir + '/tests/lib/rsd.rb'

if !ENV['RSD']
    ENV['RSD'] = File.realpath(test_dir + '/../target/debug/rsd')
elsif ENV['RSD'].include?('/')
    ENV['RSD'] = File.realpath(ENV['RSD'])
end

if !ENV['QEMU_IMG']
    ENV['QEMU_IMG'] = 'qemu-img'
end

Dir.chdir(test_dir + '/tests')

Dir.entries('.').reject { |e|
    e[0] == '.'
}.select { |e|
    File.file?(e)
}.each { |test|
    if (cmdline && cmdline[0] != test) || (to_run && !to_run.include?(test)) || not_to_run.include?(test)
        next
    end

    lines = IO.readlines(test)
    shebang = lines.shift
    if !shebang.start_with?('#!')
        next
    end
    interpreter = shebang.delete_prefix('#!').strip

    test_info = nil
    while true
        line = lines.shift.strip
        if line[0] == '#'
            line = line[1..].strip
            if line.start_with?('test:')
                test_info = line.delete_prefix('test:')
                break
            end
        elsif !line.empty?
            break
        end
    end

    if !test_info
        next
    end

    while lines[0].strip[0] == '#'
        test_info += lines.shift.strip[1..]
    end

    test_info = JSON.parse(test_info)

    run_test(test, interpreter, test_info, cmdline)
}

await_all_tests()
print_results_and_exit()
